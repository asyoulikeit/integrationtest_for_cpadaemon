<!--
This file is part of CPA-Daemon,
a gRPC frontend for CPAchecker:
https://gitlab.com/sosy-lab/software/cpa-daemon/

SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: CC-BY-ND-4.0
-->

<!--
We use Markdown instead of AsciiDoc to ensure compatibility with rendering of GitLab Release Description.
-->

# CPA-Daemon Changelog


[CPA-Daemon](https://gitlab.com/sosy-lab/software/cpa-daemon/)
is a microservice for continuous software verification with CPAchecker.

All notable changes to CPA-Daemon are documented in this file,
starting from [1.0.0-SNAPSHOT-TACAS24-submission].

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

*Nothing so far.*

### Fixed

*Nothing so far.*

### Changed

- Renamed cpachecker-grpc to CPA-Daemon.

### Removed

*Nothing so far.*


## [1.0.0-SNAPSHOT-TACAS24-submission] - 2023-10-23

This is the first pre-release of CPA-Daemon.
It uses the old working-title 'cpachecker-grpc'.

### Added

- Support for three backends 'Separate JVM', 'Native', and 'Library'.
    - 'Separate JVM' executes original CPAchecker in a separate JVM process.
      This backend reflect the original CPAchecker execution.
    - 'Native' executes CPAchecker as native executable in a separate process.
      This backend provides a significantly faster response for fast-to-solve verification tasks
      (expected verification run time below 10 seconds). For most longer-running tasks, it should perform
      similar to Separate-JVM, but this is not guaranteed.
    - 'Library' executes CPAchecker in the same process as CPA-Daemon.
      This backend provides a significantly faster response, but requires an initial warm-up
      of a few seconds after server restarts. This warm-up happens with the first verification run.
- [gRPC interface](https://gitlab.com/sosy-lab/software/cpa-daemon/-/blob/1.0.0-SNAPSHOT-TACAS24-submission/src/main/proto/cpachecker.proto)
    for two services: `Runner` and `RunManager`.



[Unreleased]: https://gitlab.com/sosy-lab/software/cpa-daemon/-/compare/1.0.0-SNAPSHOT-TACAS24-submission...HEAD
[1.0.0-SNAPSHOT-TACAS24-submission]: https://gitlab.com/sosy-lab/software/cpa-daemon/-/releases/1.0.0-SNAPSHOT-TACAS24-submission

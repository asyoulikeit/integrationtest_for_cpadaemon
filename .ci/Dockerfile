# This file is part of CPA-Daemon,
# a gRPC frontend for CPAchecker:
# https://gitlab.com/sosy-lab/software/cpa-daemon/
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

# To build, run:
# docker build --pull -t registry.gitlab.com/sosy-lab/software/cpa-daemon/ci -f .ci/Dockerfile -

FROM ubuntu:23.04

RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN apt-get update && apt-get -qq -y install \
  curl \
  wget \
  unzip \
  zip \
  build-essential \
  libz-dev \
  zlib1g-dev \
  python3.11 \
  python3.11-venv \
  npm \
  git \
  && rm -rf /var/lib/apt/lists/*

ENV SDKMAN_DIR=/root/.sdkman
RUN curl -s "https://get.sdkman.io" | bash
# We can not use Java 21 yet because of the new https://bugs.openjdk.org/browse/JDK-8015831
# that conflicts with CPAchecker's config.inject
RUN bash -c 'source "$SDKMAN_DIR/bin/sdkman-init.sh" && sdk install java 20.0.2-graal '
RUN bash -c 'source "$SDKMAN_DIR/bin/sdkman-init.sh" && sdk install gradle 8.3'

# Add candidate path to $PATH environment variable
ENV JAVA_HOME="$SDKMAN_DIR/candidates/java/current"
ENV PATH="$JAVA_HOME/bin:$PATH"

ENTRYPOINT ["/bin/bash", "-c", "source $SDKMAN_DIR/bin/sdkman-init.sh && \"$@\"", "-s"]

RUN npm install -g asciidoc-link-check

# This file is part of CPA-Daemon,
# a gRPC frontend for CPAchecker:
# https://gitlab.com/sosy-lab/software/cpa-daemon/
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import benchexec.result as result
import benchexec.tools.template
from benchexec.tools.template import BaseTool2
from json import JSONDecoder, JSONDecodeError


class Tool(benchexec.tools.template.BaseTool2):
    """
    This tool-info module executes the connector script
    for the CPAchecker-daemon.
    """

    REQUIRED_PATHS = ["quarkus-app/**/*", "bin/__cpadaemon", "proto/*", "connector.py"]

    def executable(self, tool_locator):
        return tool_locator.find_executable("__cpadaemon", subdir="bin")

    def version(self, executable):
        return self._version_from_tool(executable, "--version")

    def program_files(self, executable):
        return self._program_files_from_executable(
            executable, self.REQUIRED_PATHS, parent_dir=True
        )

    def name(self):
        return "CPAchecker-daemon"

    def cmdline(self, executable, options, task, rlimits):
        base = [executable]

        if task.property_file:
            base += ["--spec", task.property_file]

        if isinstance(task.options, dict) and task.options.get("language") == "C":
            data_model = task.options.get("data_model")
            if data_model:
                data_model_option = {"ILP32": "LINUX32", "LP64": "LINUX64"}.get(
                    data_model
                )
                base += ["--data-model", data_model_option]

        return base + options + [task.single_input_file]

    def determine_result(self, run: BaseTool2.Run):
        output: BaseTool2.RunOutput = run.output
        verdict_json = output.text.split("-" * 20)[-1]

        try:
            res = JSONDecoder().decode(verdict_json)
        except JSONDecodeError:
            return result.RESULT_UNKNOWN

        verdict = res["verdict"]
        if verdict == "UNKNOWN":
            return result.RESULT_UNKNOWN

        if verdict == "TRUE":
            return result.RESULT_TRUE_PROP

        if verdict == "FALSE":
            return result.RESULT_FALSE_PROP

        if verdict == "DONE":
            return result.RESULT_DONE

        return result.RESULT_ERROR


def test_determine_result_unknown():
    tool = Tool()
    verdict_output = "TRUE FALSE ERROR\n" + ("-" * 20) + "\n" + '{"verdict": "UNKNOWN"}'
    run_output = BaseTool2.RunOutput(verdict_output.split("\n"))
    run = BaseTool2.Run(
        cmdline="dummy", exit_code=1, output=run_output, termination_reason="timeout"
    )
    expected_result = result.RESULT_UNKNOWN

    actual_result = tool.determine_result(run)

    assert expected_result == actual_result


def test_determine_result_true():
    tool = Tool()
    verdict_output = "UNKNOWN FALSE ERROR\n" + ("-" * 20) + "\n" + '{"verdict": "TRUE"}'
    run_output = BaseTool2.RunOutput(verdict_output.split("\n"))
    run = BaseTool2.Run(
        cmdline="dummy", exit_code=1, output=run_output, termination_reason=None
    )
    expected_result = result.RESULT_TRUE_PROP

    actual_result = tool.determine_result(run)

    assert expected_result == actual_result


def test_determine_result_false():
    tool = Tool()
    verdict_output = "UNKNOWN TRUE ERROR\n" + ("-" * 20) + "\n" + '{"verdict": "FALSE"}'
    run_output = BaseTool2.RunOutput(verdict_output.split("\n"))
    run = BaseTool2.Run(
        cmdline="dummy", exit_code=1, output=run_output, termination_reason=None
    )
    expected_result = result.RESULT_FALSE_PROP

    actual_result = tool.determine_result(run)

    assert expected_result == actual_result


def test_determine_result_error():
    tool = Tool()
    verdict_output = "UNKNOWN TRUE FALSE\n" + ("-" * 20) + "\n" + '{"verdict": "ERROR"}'
    run_output = BaseTool2.RunOutput(verdict_output.split("\n"))
    run = BaseTool2.Run(
        cmdline="dummy", exit_code=1, output=run_output, termination_reason=None
    )
    expected_result = result.RESULT_ERROR

    actual_result = tool.determine_result(run)

    assert expected_result == actual_result


def test_determine_result_none():
    tool = Tool()
    verdict_output = "UNKNOWN TRUE FALSE\n" + ("-" * 20) + "\n" + '{"verdict": ""}'
    run_output = BaseTool2.RunOutput(verdict_output.split("\n"))
    run = BaseTool2.Run(
        cmdline="dummy", exit_code=1, output=run_output, termination_reason=None
    )
    expected_result = result.RESULT_ERROR

    actual_result = tool.determine_result(run)

    assert expected_result == actual_result

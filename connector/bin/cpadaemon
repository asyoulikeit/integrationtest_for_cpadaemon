#!/usr/bin/env python3

# This file is part of CPA-Daemon,
# a gRPC frontend for CPAchecker:
# https://gitlab.com/sosy-lab/software/cpa-daemon/
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import glob
import os
import pathlib
import sys

sys.dont_write_bytecode = True  # prevents writing .pyc files

script = pathlib.Path(__file__).resolve()
project_dir = script.parent.parent
proto_dir = project_dir / "proto"

lib_dir = project_dir / "lib"

sys.path.insert(0, str(project_dir))
sys.path.append(str(lib_dir))
sys.path.append(str(proto_dir))

sys.setrecursionlimit(5000)
import connector  # noqa E402

sys.exit(connector.main(sys.argv[1:]))

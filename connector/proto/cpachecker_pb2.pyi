from google.protobuf import duration_pb2 as _duration_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class MachineModel(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    UNSPECIFIED: _ClassVar[MachineModel]
    LINUX32: _ClassVar[MachineModel]
    LINUX64: _ClassVar[MachineModel]

class Verdict(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    UNKNOWN: _ClassVar[Verdict]
    TRUE: _ClassVar[Verdict]
    FALSE: _ClassVar[Verdict]
    ERROR: _ClassVar[Verdict]
    DONE: _ClassVar[Verdict]

class RunStatus(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
    __slots__ = []
    UNKNOWN_STATUS: _ClassVar[RunStatus]
    PENDING: _ClassVar[RunStatus]
    RUNNING: _ClassVar[RunStatus]
    FINISHED: _ClassVar[RunStatus]
UNSPECIFIED: MachineModel
LINUX32: MachineModel
LINUX64: MachineModel
UNKNOWN: Verdict
TRUE: Verdict
FALSE: Verdict
ERROR: Verdict
DONE: Verdict
UNKNOWN_STATUS: RunStatus
PENDING: RunStatus
RUNNING: RunStatus
FINISHED: RunStatus

class StartRunRequest(_message.Message):
    __slots__ = ["programCode", "config", "specification", "additionalOptions", "machineModel", "entryFunction", "witness"]
    PROGRAMCODE_FIELD_NUMBER: _ClassVar[int]
    CONFIG_FIELD_NUMBER: _ClassVar[int]
    SPECIFICATION_FIELD_NUMBER: _ClassVar[int]
    ADDITIONALOPTIONS_FIELD_NUMBER: _ClassVar[int]
    MACHINEMODEL_FIELD_NUMBER: _ClassVar[int]
    ENTRYFUNCTION_FIELD_NUMBER: _ClassVar[int]
    WITNESS_FIELD_NUMBER: _ClassVar[int]
    programCode: str
    config: str
    specification: str
    additionalOptions: _containers.RepeatedCompositeFieldContainer[CpacheckerOption]
    machineModel: MachineModel
    entryFunction: str
    witness: str
    def __init__(self, programCode: _Optional[str] = ..., config: _Optional[str] = ..., specification: _Optional[str] = ..., additionalOptions: _Optional[_Iterable[_Union[CpacheckerOption, _Mapping]]] = ..., machineModel: _Optional[_Union[MachineModel, str]] = ..., entryFunction: _Optional[str] = ..., witness: _Optional[str] = ...) -> None: ...

class CpacheckerOption(_message.Message):
    __slots__ = ["name", "textValue", "fileValue"]
    NAME_FIELD_NUMBER: _ClassVar[int]
    TEXTVALUE_FIELD_NUMBER: _ClassVar[int]
    FILEVALUE_FIELD_NUMBER: _ClassVar[int]
    name: str
    textValue: TextValues
    fileValue: FileValues
    def __init__(self, name: _Optional[str] = ..., textValue: _Optional[_Union[TextValues, _Mapping]] = ..., fileValue: _Optional[_Union[FileValues, _Mapping]] = ...) -> None: ...

class TextValues(_message.Message):
    __slots__ = ["values"]
    VALUES_FIELD_NUMBER: _ClassVar[int]
    values: _containers.RepeatedScalarFieldContainer[str]
    def __init__(self, values: _Optional[_Iterable[str]] = ...) -> None: ...

class FileValues(_message.Message):
    __slots__ = ["values"]
    VALUES_FIELD_NUMBER: _ClassVar[int]
    values: _containers.RepeatedScalarFieldContainer[bytes]
    def __init__(self, values: _Optional[_Iterable[bytes]] = ...) -> None: ...

class RunResponse(_message.Message):
    __slots__ = ["error", "run"]
    ERROR_FIELD_NUMBER: _ClassVar[int]
    RUN_FIELD_NUMBER: _ClassVar[int]
    error: str
    run: Run
    def __init__(self, error: _Optional[str] = ..., run: _Optional[_Union[Run, _Mapping]] = ...) -> None: ...

class Run(_message.Message):
    __slots__ = ["name", "status", "resultVerdict", "resultMessage", "details"]
    NAME_FIELD_NUMBER: _ClassVar[int]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    RESULTVERDICT_FIELD_NUMBER: _ClassVar[int]
    RESULTMESSAGE_FIELD_NUMBER: _ClassVar[int]
    DETAILS_FIELD_NUMBER: _ClassVar[int]
    name: str
    status: RunStatus
    resultVerdict: Verdict
    resultMessage: _containers.RepeatedScalarFieldContainer[str]
    details: RunDetails
    def __init__(self, name: _Optional[str] = ..., status: _Optional[_Union[RunStatus, str]] = ..., resultVerdict: _Optional[_Union[Verdict, str]] = ..., resultMessage: _Optional[_Iterable[str]] = ..., details: _Optional[_Union[RunDetails, _Mapping]] = ...) -> None: ...

class RunDetails(_message.Message):
    __slots__ = ["stdout", "stderr", "outputFiles"]
    STDOUT_FIELD_NUMBER: _ClassVar[int]
    STDERR_FIELD_NUMBER: _ClassVar[int]
    OUTPUTFILES_FIELD_NUMBER: _ClassVar[int]
    stdout: str
    stderr: str
    outputFiles: _containers.RepeatedScalarFieldContainer[str]
    def __init__(self, stdout: _Optional[str] = ..., stderr: _Optional[str] = ..., outputFiles: _Optional[_Iterable[str]] = ...) -> None: ...

class GetFileRequest(_message.Message):
    __slots__ = ["runName", "fileName"]
    RUNNAME_FIELD_NUMBER: _ClassVar[int]
    FILENAME_FIELD_NUMBER: _ClassVar[int]
    runName: str
    fileName: _containers.RepeatedScalarFieldContainer[str]
    def __init__(self, runName: _Optional[str] = ..., fileName: _Optional[_Iterable[str]] = ...) -> None: ...

class GetFileResponse(_message.Message):
    __slots__ = ["result"]
    RESULT_FIELD_NUMBER: _ClassVar[int]
    result: _containers.RepeatedCompositeFieldContainer[MaybeFile]
    def __init__(self, result: _Optional[_Iterable[_Union[MaybeFile, _Mapping]]] = ...) -> None: ...

class MaybeFile(_message.Message):
    __slots__ = ["runName", "fileName", "content", "error"]
    RUNNAME_FIELD_NUMBER: _ClassVar[int]
    FILENAME_FIELD_NUMBER: _ClassVar[int]
    CONTENT_FIELD_NUMBER: _ClassVar[int]
    ERROR_FIELD_NUMBER: _ClassVar[int]
    runName: str
    fileName: str
    content: bytes
    error: str
    def __init__(self, runName: _Optional[str] = ..., fileName: _Optional[str] = ..., content: _Optional[bytes] = ..., error: _Optional[str] = ...) -> None: ...

class Void(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class GetRunRequest(_message.Message):
    __slots__ = ["name"]
    NAME_FIELD_NUMBER: _ClassVar[int]
    name: str
    def __init__(self, name: _Optional[str] = ...) -> None: ...

class ListRunsRequest(_message.Message):
    __slots__ = []
    def __init__(self) -> None: ...

class ListRunsResponse(_message.Message):
    __slots__ = ["runs"]
    RUNS_FIELD_NUMBER: _ClassVar[int]
    runs: _containers.RepeatedCompositeFieldContainer[Run]
    def __init__(self, runs: _Optional[_Iterable[_Union[Run, _Mapping]]] = ...) -> None: ...

class CancelRunRequest(_message.Message):
    __slots__ = ["name"]
    NAME_FIELD_NUMBER: _ClassVar[int]
    name: str
    def __init__(self, name: _Optional[str] = ...) -> None: ...

class CloseRunRequest(_message.Message):
    __slots__ = ["name"]
    NAME_FIELD_NUMBER: _ClassVar[int]
    name: str
    def __init__(self, name: _Optional[str] = ...) -> None: ...

class WaitRunRequest(_message.Message):
    __slots__ = ["name", "timeout"]
    NAME_FIELD_NUMBER: _ClassVar[int]
    TIMEOUT_FIELD_NUMBER: _ClassVar[int]
    name: str
    timeout: _duration_pb2.Duration
    def __init__(self, name: _Optional[str] = ..., timeout: _Optional[_Union[_duration_pb2.Duration, _Mapping]] = ...) -> None: ...

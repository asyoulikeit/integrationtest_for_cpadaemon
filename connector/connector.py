# This file is part of CPA-Daemon,
# a gRPC frontend for CPAchecker:
# https://gitlab.com/sosy-lab/software/cpa-daemon/
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import argparse
import datetime
import fnmatch
import grpc
import json
import logging
import re
from urllib import request
from urllib.error import HTTPError, URLError
import subprocess
import sys
import time
from argparse import ArgumentParser
from google.protobuf.duration_pb2 import Duration
from pathlib import Path
from typing import Dict, Iterable, List, Union, cast

# Necessary hack to use the generated proto files out of the box
loc = Path(__file__).parent
proto = loc / "proto"
sys.path.append(str(proto))

from proto.cpachecker_pb2_grpc import RunManagerStub, RunnerStub  # noqa: E402
from proto.cpachecker_pb2 import (  # noqa: E402
    CloseRunRequest,
    CpacheckerOption,
    FileValues,
    GetFileResponse,
    RunStatus,
    CancelRunRequest,
    StartRunRequest,
    GetFileRequest,
    RunResponse,
    Run,
    MachineModel,
    TextValues,
    WaitRunRequest,
    Verdict,
)

SCRIPT_LOC = Path(__file__).parent
JAR = SCRIPT_LOC / "quarkus-app" / "quarkus-run.jar"
CPADAEMON_BASE_COMMAND = ["java", "-jar", JAR]
RESOURCES = SCRIPT_LOC / "resources"
N_RETRIES = 3
MAX_HEALTH_RETRIES = 1000
ERR_RESPONSE = RunResponse(run=Run(name="", resultVerdict=Verdict.ERROR))

UNREACH_CALL = re.compile(
    r"CHECK\(\s*init\(main\(\)\),\s*LTL\(G\s*!\s*call\(reach_error\(\)\)\)\s*\)"
)


Option = Union[FileValues, TextValues]


class State:
    process: subprocess.Popen | None = None
    config: argparse.Namespace

    def __init__(self, args: List[str]):
        parser = get_parser()
        self.config, remaining = parser.parse_known_args(args)
        self.properties: Dict[str, CpacheckerOption] = State.parse_vars(
            self.config.setprop
        )
        self.cpachecker_config = self._maybe_find_cpachecker_config(remaining)

    def _maybe_find_cpachecker_config(self, remaining: List[str]) -> Path:
        """
        Try to find a CPAchecker config file in the remaining arguments.
        If one is found, remove it from the list and return it.
        """

        if len(remaining) == 0:
            if self.config.svcomp23 is not None:
                return self.config.svcomp23
            if self.config.config is not None:
                return self.config.config

        if len(remaining) >= 1 and (
            self.config.svcomp23 is not None or self.config.config is not None
        ):
            print("Explicit config given, ignoring remaining arguments.")
            if self.config.svcomp23 is not None:
                return self.config.svcomp23

            return self.config.config

        if len(remaining) > 1:
            print("multiple cpachecker configs given. Using the first one that exists.")

        config_loc = RESOURCES / "cpachecker_configs"

        for arg in remaining:
            file_name = arg.lstrip("-").rsplit(".properties", 1)[0]
            file_name = file_name + ".properties"
            file_loc = config_loc / file_name
            print("Trying to find config file: ", file_loc)
            if file_loc.is_file() and file_loc.exists():
                logging.info("Using cpachecker config file: %s", file_loc)
                return file_loc

        raise argparse.ArgumentError(None, "No CPAchecker config file given.")

    @staticmethod
    def _detect_multi_file_argument(arg: str) -> Option:
        """
        Configuration parameters of CPAchecker accept multiple values separated by comma.
        If these values are just strings, nothing remains to be done.
        However, if these values are file names, we need to read the files and an pack them
        into multiple FileOptions to be sent to the server.
        The following invariant holds: if values[0] is not a file then all values are not a file.
        """

        # ignore empty values, this is ok and should not lead to us interpreting
        # all results as TextValue.
        values = [v for v in arg.split(",") if v]
        # TextValues
        # check if any value is not a file
        for value in values:
            if not Path(value).is_file():
                return TextValues(values=[arg])

        # all values are files
        # read the files and pack them into FileOptions
        content: List[bytes] = []
        for value in values:
            with Path(value).open("rb") as f:
                content.append(f.read())

        return FileValues(values=content)

    @staticmethod
    def parse_var(s: str) -> tuple[str, str]:
        """
        Parse a key, value pair, separated by '='
        That's the reverse of ShellArgs.

        On the command line (argparse) a declaration will typically look like:
            foo=hello
        or
            foo="hello world"
        """
        items = s.split("=")
        key = items[0].strip()  # we remove blanks around keys, as is logical
        value = ""
        if len(items) > 1:
            # rejoin the rest:
            value = "=".join(items[1:])
        return (key, value)

    @staticmethod
    def parse_vars(items) -> dict[str, CpacheckerOption]:
        """
        Parse a series of key-value pairs and return a dictionary
        """
        d = {}

        if items:
            for item in items:
                key, value = State.parse_var(item)
                option = State._detect_multi_file_argument(value)
                if isinstance(option, FileValues):
                    d[key] = CpacheckerOption(name=key, fileValue=option)
                else:
                    d[key] = CpacheckerOption(name=key, textValue=option)
        return d

    def cleanup(self):
        if not self.config.cleanup:
            return

        if self.process is not None:
            self.process.kill()
            self.process.wait()
            self.process = None


def get_parser():
    parser = ArgumentParser("cpa-daemon", description="Connector for CPA-Daemon")

    config = parser.add_mutually_exclusive_group()
    config.add_argument("--config", help="Path to the configuration file", type=Path)
    config.add_argument(
        "--svcomp23",
        help="Use the SVCOMP 23 configuration",
        action="store_const",
        const=RESOURCES / "svcomp23.properties",
    )
    parser.add_argument("--version", action="version", version="1.0")
    parser.add_argument(
        "--witness", help="Path to the witness file", type=Path, default=None
    )
    parser.add_argument(
        "--entryfunction", help="Name of the entry function", type=str, default="main"
    )
    parser.add_argument("--spec", help="Path to the specification file", type=Path)
    parser.add_argument(
        "--data-model",
        help="Data model to use",
        choices=["LINUX32", "LINUX64"],
        default="LINUX32",
    )
    parser.add_argument(
        "--no-launch",
        help="Do not launch a new daemon",
        action="store_true",
        default=False,
    )
    parser.add_argument(
        "--result-file",
        help="Pattern for the result file that should be extracted",
        type=str,
        metavar="PATTERN",
        default=[],
        action="append",
    )
    parser.add_argument(
        "--timeout",
        help="Timeout for the run in seconds",
        type=int,
        default=60,
        metavar="SECONDS",
    )

    # Configure the endpoint for the grpc server
    parser.add_argument(
        "--endpoint",
        help="Endpoint for the CPA-Daemon gRPC server",
        type=str,
        default="localhost:8080",
        metavar="ENDPOINT",
    )

    parser.add_argument(
        "--output-dir",
        action="store",
        type=Path,
        default=Path.cwd() / "output",
        help="Directory to store the output files in",
    )

    parser.add_argument("PROGRAM", help="Path to the program to verify", type=Path)

    # put unhandled arguments into a list
    parser.add_argument(
        "--setprop",
        action="append",
        default=[],
        metavar="KEY=VALUE",
        help="Set a property. The property name is the KEY, the property value is VALUE."
        "This option can be used multiple times to set multiple properties.",
    )

    # flag whether a cleanup should be performed, default is false
    parser.add_argument(
        "--cleanup",
        action="store_true",
        default=False,
        help="Flag whether a cleanup should be performed, default is false",
    )

    return parser


def read_text(file: Path) -> str | None:
    if file is None:
        return None

    if not file.is_file():
        raise AssertionError(f"Path {file} does not Point to file")

    with file.open("r") as f:
        return f.read()


def get_specification(config: argparse.Namespace) -> str | None:
    if config.spec is None:
        return None

    spec = read_text(config.spec)
    if spec is None:
        return None

    # For the SV-Benchmarks to work we need to accept the unreach-call.prp specification
    # The engine however will directly pass the specification to CPAchecker, bypassing
    # its internal matching of SV-COMP tasks to specification automata.
    # Thus we apply a similar measure here: if the specification contains the SV-COMP reachability
    # property, then we just replace it with the proper specification automaton.
    match = UNREACH_CALL.search(spec)
    if match is None:
        return spec
    logging.info("Found svcomp spec for unreachable call, translating to automaton")
    return read_text(RESOURCES / "sv-comp-reachability.spc")


def start_run(state: State, runner: RunnerStub) -> RunResponse:
    config = state.config
    machine_model = cast(MachineModel, MachineModel.Value(config.data_model))

    cpachecker_config = read_text(state.cpachecker_config)
    specification = get_specification(config)
    additional_options = state.properties.values()

    request = StartRunRequest(
        programCode=read_text(config.PROGRAM),
        config=cpachecker_config,
        specification=specification,
        machineModel=machine_model,
        entryFunction=config.entryfunction,
        witness=read_text(config.witness),
        additionalOptions=additional_options,
    )
    logging.debug("Sending StartRunRequest: %s", request)

    return runner.StartRun(request)


def find_files_to_retrieve(state: State, result: RunResponse) -> Iterable[str]:
    files = set()
    for pattern in state.config.result_file:
        files |= set(fnmatch.filter(result.run.details.outputFiles, pattern))

    return files


def get_files(
    run: Run, file: Iterable[str], manager: RunManagerStub
) -> GetFileResponse:
    file_request = GetFileRequest(runName=run.name, fileName=file)
    logging.debug("Sending GetFileRequest: %s", file_request)
    return manager.GetFile(file_request)


def write_files(state: State, files: GetFileResponse):
    outdir: Path = state.config.output_dir
    outdir.mkdir(parents=True, exist_ok=True)
    for file in files.result:
        if file.error != "":
            print(f"Error while retrieving file {file.fileName}: {file.error}")
            continue

        out = outdir / Path(file.fileName).name

        logging.info("Retrieved file %s", file.fileName)
        with out.open("wb") as f:
            f.write(file.content)


def wait_run(run: Run, timeout: int, manager: RunManagerStub) -> RunResponse:
    duration = Duration(seconds=timeout)
    request = WaitRunRequest(name=run.name, timeout=duration)
    return manager.WaitRun(request)


def close_run(run: Run, manager: RunManagerStub):
    request = CloseRunRequest(name=run.name)
    return manager.CloseRun(request)


def stop(run: RunResponse, manager: RunManagerStub) -> RunResponse:
    name = run.run.name
    if run.run.status == RunStatus.FINISHED:
        return run

    print(f"Stopping run {name}")
    stop_request = CancelRunRequest(name=name)
    return manager.CancelRun(stop_request)


def start_daemon(run_state: State):
    # launch a subprocess in a new session (to avoid getting killed when this script finishes)
    proc = subprocess.Popen(
        CPADAEMON_BASE_COMMAND,
        start_new_session=True,
    )
    run_state.process = proc


def maybe_start_daemon(run_state: State, address: str) -> grpc.Channel:
    channel = grpc.insecure_channel(
        address,
        options=[
            ("grpc.max_send_message_length", 47693650),
            ("grpc.max_receive_message_length", 47693650),
        ],
    )
    timeout = 0.2
    if run_state.config.no_launch:
        timeout = 5
    try:
        grpc.channel_ready_future(channel).result(timeout=timeout)
        return channel
    except grpc.FutureTimeoutError:
        print("Could not connect to daemon.")
        if run_state.config.no_launch:
            raise ConnectionError("Could not connect to daemon.")

    time = datetime.datetime.now()
    print("Starting daemon...")
    start_daemon(run_state)
    for i in range(N_RETRIES):
        try:
            grpc.channel_ready_future(channel).result(timeout=5)
            end = datetime.datetime.now() - time
            print(f"Starting took: {end.total_seconds()}s total with {i} retries.")
            return channel
        except grpc.FutureTimeoutError:
            pass

    raise TimeoutError("Could not connect to daemon in the given time and retries.")


def result_verdict_to_string(verdict: Verdict) -> str:
    """
    Convert a verdict to a string.
    """
    if verdict == Verdict.ERROR:
        return "ERROR"
    if verdict == Verdict.UNKNOWN:
        return "UNKNOWN"
    if verdict == Verdict.TRUE:
        return "TRUE"
    if verdict == Verdict.FALSE:
        return "FALSE"
    if verdict == Verdict.DONE:
        return "DONE"

    # This should never happen
    raise AssertionError(f"Unknown verdict: {verdict}")


def log_result(res: RunResponse):
    verdict = result_verdict_to_string(res.run.resultVerdict)
    to_json = {
        "verdict": verdict,
    }

    print("-" * 20)
    json.dump(to_json, sys.stdout)


def handle_long_running_run(
    run: Run, timeout: int, manager: RunManagerStub
) -> RunResponse:
    print("Run was not finished during cancellation. Waiting...")
    res = wait_run(run, timeout=timeout, manager=manager)
    if res.run.status != RunStatus.FINISHED:
        close_run(run, manager)
        msg = f"Cancellation did not propagate in {timeout} seconds. Aborting..."
        print(msg)
        raise TimeoutError(msg)
    return res


def check_health(address: str):
    # add http:// to the address if necessary
    if not address.startswith("http"):
        address = address.split("://")[-1]
        address = "http://" + address
    logging.info("Performing readiness check against %s", address)
    for _ in range(MAX_HEALTH_RETRIES):
        try:
            response = request.urlopen(address + "/q/health/ready")
        except (URLError, HTTPError):
            time.sleep(0.3)
            continue

        if response.code == 200:
            break

        time.sleep(0.3)
    logging.info("Readiness check complete!")


def main(args: List[str]):
    run_state = State(args)

    try:
        if run_state.config.no_launch:
            check_health(run_state.config.endpoint)
        channel = maybe_start_daemon(run_state, run_state.config.endpoint)
        manager = RunManagerStub(channel)
        runner = RunnerStub(channel)

        res = start_run(run_state, runner)
        if res.error != "":
            print("Error while starting run: ", res.error)
            log_result(ERR_RESPONSE)
            return
        print("Start Run: ", res)
        run = wait_run(res.run, timeout=run_state.config.timeout, manager=manager)
        print("Wait Run: ", run)
        stop_response = stop(run, manager)

        if stop_response.run.status != RunStatus.FINISHED:
            handle_long_running_run(
                stop_response.run,
                timeout=max(60, run_state.config.timeout),
                manager=manager,
            )
        try:
            print(run.run.details.stdout)
            print(run.run.details.stderr, file=sys.stderr)
        except Exception:
            print("exception while printing stdout/stderr")

        logging.info("Retrieving files")
        to_retrieve = find_files_to_retrieve(run_state, run)
        logging.info("Retrieving %d files: %s", len(to_retrieve), to_retrieve)
        files = get_files(run.run, to_retrieve, manager)
        write_files(run_state, files)

        # Delete run from server after retrieving all information
        close_run(stop_response.run, manager)

        log_result(run)

    except ConnectionError:
        print("No active daemon was present. Aborting...")
        log_result(ERR_RESPONSE)
    except TimeoutError:
        log_result(ERR_RESPONSE)
    finally:
        run_state.cleanup()


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    main(sys.argv[1:])

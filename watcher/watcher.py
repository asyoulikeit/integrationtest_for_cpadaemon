#!/bin/env python3

# This file is part of CPA-Daemon,
# a gRPC frontend for CPAchecker:
# https://gitlab.com/sosy-lab/software/cpa-daemon/
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

import argparse
import logging
import requests
import subprocess
import sys
import time
from enum import Enum
from pathlib import Path
from typing import Callable


def get_parser():
    """The parser accepts the option --endpoint, specifying the endpoint on which the
    daemon shall run.
    The option --executable is a Path pointing to the quarkus-run.jar file.
    """

    parser = argparse.ArgumentParser(description="Watcher daemon")
    parser.add_argument(
        "--endpoint",
        dest="endpoint",
        default="localhost:8080",
        help="Endpoint on which the daemon shall run",
    )

    default_location = (
        Path(__file__).parent.parent
        / "build"
        / "toArchive"
        / "cpa-daemon"
        / "quarkus-app"
        / "quarkus-run.jar"
    )
    parser.add_argument(
        "--executable",
        dest="executable",
        default=default_location,
        help="Path to the quarkus-run.jar file",
    )

    return parser


"""
Utility (Linux only) to ensure subprocesses exit when their parents do by sending
a specified signal when the parent dies.
Usage:
    subprocess.Popen(['some-executable'], preexec_fn=on_parent_exit('SIGHUP'))
"""

import signal
from ctypes import cdll

# Constant taken from http://linux.die.net/include/linux/prctl.h
PR_SET_PDEATHSIG = 1


class PrCtlError(Exception):
    pass


def on_parent_exit(signame):
    """
    Return a function to be run in a child process which will trigger SIGNAME
    to be sent when the parent process dies
    """
    signum = getattr(signal, signame)

    def set_parent_exit_signal():
        # http://linux.die.net/man/2/prctl
        result = cdll["libc.so.6"].prctl(PR_SET_PDEATHSIG, signum)
        if result != 0:
            raise PrCtlError("prctl failed with error code %s" % result)

    return set_parent_exit_signal


class ProcessState(Enum):
    IDLE = 0
    STARTING = 1
    RUNNING = 2
    STOPPING = 3
    STOPPED = 4


process = None


def watch(
    process_factory: Callable[[], subprocess.Popen[bytes]],
    health_address: str,
    sleep_time=1,
):
    global process
    logging.info("Starting Process...")
    process = process_factory()
    process_state = ProcessState.STARTING

    while True:
        time.sleep(sleep_time)
        try:
            response = requests.get(health_address, timeout=1)
        except (requests.exceptions.ConnectionError, requests.exceptions.Timeout) as e:
            logging.debug("Connection error, restarting process")
            response = None
            if process_state == ProcessState.STARTING:
                continue

        if process_state != ProcessState.STARTING and (
            response is None or response.status_code != 200
        ):
            logging.debug("Health check failed, restarting process")
            process.kill()
            process_state = ProcessState.STOPPING
            process.wait()
            process_state = ProcessState.STOPPED
            process = process_factory()
            process_state = ProcessState.STARTING
            continue

        # response is 200, process is healthy and running
        if (
            response is not None
            and response.status_code == 200
            and process_state == ProcessState.STARTING
        ):
            process_state = ProcessState.RUNNING


def main(raw_args: list[str]):
    """
    Start the executable in a new subprocess. Continuously check the health endpoint via an HTTP request.
    If the health request fails, restart the executable.

    :param args: the arguments passed to the program
    :return:
    """

    parser = get_parser()
    args, jvm_args = parser.parse_known_args(raw_args)
    logging.info("Args forwards to the JVM: %s", jvm_args)

    # prep the arguments for quarkus http and grpc
    host = args.endpoint.split(":")[0]
    port = args.endpoint.split(":")[1]

    template_host = "-Dquarkus.{method}.host={host}"
    template_port = "-Dquarkus.{method}.port={port}"

    conn_args = [
        template_host.format(method="http", host=host),
        template_port.format(method="http", port=port),
        template_host.format(method="grpc", host=host),
        template_port.format(method="grpc", port=port),
    ]

    # Start the executable in a new subprocess
    process_factory = lambda: subprocess.Popen(  # noqa E731
        ["java", "-jar", args.executable] + conn_args + jvm_args,
        preexec_fn=on_parent_exit("SIGHUP"),
    )

    health_address = "http://" + host.split("//")[-1] + ":" + port + "/q/health/live"
    watch(process_factory, health_address)


def handler_stop_signals(signum, frame):
    global process
    logging.info("Stopping the watcher daemon...")
    if process is not None:
        logging.info("Killing the process...")
        process.kill()
        process.wait(5)
    sys.exit(0)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)

    signal.signal(signal.SIGINT, handler_stop_signals)
    signal.signal(signal.SIGTERM, handler_stop_signals)
    signal.signal(signal.SIGHUP, handler_stop_signals)

    main(sys.argv[1:])

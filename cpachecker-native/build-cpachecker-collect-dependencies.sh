#!/bin/bash

# This file is part of CPA-Daemon,
# a gRPC frontend for CPAchecker:
# https://gitlab.com/sosy-lab/software/cpa-daemon/
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -euo pipefail
set -x

CPACHECKER_DIR="$1"
SVBENCHMARKS_DIR=$(realpath "$2")
OUTPUT_DIR=$(realpath "$3")
mkdir -p "$OUTPUT_DIR"
(
  cd $CPACHECKER_DIR
  export JAVA_VM_ARGUMENTS="-agentlib:native-image-agent=config-merge-dir=$OUTPUT_DIR/META-INF/native-image"
  for relevant in eca-rers2012/Problem13_label45.c float-newlib/double_req_bl_0210.c array-examples/sanfoundry_24-1.i bitvector/jain_1-1.c ntdrivers-simplified/diskperf_simpl1.cil.c ntdrivers-simplified/floppy_simpl4.cil-1.c product-lines/email_spec11_product03.cil.c recursive-simple/id_o200.c; do
    scripts/cpa.sh -svcomp23 -timelimit 900 "$SVBENCHMARKS_DIR/$relevant"
  done
  scripts/cpa.sh -predicateAnalysis-bam -timelimit 100 "$SVBENCHMARKS_DIR/float-newlib/double_req_bl_0210.c"
  scripts/cpa.sh -svcomp23 -timelimit 900 -spec "$SVBENCHMARKS_DIR/properties/no-data-race.prp"  "$SVBENCHMARKS_DIR/goblint-regression/13-privatized_37-traces-ex-4_true.i"
  scripts/cpa.sh -svcomp23 -timelimit 900 -spec "$SVBENCHMARKS_DIR/properties/valid-memsafety.prp"  "$SVBENCHMARKS_DIR/array-memsafety/cstrcat_unsafe.c"
  scripts/cpa.sh -svcomp23 -timelimit 900 -spec "$SVBENCHMARKS_DIR/properties/valid-memsafety.prp"  "$SVBENCHMARKS_DIR/ldv-memsafety/memsat3.c"
  scripts/cpa.sh -svcomp23 -timelimit 900 -spec "$SVBENCHMARKS_DIR/properties/valid-memsafety.prp"  "$SVBENCHMARKS_DIR/goblint-regression/28-race_reach_92-evilcollapse_racing.i"
  scripts/cpa.sh -svcomp23 -timelimit 900 -spec "$SVBENCHMARKS_DIR/properties/valid-memcleanup.prp"  "$SVBENCHMARKS_DIR/forester-heap/sll-circular-2.i"
  scripts/cpa.sh -svcomp23 -timelimit 900 -spec "$SVBENCHMARKS_DIR/properties/no-overflow.prp"  "$SVBENCHMARKS_DIR/pthread/bigshot_s.i"
  scripts/cpa.sh -svcomp23 -timelimit 900 -spec "$SVBENCHMARKS_DIR/properties/no-overflow.prp"  "$SVBENCHMARKS_DIR/pthread/bigshot_s.i"
  scripts/cpa.sh -svcomp23 -timelimit 900 -spec "$SVBENCHMARKS_DIR/properties/no-overflow.prp"  "$SVBENCHMARKS_DIR/recursive/EvenOdd03WithOverflowBug.c"
  scripts/cpa.sh -svcomp23 -timelimit 900 -spec "$SVBENCHMARKS_DIR/properties/termination.prp"  "$SVBENCHMARKS_DIR/termination-crafted/2Nested-1.c"
  scripts/cpa.sh -setprop cpa.arg.compressWitness=true -setprop counterexample.export.compressWitness=true -setprop termination.compressWitness=true -svcomp23 -timelimit '900 s' -stats -spec "$SVBENCHMARKS_DIR/properties/unreach-call.prp" -32 "$SVBENCHMARKS_DIR/weaver/mult-comm.wvr.c"

)

// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import com.google.protobuf.util.Durations;
import java.io.IOException;
import java.util.List;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.sosy_lab.cpachecker.grpc.proto.Cpachecker;

/** Integration tests for all services implemented in {@link CpacheckerService}. */
interface AllServiceIntegrationTest extends RunnerServiceTest, RunManagerServiceTest {

  @ParameterizedTest(name = "{0}")
  @MethodSource("provideValidRunArguments")
  default void testRun_hasExpectedVerdict(
      String testName, Cpachecker.StartRunRequest request, Cpachecker.Verdict expectedVerdict) {

    // when
    Cpachecker.RunResponse runAtStart = getRunner().startRun(request);
    String runName = runAtStart.getRun().getName();
    Cpachecker.WaitRunRequest waitRequest = getWaitRequest(runName);
    Cpachecker.RunResponse runAfterWait = getRunManager().waitRun(waitRequest);

    // then
    assertThat(runAfterWait.getRun().getStatus(), is(Cpachecker.RunStatus.FINISHED));
    assertThat(
        "Verdict does not match expected verdict.\n"
            + "CPAchecker stdout: "
            + runAfterWait.getRun().getDetails().getStdout()
            + "\nCPAchecker stderr: "
            + runAfterWait.getRun().getDetails().getStderr(),
        runAfterWait.getRun().getResultVerdict(),
        equalTo(expectedVerdict));
  }

  private Cpachecker.WaitRunRequest getWaitRequest(String runName) {
    return Cpachecker.WaitRunRequest.newBuilder()
        .setName(runName)
        .setTimeout(Durations.fromMillis(getWaitTimelimitInMillis()))
        .build();
  }

  static List<Arguments> provideValidRunArguments() throws IOException {
    return RunnerServiceTest.provideValidRequests();
  }
}

// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.oneOf;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.jupiter.api.Assertions.fail;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.getContentAsString;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.value;

import com.google.protobuf.util.Durations;
import io.quarkus.test.junit.QuarkusTest;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.junit.platform.commons.util.Preconditions;
import org.sosy_lab.cpachecker.grpc.proto.Cpachecker;
import org.sosy_lab.cpachecker.grpc.proto.RunManagerGrpc;
import org.sosy_lab.cpachecker.grpc.proto.RunnerGrpc;
import org.sosy_lab.cpachecker.grpc.runs.TestUtils;

@QuarkusTest
interface RunManagerServiceTest {

  Path PROGRAM_FALSE = TestUtils.getRunnerFile("unsafe.c");
  Path PROGRAM_INFINITE_RUN = TestUtils.getRunnerFile("infiniteRun.c");
  Path CONFIG_NO_VERIFICATION = TestUtils.getRunnerFile("generateCFA.properties");

  Path CONFIG_VERIFICATION = TestUtils.getRunnerFile("valueAnalysis.properties");

  Path CONFIG_VALIDATION = TestUtils.getRunnerFile("witnessValidation.properties");
  Path SPECIFICATION = TestUtils.getRunnerFile("unreach-call.spc");
  Path WITNESS_CORRECT = TestUtils.getRunnerFile("violationWitness-correct.graphml");

  String OPTION_CFA_OUTPUT_KEY = "cfa.export";
  String OPTION_CFA_OUTPUT_VALUE = "true";
  String OPTION_CFA_OUTPUT_PATH_KEY = "cfa.file";
  String OPTION_CFA_OUTPUT_PATH_VALUE = "cfa.dot";
  String OPTION_STATISTICS_OUTPUT_KEY = "statistics.export";
  String OPTION_STATISTICS_OUTPUT_VALUE = "true";
  String OPTION_STATISTICS_OUTPUT_PATH_KEY = "statistics.file";
  String OPTION_STATISTICS_OUTPUT_PATH_VALUE = "Statistics.txt";

  long WAIT_FOR_SHORT_RUNNING_RUN_IN_MILLIS = 5_000;

  Cpachecker.CpacheckerOption noOutput =
      Cpachecker.CpacheckerOption.newBuilder()
          .setName("output.disable")
          .setTextValue(value("true"))
          .build();

  String NAME_MISSING_RUN = "foobar";
  int REPEATED_REQUESTS_NUMBER = 10;

  RunnerGrpc.RunnerBlockingStub getRunner();

  RunManagerGrpc.RunManagerBlockingStub getRunManager();

  long getWaitTimelimitInMillis();

  default String startNewRun() {
    Cpachecker.StartRunRequest startRequest = getDefaultStartRequest();
    return startNewRun(startRequest);
  }

  private String startNewRun(Cpachecker.StartRunRequest request) {
    RunnerGrpc.RunnerBlockingStub runner = getRunner();
    Cpachecker.RunResponse runResponse = runner.startRun(request);

    assertThat(runResponse.hasRun(), is(true));
    String nameStartedRun = runResponse.getRun().getName();

    // if the extremely unlikely case happens and the existing run hast the same name as our
    // supposedly missing run, we have to rename NAME_MISSING_RUN
    assertThat(nameStartedRun, not(equalTo(NAME_MISSING_RUN)));
    return nameStartedRun;
  }

  // this run is not really long-running, but long-ish, compared to the default 'startNewRun'. It
  // also produces some additional output on the command-line
  private String startLongRunningRun() throws IOException {
    Cpachecker.StartRunRequest startRequest =
        Cpachecker.StartRunRequest.newBuilder()
            .setProgramCode(getContentAsString(PROGRAM_FALSE))
            .setConfig("") // we only need the additional option below
            .addAdditionalOptions(noOutput) // output slows down shutdown of the run
            .setSpecification(getContentAsString(SPECIFICATION))
            .setMachineModel(Cpachecker.MachineModel.LINUX32)
            .setWitness(getContentAsString(WITNESS_CORRECT))
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName("witness.validation.violation.config")
                    .setFileValue(value(TestUtils.getContentAsBytes(CONFIG_VALIDATION)))
                    .build())
            .build();

    return startNewRun(startRequest);
  }

  private String startRunThatDoesNotStop() {
    Cpachecker.StartRunRequest startRequest =
        Cpachecker.StartRunRequest.newBuilder()
            .setProgramCode(getContentAsString(PROGRAM_INFINITE_RUN))
            .setConfig(getContentAsString(CONFIG_VERIFICATION))
            .setMachineModel(Cpachecker.MachineModel.LINUX32)
            .addAdditionalOptions(noOutput) // output slows down shutdown of the run
            .build();

    return startNewRun(startRequest);
  }

  private Cpachecker.StartRunRequest getDefaultStartRequest() {
    return Cpachecker.StartRunRequest.newBuilder()
        .setProgramCode(getContentAsString(PROGRAM_FALSE))
        .setConfig(getContentAsString(CONFIG_NO_VERIFICATION))
        .addAdditionalOptions(noOutput) // output slows down shutdown of the run
        .setMachineModel(Cpachecker.MachineModel.LINUX32)
        .build();
  }

  private Cpachecker.ListRunsRequest createListRunsRequest() {
    return Cpachecker.ListRunsRequest.newBuilder().build();
  }

  private Cpachecker.WaitRunRequest createWaitRunRequest(String name, long timeoutInMillis) {
    return Cpachecker.WaitRunRequest.newBuilder()
        .setName(name)
        .setTimeout(Durations.fromMillis(timeoutInMillis))
        .build();
  }

  @Test
  default void testGetRun_existingRun() throws InterruptedException {
    // given
    String lastStartedRun = startNewRun();
    // some time for run to actually run
    Thread.sleep(WAIT_FOR_SHORT_RUNNING_RUN_IN_MILLIS);
    Cpachecker.GetRunRequest getRunRequest = createGetRunRequest(lastStartedRun);
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // then
    Cpachecker.RunResponse runResponse = runManager.getRun(getRunRequest);

    // then there should be a run returned
    assertThat(runResponse.hasRun(), is(true));
    Cpachecker.Run run = runResponse.getRun();
    // and the name information should be consistent between request and response
    assertThat(run.getName(), is(lastStartedRun));
    // and the run should have had plenty of time to be started since 'runner.startRun' was called
    assertThat(run.getStatus(), oneOf(Cpachecker.RunStatus.RUNNING, Cpachecker.RunStatus.FINISHED));
  }

  @Test
  default void testGetRun_multipleGetsOnSameRun() throws InterruptedException, IOException {
    // given a long-running run to make it more likely that the stdout and stderr are filled between
    // requests
    String lastStartedRun = startLongRunningRun();
    Cpachecker.GetRunRequest getRunRequest = createGetRunRequest(lastStartedRun);
    final long timeToSleep = WAIT_FOR_SHORT_RUNNING_RUN_IN_MILLIS / REPEATED_REQUESTS_NUMBER;
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // when doing multiple requests for the same run
    List<Cpachecker.RunResponse> responses = new ArrayList<>(REPEATED_REQUESTS_NUMBER);
    for (int i = 0; i < REPEATED_REQUESTS_NUMBER; i++) {
      responses.add(runManager.getRun(getRunRequest));
      // give the run some time to produce output (if it is still running)
      Thread.sleep(timeToSleep);
    }

    // then all responses should have
    for (Cpachecker.RunResponse runResponse : responses) {
      // that valid run
      assertThat(runResponse.hasRun(), is(true));
      Cpachecker.Run run = runResponse.getRun();
      // and the name information should be consistent between request and response
      assertThat(run.getName(), is(lastStartedRun));
      // and the run should have had plenty of time to be started since 'runner.startRun' was called
      assertThat(
          run.getStatus(), oneOf(Cpachecker.RunStatus.RUNNING, Cpachecker.RunStatus.FINISHED));
    }

    // and the stdout and stderr should always grow (or stay the same).
    // we do not expect that stdouts and stderrs are always non-blank here,
    // because the first get request may be so soon after the start of the run that there is no
    // output yet
    List<String> stdouts = new ArrayList<>(REPEATED_REQUESTS_NUMBER);
    List<String> stderrs = new ArrayList<>(REPEATED_REQUESTS_NUMBER);
    for (Cpachecker.RunResponse runResponse : responses) {
      Cpachecker.Run run = runResponse.getRun();
      String stdout = run.getDetails().getStdout();
      String stderr = run.getDetails().getStderr();
      stdouts.add(stdout);
      stderrs.add(stderr);
    }
    for (int i = 0; i < stdouts.size() - 2; i++) {
      String stdout1 = stdouts.get(i);
      String stdout2 = stdouts.get(i + 1);
      assertThat(stdout2, startsWith(stdout1));
    }
    for (int i = 0; i < stderrs.size() - 2; i++) {
      String stderr1 = stderrs.get(i);
      String stderr2 = stderrs.get(i + 1);
      assertThat(stderr2, startsWith(stderr1));
    }
    // we do not check that there actually is some output on stdout/stderr - this is done by other
    // tests.
  }

  @Test
  default void testGetRun_missingRun() {
    // given
    startNewRun();
    Cpachecker.GetRunRequest getRunRequest = createGetRunRequest(NAME_MISSING_RUN);
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // when
    Cpachecker.RunResponse runResponse = runManager.getRun(getRunRequest);

    // then there should be an error
    assertThat(runResponse.hasError(), is(true));
    // and the error should have some sane value
    assertThat(runResponse.getError(), not(blankOrNullString()));
  }

  private Cpachecker.GetRunRequest createGetRunRequest(String name) {
    return Cpachecker.GetRunRequest.newBuilder().setName(name).build();
  }

  @Test
  default void testGetRun_listsSingleOutputFile() throws InterruptedException {
    // given we start a run that exports the CFA to a file
    Cpachecker.StartRunRequest runRequestWithExplicitOutputFile =
        getDefaultStartRequest().toBuilder()
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_CFA_OUTPUT_PATH_KEY)
                    .setTextValue(value(OPTION_CFA_OUTPUT_PATH_VALUE)))
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_CFA_OUTPUT_KEY)
                    .setTextValue(value(OPTION_CFA_OUTPUT_VALUE)))
            .build();
    String runName = startNewRun(runRequestWithExplicitOutputFile);
    // some time for the run to actually run
    Thread.sleep(WAIT_FOR_SHORT_RUNNING_RUN_IN_MILLIS);
    // and a get request for that run
    Cpachecker.GetRunRequest getRunRequest = createGetRunRequest(runName);
    // and our run manager
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // The time might not suffice, retry for 2 more times
    // to reduce flakiness of the test
    // when
    Cpachecker.RunResponse runResponse = runManager.getRun(getRunRequest);
    for (int i = 0; i < 2; i++) {
      if (runResponse.hasRun()
          && runResponse.getRun().getStatus().equals(Cpachecker.RunStatus.FINISHED)) {
        break;
      }
      Thread.sleep(WAIT_FOR_SHORT_RUNNING_RUN_IN_MILLIS);
      runResponse = runManager.getRun(getRunRequest);
    }

    // then the response should contain the CFA file
    Preconditions.condition(
        runResponse.hasRun(), "RunResponse has no run, can't check for output files");
    Preconditions.condition(
        runResponse.getRun().getStatus().equals(Cpachecker.RunStatus.FINISHED),
        "Run is not finished yet, can't reliably check for output files");
    assertThat(
        "Output file not found in response: " + runResponse,
        runResponse.getRun().getDetails().getOutputFilesList(),
        hasItem(OPTION_CFA_OUTPUT_PATH_VALUE));
  }

  @Test
  default void testGetRun_listsMultipleOutputFiles() throws InterruptedException {
    // given we start a run that exports the CFA and the ARG to a file
    Cpachecker.StartRunRequest runRequestWithExplicitOutputFile =
        getDefaultStartRequest().toBuilder()
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_CFA_OUTPUT_PATH_KEY)
                    .setTextValue(value(OPTION_CFA_OUTPUT_PATH_VALUE)))
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_CFA_OUTPUT_KEY)
                    .setTextValue(value(OPTION_CFA_OUTPUT_VALUE)))
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_STATISTICS_OUTPUT_KEY)
                    .setTextValue(value(OPTION_STATISTICS_OUTPUT_VALUE)))
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_STATISTICS_OUTPUT_PATH_KEY)
                    .setTextValue(value(OPTION_STATISTICS_OUTPUT_PATH_VALUE)))
            .build();
    String runName = startNewRun(runRequestWithExplicitOutputFile);
    // some time for the run to actually run
    Thread.sleep(WAIT_FOR_SHORT_RUNNING_RUN_IN_MILLIS);
    // and a get request for that run
    Cpachecker.GetRunRequest getRunRequest = createGetRunRequest(runName);
    // and our run manager
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // when
    Cpachecker.RunResponse runResponse = runManager.getRun(getRunRequest);

    // then the response should contain the CFA file and the ARG file
    Preconditions.condition(
        runResponse.hasRun(), "RunResponse has no run, can't check for output files");
    Preconditions.condition(
        runResponse.getRun().getStatus().equals(Cpachecker.RunStatus.FINISHED),
        "Run is not finished yet, can't reliably check for output files");
    assertThat(
        "Output file not found in response: " + runResponse,
        runResponse.getRun().getDetails().getOutputFilesList(),
        hasItems(OPTION_CFA_OUTPUT_PATH_VALUE, OPTION_STATISTICS_OUTPUT_PATH_VALUE));
  }

  @Test
  default void testListRuns_singleRun() {
    // given
    String lastStartedRun = startNewRun();
    Cpachecker.ListRunsRequest listRunsRequest = createListRunsRequest();
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // when
    Cpachecker.ListRunsResponse listRunsResponse = runManager.listRuns(listRunsRequest);

    // then the list of the response should contain a single run
    assertThat(listRunsResponse.getRunsList().size(), greaterThanOrEqualTo(1));
    // and the run that we just started is included
    assertThat(listRunsResponse.getRunsList(), hasItem(hasProperty("name", is(lastStartedRun))));
  }

  @Test
  default void testListRuns_multipleRuns() {
    // given multiple runs are started
    for (int i = 0; i < REPEATED_REQUESTS_NUMBER; i++) {
      startNewRun();
    }
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // when
    Cpachecker.ListRunsResponse listRunsResponse = runManager.listRuns(createListRunsRequest());

    // then the list of the response should contain all runs
    assertThat(
        listRunsResponse.getRunsList().size(), greaterThanOrEqualTo(REPEATED_REQUESTS_NUMBER));
    // and each run has a unique name
    Set<String> runNames = new HashSet<>();
    for (Cpachecker.Run run : listRunsResponse.getRunsList()) {
      assertThat(runNames, not(hasItem(run.getName())));
      runNames.add(run.getName());
    }
  }

  @Test
  default void testCancelRun_existingRun() throws InterruptedException {
    // given one run is started
    String lastStartedRun = startRunThatDoesNotStop();
    Cpachecker.CancelRunRequest cancelRunRequest = createCancelRunRequest(lastStartedRun);
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // when
    final Cpachecker.RunResponse cancelRunResponse = runManager.cancelRun(cancelRunRequest);
    // give the process some time to stop
    Thread.sleep(getWaitTimelimitInMillis());
    final Cpachecker.RunResponse latestState =
        runManager.getRun(createGetRunRequest(lastStartedRun));

    // then the response should have a valid run
    assertThat(cancelRunResponse.hasRun(), is(true));
    // with the name of the started run
    assertThat(cancelRunResponse.getRun().getName(), is(lastStartedRun));
    // and the run was still running when destroy was called (this shows that this is a soft
    // termination)
    assertThat(cancelRunResponse.getRun().getStatus(), is(Cpachecker.RunStatus.RUNNING));
    // but finally the run has terminated
    assertThat(latestState.getRun().getStatus(), is(Cpachecker.RunStatus.FINISHED));
  }

  @Test
  default void testCancelRun_missingRun() {
    // given
    Cpachecker.CancelRunRequest cancelRunRequest = createCancelRunRequest(NAME_MISSING_RUN);
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // when
    Cpachecker.RunResponse cancelRunResponse = runManager.cancelRun(cancelRunRequest);

    // then the response has an error
    assertThat(cancelRunResponse.hasError(), is(true));
    // and the error is a sane message
    assertThat(cancelRunResponse.getError(), not(blankOrNullString()));
  }

  private Cpachecker.CancelRunRequest createCancelRunRequest(String name) {
    return Cpachecker.CancelRunRequest.newBuilder().setName(name).build();
  }

  @Test
  default void testWaitRun_missingRun() {
    // given
    Cpachecker.WaitRunRequest waitRunRequest =
        createWaitRunRequest(NAME_MISSING_RUN, getWaitTimelimitInMillis());
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // when
    Cpachecker.RunResponse waitRunResponse = runManager.waitRun(waitRunRequest);

    // then the response has an error
    assertThat(waitRunResponse.hasError(), is(true));
    // and the error is a sane message
    assertThat(waitRunResponse.getError(), not(blankOrNullString()));
  }

  @Test
  default void testWaitRun_existingTerminatingRun() throws IOException {
    // given one run is started
    String lastStartedRun = startLongRunningRun();
    Cpachecker.WaitRunRequest waitRunRequest =
        createWaitRunRequest(lastStartedRun, getWaitTimelimitInMillis());
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // when
    final Cpachecker.RunResponse waitRunResponse = runManager.waitRun(waitRunRequest);

    // then the response should have a valid run
    assertThat(waitRunResponse.hasRun(), is(true));
    // with the name of the started run
    assertThat(waitRunResponse.getRun().getName(), is(lastStartedRun));
    // and the run is finished
    assertThat(waitRunResponse.getRun().getStatus(), is(Cpachecker.RunStatus.FINISHED));
  }

  @Test
  default void testWaitRun_existingFinishedRun() {
    // given one run is started
    String lastStartedRun = startNewRun();
    Cpachecker.WaitRunRequest waitRunRequest =
        createWaitRunRequest(lastStartedRun, getWaitTimelimitInMillis());
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // when
    final Cpachecker.RunResponse waitRunResponse1 = runManager.waitRun(waitRunRequest);
    final Cpachecker.RunResponse waitRunResponse2 = runManager.waitRun(waitRunRequest);

    // then the response should have a valid run
    assertThat(waitRunResponse1.hasRun(), is(true));
    // with the name of the started run
    assertThat(waitRunResponse1.getRun().getName(), is(lastStartedRun));
    // and the run is finished
    assertThat(waitRunResponse1.getRun().getStatus(), is(Cpachecker.RunStatus.FINISHED));
    // and a consecutive wait produces the same state
    assertThat(waitRunResponse2.getRun(), is(waitRunResponse1.getRun()));

    // and the run should have the details filled, and there should be some output on both stderr
    // and stdout (because CPAchecker uses both).
    assertThat(waitRunResponse1.getRun().getDetails().getStderr(), not(blankOrNullString()));
    assertThat(waitRunResponse2.getRun().getDetails().getStdout(), not(blankOrNullString()));
  }

  @Test
  default void testWaitRun_existingCancelledRun() {
    // given an infinite run is started
    String lastStartedRun = startRunThatDoesNotStop();
    Cpachecker.WaitRunRequest waitRunRequest =
        createWaitRunRequest(lastStartedRun, getWaitTimelimitInMillis());
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();
    // that was cancelled
    final Cpachecker.RunResponse cancelResponse =
        runManager.cancelRun(createCancelRunRequest(lastStartedRun));

    // when we wait for the run to finish
    final Cpachecker.RunResponse waitRunResponse = runManager.waitRun(waitRunRequest);

    // then the run was not finished at the cancel
    assertThat(cancelResponse.getRun().getStatus(), is(Cpachecker.RunStatus.RUNNING));
    // but the wait waited for it to be finished
    assertThat(waitRunResponse.getRun().getStatus(), is(Cpachecker.RunStatus.FINISHED));
    // and the names are still the same as in the request
    assertThat(waitRunResponse.getRun().getName(), is(cancelResponse.getRun().getName()));
    assertThat(waitRunResponse.getRun().getName(), is(lastStartedRun));
  }

  @Test
  default void testWaitRun_existingRunTimeout() {
    // given an infinite run is started
    String lastStartedRun = startRunThatDoesNotStop();
    Cpachecker.WaitRunRequest waitRunRequest =
        createWaitRunRequest(lastStartedRun, 1 /* millisecond */);
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // when we wait for the run to finish
    final Cpachecker.RunResponse waitRunResponse = runManager.waitRun(waitRunRequest);

    // then the run was not finished with the wait
    assertThat(waitRunResponse.getRun().getStatus(), is(Cpachecker.RunStatus.RUNNING));
  }

  @Test
  default void testGetFile_noFileReturnsEmptyResult() throws InterruptedException {
    // given we start a run that exports the CFA to a file
    Cpachecker.StartRunRequest runRequestWithExplicitOutputFile =
        getDefaultStartRequest().toBuilder()
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_CFA_OUTPUT_PATH_KEY)
                    .setTextValue(value(OPTION_CFA_OUTPUT_PATH_VALUE)))
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_CFA_OUTPUT_KEY)
                    .setTextValue(value(OPTION_CFA_OUTPUT_VALUE)))
            .build();
    String runName = startNewRun(runRequestWithExplicitOutputFile);
    // some time for the run to actually run
    Thread.sleep(WAIT_FOR_SHORT_RUNNING_RUN_IN_MILLIS);
    // and a get-file request for that run, but with no file names
    Cpachecker.GetFileRequest getFileRequest =
        Cpachecker.GetFileRequest.newBuilder().setRunName(runName).build();
    // and our run manager
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // when
    Cpachecker.GetFileResponse fileResponse = runManager.getFile(getFileRequest);

    // then the response should be valid, but contain no result
    assertThat(fileResponse.getResultCount(), is(0));
  }

  @Test
  default void testGetFile_singleFileThatExists() throws InterruptedException {
    // given we start a run that exports the CFA to a file
    Cpachecker.StartRunRequest runRequestWithExplicitOutputFile =
        getDefaultStartRequest().toBuilder()
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_CFA_OUTPUT_PATH_KEY)
                    .setTextValue(value(OPTION_CFA_OUTPUT_PATH_VALUE)))
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_CFA_OUTPUT_KEY)
                    .setTextValue(value(OPTION_CFA_OUTPUT_VALUE)))
            .build();
    String runName = startNewRun(runRequestWithExplicitOutputFile);
    // some time for the run to actually run
    Thread.sleep(WAIT_FOR_SHORT_RUNNING_RUN_IN_MILLIS);
    // and a get-file request for that run with the CFA file name
    Cpachecker.GetFileRequest getFileRequest =
        Cpachecker.GetFileRequest.newBuilder()
            .setRunName(runName)
            .addFileName(OPTION_CFA_OUTPUT_PATH_VALUE)
            .build();
    // and our run manager
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // when
    Cpachecker.GetFileResponse fileResponse = runManager.getFile(getFileRequest);

    // then the response should contain the CFA file
    assertThat(fileResponse.getResultCount(), is(1));
    Cpachecker.MaybeFile file = fileResponse.getResult(0);
    assertThat(file.getFileName(), is(OPTION_CFA_OUTPUT_PATH_VALUE));
    assertThat(file.getContent(), notNullValue());
    assertThat(file.getContent().size(), greaterThan(0));
  }

  @Test
  default void testGetFile_multipleFilesThatExit() throws InterruptedException {
    // given we start a run that exports the CFA and the Statistics to a file
    Cpachecker.StartRunRequest runRequestWithExplicitOutputFile =
        getDefaultStartRequest().toBuilder()
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_CFA_OUTPUT_PATH_KEY)
                    .setTextValue(value(OPTION_CFA_OUTPUT_PATH_VALUE)))
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_CFA_OUTPUT_KEY)
                    .setTextValue(value(OPTION_CFA_OUTPUT_VALUE)))
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_STATISTICS_OUTPUT_KEY)
                    .setTextValue(value(OPTION_STATISTICS_OUTPUT_VALUE)))
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_STATISTICS_OUTPUT_PATH_KEY)
                    .setTextValue(value(OPTION_STATISTICS_OUTPUT_PATH_VALUE)))
            .build();
    String runName = startNewRun(runRequestWithExplicitOutputFile);
    // some time for the run to actually run
    Thread.sleep(WAIT_FOR_SHORT_RUNNING_RUN_IN_MILLIS);
    // and a get-file request for that run with the CFA file name and the Statistics file name
    Cpachecker.GetFileRequest getFileRequest =
        Cpachecker.GetFileRequest.newBuilder()
            .setRunName(runName)
            .addFileName(OPTION_CFA_OUTPUT_PATH_VALUE)
            .addFileName(OPTION_STATISTICS_OUTPUT_PATH_VALUE)
            .build();
    // and our run manager
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // when
    Cpachecker.GetFileResponse fileResponse = runManager.getFile(getFileRequest);

    // then there should be two result files
    assertThat(fileResponse.getResultCount(), is(2));
    Set<String> fileNames = new HashSet<>();
    for (Cpachecker.MaybeFile file : fileResponse.getResultList()) {
      fileNames.add(file.getFileName());
      // all result files should have a valid content
      assertThat(file.getContent(), notNullValue());
      assertThat(file.getContent().size(), greaterThan(0));
    }
    // and the result file names should be the ones we asked for
    assertThat(
        fileNames,
        containsInAnyOrder(OPTION_CFA_OUTPUT_PATH_VALUE, OPTION_STATISTICS_OUTPUT_PATH_VALUE));
  }

  @Test
  default void testGetFile_multipleFilesOneExistsOneMissing() throws InterruptedException {
    // given we start a run that exports the CFA to a file, but not the Statistics
    Cpachecker.StartRunRequest runRequestWithExplicitOutputFile =
        getDefaultStartRequest().toBuilder()
            // make sure no other outputs are written
            .addAdditionalOptions(noOutput)
            // only CFA file(s)
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_CFA_OUTPUT_PATH_KEY)
                    .setTextValue(value(OPTION_CFA_OUTPUT_PATH_VALUE)))
            .addAdditionalOptions(
                Cpachecker.CpacheckerOption.newBuilder()
                    .setName(OPTION_CFA_OUTPUT_KEY)
                    .setTextValue(value(OPTION_CFA_OUTPUT_VALUE)))
            .build();
    String runName = startNewRun(runRequestWithExplicitOutputFile);
    // some time for the run to actually run
    Thread.sleep(WAIT_FOR_SHORT_RUNNING_RUN_IN_MILLIS);
    // and a get-file request for that run with the existing CFA file name and the missing
    // Statistics file name
    Cpachecker.GetFileRequest getFileRequest =
        Cpachecker.GetFileRequest.newBuilder()
            .setRunName(runName)
            .addFileName(OPTION_CFA_OUTPUT_PATH_VALUE)
            .addFileName(OPTION_STATISTICS_OUTPUT_PATH_VALUE)
            .build();
    // and our run manager
    RunManagerGrpc.RunManagerBlockingStub runManager = getRunManager();

    // when
    Cpachecker.GetFileResponse fileResponse = runManager.getFile(getFileRequest);

    // then there should be two results
    assertThat(fileResponse.getResultCount(), is(2));
    for (Cpachecker.MaybeFile file : fileResponse.getResultList()) {
      String fileName = file.getFileName();
      if (fileName.equals(OPTION_CFA_OUTPUT_PATH_VALUE)) {
        // the CFA file should be there
        assertThat(file.getContent(), notNullValue());
        assertThat(file.getContent().size(), greaterThan(0));
      } else if (fileName.equals(OPTION_STATISTICS_OUTPUT_PATH_VALUE)) {
        // the Statistics file should not be there
        assertThat(file.getError(), not(blankOrNullString()));
      } else {
        fail("Unexpected file name: " + fileName);
      }
    }
  }
}

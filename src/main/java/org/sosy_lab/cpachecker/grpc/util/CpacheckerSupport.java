// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.util;

import com.google.common.collect.ImmutableList;
import jakarta.inject.Singleton;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jboss.logging.Logger;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.runs.RunConfig;
import org.sosy_lab.cpachecker.grpc.runs.RunResult;

/** Utility for working with CPAchecker. */
@Singleton
public class CpacheckerSupport {

  private static final Logger LOG = Logger.getLogger(CpacheckerSupport.class);
  private static final String GROUP_NAME_REGEX_VERIFICATION_RESULT = "verificationResult";
  private static final String GROUP_NAME_REGEX_FINISHED_WITHOUT_VERIFICATION =
      "doneWithoutVerification";
  private static final String GROUP_NAME_REGEX_ERROR_MESSAGE = "errorMessage";

  private final CpacheckerCliOptions cliOptions = new CpacheckerCliOptions();

  /**
   * Regular-expression pattern that looks for a verification verdict in the output of CPAchecker.
   *
   * <p>When matching this pattern, two named groups are available through {@link
   * java.util.regex.Matcher#group(String)}:
   *
   * <ul>
   *   <li>{@link #GROUP_NAME_REGEX_FINISHED_WITHOUT_VERIFICATION} is only matched if CPAchecker
   *       finished without running any verification. In this case, the group's content has no
   *       specific meaning.
   *   <li>{@link #GROUP_NAME_REGEX_VERIFICATION_RESULT} is only matched if CPAchecker ran a
   *       verification. In this case, the group's content contains the verification verdict.
   * </ul>
   */
  private final Pattern verificationResultRegex =
      Pattern.compile(
          "^((?<"
              + GROUP_NAME_REGEX_FINISHED_WITHOUT_VERIFICATION
              + ">Finished.$)|Verification result: (?<"
              + GROUP_NAME_REGEX_VERIFICATION_RESULT
              + ">\\w+))",
          Pattern.MULTILINE);

  private final Pattern verificationErrorRegex =
      Pattern.compile(
          "^(Error|Invalid configuration): (?<" + GROUP_NAME_REGEX_ERROR_MESSAGE + ">.+)$",
          Pattern.MULTILINE);

  /**
   * Checks the given run config for validity and returns all found issues.
   *
   * @param request the run request to check
   */
  public List<String> getIssuesWithRequest(StartRun request) {
    List<String> reasons = new ArrayList<>(5);
    if (request.machineModel() == StartRun.MachineModel.UNRECOGNIZED) {
      reasons.add("Machine model unrecognized");
    }
    if (request.machineModel() == StartRun.MachineModel.UNSPECIFIED) {
      reasons.add("Machine model unspecified");
    }
    if (request.programCode().isBlank()) {
      reasons.add("No program code");
    }
    if (request.config().isBlank()
        && request.fileOptions().isEmpty()
        && request.textOptions().isEmpty()) {
      reasons.add("No configuration");
    }
    return reasons;
  }

  /**
   * Creates command-line options that are compatible with CPAchecker's command-line interface.
   *
   * @param config the run configuration
   * @param outputDirectory the directory where CPAchecker should write its output
   * @return the command-line options
   */
  public ImmutableList<String> createCommandLineOptions(RunConfig config, Path outputDirectory) {
    return cliOptions.createCommandLineOptions(config, outputDirectory);
  }

  /**
   * Parses a CPAchecker execution and returns the run result.
   *
   * @param stdout the output of CPAchecker on stdout
   * @param stderr the output of CPAchecker on stderr
   * @return the result of the CPAchecker run.
   */
  public RunResult parseExecutionResult(String stdout, String stderr) {
    return new RunResult(parseVerdict(stdout, stderr));
  }

  private RunResult.Verdict parseVerdict(String stdout, String stderr) {
    // First try to get a valid verification result from CPAchecker's stdout
    Matcher resultMatcher = verificationResultRegex.matcher(stdout);
    if (resultMatcher.find()) {
      String verificationResult = resultMatcher.group(GROUP_NAME_REGEX_VERIFICATION_RESULT);
      if (verificationResult != null) {
        return switch (verificationResult) {
          case "TRUE" -> RunResult.Verdict.TRUE;
          case "FALSE" -> RunResult.Verdict.FALSE;
          case "UNKNOWN" -> RunResult.Verdict.UNKNOWN;
          default -> throw new AssertionError(
              "Unhandled run result in output: " + verificationResult);
        };
      } else {
        assert resultMatcher.group(GROUP_NAME_REGEX_FINISHED_WITHOUT_VERIFICATION) != null
            : "The pattern to determine CPAchecker's run result did match successfully, but none of"
                + " the checked groups matched. This hints to a wrong pattern or a missing"
                + " handling of one of the pattern's groups.";
        return RunResult.Verdict.DONE;
      }
    }
    // If no verification result was found in CPAchecker's stdout, check if CPAchecker's stderr
    // contains some error message
    Matcher errorMatcher = verificationErrorRegex.matcher(stderr);
    if (errorMatcher.find()) {
      LOG.debugf(
          "CPAchecker reported an error: %s", errorMatcher.group(GROUP_NAME_REGEX_ERROR_MESSAGE));
      return RunResult.Verdict.ERROR;
    }

    LOG.info("Could not determine CPAchecker's run result.");
    return RunResult.Verdict.UNKNOWN;
  }
}

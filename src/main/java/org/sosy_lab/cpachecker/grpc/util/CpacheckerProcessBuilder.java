// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.util;

import com.google.common.collect.ImmutableList;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import org.jboss.logging.Logger;
import org.sosy_lab.cpachecker.grpc.CpacheckerService;
import org.sosy_lab.cpachecker.grpc.runs.RunException;

/** Builder for {@link CpacheckerProcess}. Allows to spawn new processes. */
@Singleton
public class CpacheckerProcessBuilder {

  private static final Logger LOG = Logger.getLogger(CpacheckerService.class);
  private final Async async;

  /**
   * Creates a new instance of this class.
   *
   * @param async utility to run asynchronous tasks
   */
  @Inject
  public CpacheckerProcessBuilder(Async async) {
    this.async = async;
  }

  /**
   * Creates a new {@link CpacheckerProcess} by running the given command. The command must include
   * the CPAchecker executable. This method is non-blocking.
   *
   * <p>Example command:
   *
   * <pre>["scripts/cpa.sh", "-config", "config/generateCFA.properties", "-noout", "programFile.c"]
   * </pre>
   *
   * @param command the command to run CPAchecker through, given as a list of parameters.
   * @return the spawned CPAchecker process.
   * @throws RunException if there are issues executing the given command
   */
  public CpacheckerProcess create(ImmutableList<String> command) throws RunException {
    try {
      ProcessBuilder processBuilder = new ProcessBuilder(command);
      LOG.debugf("Executing the following command: %s", command);
      Process systemProcess = processBuilder.start();
      CpacheckerProcess.OutputReader stdoutReader =
          new CpacheckerProcess.OutputReader(
              "stdout", () -> systemProcess.inputReader(StandardCharsets.UTF_8), async);
      CpacheckerProcess.OutputReader stderrReader =
          new CpacheckerProcess.OutputReader(
              "stderr", () -> systemProcess.errorReader(StandardCharsets.UTF_8), async);
      stdoutReader.startReading();
      stderrReader.startReading();
      CpacheckerProcess process = new CpacheckerProcess(systemProcess, stdoutReader, stderrReader);
      // We may change the implementation of CpacheckerProcess#getId
      // in the future, so make sure that we always output
      // the CpacheckerProcess#getId(), and do not accidentally
      // expose the internal system-process pid.
      LOG.debugf("Started process: %s", process.getId());
      return process;
    } catch (IOException e) {
      LOG.errorf(e, "Error executing CPAchecker with command: %s", command);
      throw new RunException("Error executing CPAchecker: " + command, e);
    }
  }
}

// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.util;

import jakarta.enterprise.context.ApplicationScoped;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Comparator;
import java.util.Objects;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import lombok.Getter;

/** Manager for working directories of runs. */
@ApplicationScoped // use a single DirectoryManager across the application
public class DirectoryManager {
  private Path workDir;
  private long runCounter = 0;

  /**
   * Creates a new {@link RunDirectory}. This class may delete the directory after use.
   *
   * @return a new RunDirectory
   * @throws IOException if directory creation fails
   */
  public synchronized RunDirectory createNewRunDirectory() throws IOException {
    if (workDir == null) {
      workDir = Files.createTempDirectory("cpa-daemon-");
      workDir.toFile().deleteOnExit();
    }
    runCounter++;
    Path currentRunDirectory = workDir.resolve("run-" + runCounter);
    return new RunDirectory(currentRunDirectory);
  }

  /**
   * A temporary run directory for use in try-with-resources. The directory is deleted when closed.
   */
  public static class RunDirectory implements Closeable {

    public static final String CONFIG_FILES_ZIP = "/META-INF/resources/configFiles.zip";
    private final Path directory;
    @Getter private Path configLocation;

    private RunDirectory(Path directory) throws IOException {
      this.directory = directory;
      if (directory.toFile().exists()) {
        throw new IOException(
            "Newly requested run directory already exists, but shouldn't. This will likely override"
                + " other files.");
      }
      Files.createDirectories(directory);
      // we create all directories here so that only this method has to throw an IOException
      Path currentOutputDirectory = getOutputDirPath(directory);
      Files.createDirectories(currentOutputDirectory);
      Path currentInputDirectory = getInputDirPath(directory);
      Files.createDirectories(currentInputDirectory);
      configLocation = currentInputDirectory.resolve("config.properties");
    }

    /** Returns the output directory in this run directory. */
    public Path getCurrentOutputDirectory() {
      Path outputDir = getOutputDirPath(directory);
      // use assert so that this expensive I/O check does not occur in production
      assert outputDir.toFile().exists();
      return outputDir;
    }

    /** Returns the input directory in this run directory. */
    public Path getCurrentInputDirectory() {
      Path inputDir = getInputDirPath(directory);
      // use assert so that this expensive I/O check does not occur in production
      assert inputDir.toFile().exists();
      return inputDir;
    }

    /** Returns a new temporary logging file in the output directory of this run directory. */
    public Path getNewLoggingFile() throws IOException {
      return Files.createTempFile(getCurrentOutputDirectory(), "CPAchecker", ".log");
    }

    private Path getOutputDirPath(Path parentDir) {
      // don't use a constant for this String so that people don't use the constant
      // instead of this method
      return parentDir.resolve("output");
    }

    private Path getInputDirPath(Path parentDir) {
      // don't use a constant for this String so that people don't use the constant
      // instead of this method
      return parentDir.resolve("input");
    }

    /**
     * Unpacks the zipped configs of CPAchecker into the input directory of this run directory. The
     * zipped configs have `config` as their top level folder. This assumption must hold for this
     * method to work.
     *
     * @throws IOException if an I/O error occurs while unpacking the configs
     */
    public void makeCpacheckerConfigsAvailable() throws IOException {
      try (InputStream resourceURL =
              Objects.requireNonNull(getClass().getResourceAsStream(CONFIG_FILES_ZIP));
          ZipInputStream zis = new ZipInputStream(resourceURL)) {

        final Path outputDir = getCurrentInputDirectory();
        ZipEntry entry;
        while ((entry = zis.getNextEntry()) != null) {
          Path toPath = outputDir.resolve(entry.getName());

          if (entry.isDirectory()) {
            Files.createDirectories(toPath);
            continue;
          }

          if (toPath.getParent() != null && Files.notExists(toPath.getParent())) {
            Files.createDirectories(toPath.getParent());
          }

          Files.copy(zis, toPath, StandardCopyOption.REPLACE_EXISTING);
        }
        zis.closeEntry();
      }
      Path configDir = getCurrentInputDirectory().resolve("config");
      configLocation = Files.createTempFile(configDir, "config", ".properties");
    }

    @Override
    public void close() throws IOException {
      // remove directory and all its content recursively
      try (Stream<Path> streamFiles = Files.walk(directory)) {
        streamFiles.sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
      }
    }

    @Override
    public String toString() {
      return directory.toString();
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) {
        return true;
      }
      if (o == null || getClass() != o.getClass()) {
        return false;
      }
      RunDirectory that = (RunDirectory) o;
      return Objects.equals(directory, that.directory);
    }

    @Override
    public int hashCode() {
      return Objects.hash(directory);
    }
  }
}

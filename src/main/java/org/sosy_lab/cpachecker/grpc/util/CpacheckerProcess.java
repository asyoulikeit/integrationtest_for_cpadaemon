// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.util;

import com.google.common.base.Preconditions;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import org.jboss.logging.Logger;

/** A CPAchecker process. */
public class CpacheckerProcess {

  private final Process process;
  private final OutputReader stdout;
  private final OutputReader stderr;

  /**
   * Creates a new instance of this class.
   *
   * @param process the fully configured process as a {@link ProcessBuilder}
   */
  CpacheckerProcess(Process process, OutputReader stdout, OutputReader stderr) {
    this.process = process;
    this.stdout = stdout;
    this.stderr = stderr;
  }

  /** Returns the unique ID of this process. */
  public String getId() {
    return String.valueOf(process.pid());
  }

  /** Destroys this process. */
  public void destroy() {
    process.destroy();
  }

  /**
   * Waits on this process. The method blocks until the process has terminated or the given
   * timelimit has passed.
   *
   * @param timelimitInMillis time limit to wait for the process to terminate, in milliseconds
   * @throws InterruptedException if the Thread is interrupted while waiting.
   */
  public void waitFor(long timelimitInMillis) throws InterruptedException {
    boolean processFinished = process.waitFor(timelimitInMillis, TimeUnit.MILLISECONDS);
    if (processFinished) {
      // if the process finished in time, we wait indefinitely on the stdout and stderr to be read
      // completely. This may go above the given time limit, but the imprecision shouldn't be too
      // big and it is the only sane way to not overcomplicate the time measurement.
      stdout.waitFor();
      stderr.waitFor();
    }
  }

  /** Returns the last polled state of stdout. */
  public String getCurrentStdout() {
    return stdout.readString();
  }

  /** Returns the last polled state of stderr. */
  public String getCurrentStderr() {
    return stderr.readString();
  }

  /** Returns whether this process is finished. */
  public boolean isDone() {
    return !process.isAlive() && stdout.isDone() && stderr.isDone();
  }

  /**
   * Reader for process output. When started, this class initializes the {@link BufferedReader} that
   * was provided on construction and reads from it until it is closed.
   */
  static class OutputReader {
    private static final Logger LOG = Logger.getLogger(OutputReader.class);
    private final StringBuilder currentOutput = new StringBuilder();

    private final String name;
    private final Supplier<BufferedReader> getReader;
    private final Async async;

    private CompletableFuture<Void> readFuture;

    OutputReader(String name, Supplier<BufferedReader> getReader, Async async) {
      this.name = name;
      this.getReader = getReader;
      this.async = async;
    }

    /**
     * Start reading from the {@link BufferedReader} that is provided on construction. This call is
     * non-blocking.
     */
    void startReading() {
      Preconditions.checkState(readFuture == null);
      readFuture = async.startWorker(this::run);
    }

    private void run() {
      try (BufferedReader reader = getReader.get()) {
        // this continuously reads from the process until it is closed
        reader.lines().forEach(line -> currentOutput.append(line).append("\n"));
        LOG.debugf("Finished reading: %s", name);

      } catch (UncheckedIOException | IOException e) {
        // this is expected to happen when the process is closed before it is done
        LOG.debugf(
            "Error reading %s. We silently ignore the error and stop reading. Error: %s",
            name, e.getMessage());
      }
    }

    String readString() {
      // this operation is not thread-safe: stdout may be written to by the process while
      // we turn it into a string. But we do not care about the latest synchronized state because
      // we do not promise the caller anything.
      return currentOutput.toString();
    }

    /** Returns whether reading is done. */
    boolean isDone() {
      return readFuture.isDone();
    }

    /** Waits for reading to finish indefinitely. */
    void waitFor() throws InterruptedException {
      try {
        readFuture.get();
      } catch (ExecutionException e) {
        LOG.debugf(
            "Error reading %s. We silently ignore the error. Error: %s", name, e.getMessage());
      }
    }
  }
}

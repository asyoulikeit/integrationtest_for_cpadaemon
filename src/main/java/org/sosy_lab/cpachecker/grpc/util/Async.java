// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.util;

import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.infrastructure.Infrastructure;
import io.smallrye.mutiny.unchecked.Unchecked;
import jakarta.inject.Singleton;
import java.time.Duration;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;

/** Utility for asynchronous execution. */
@Singleton
public class Async {

  /**
   * Starts the given runnable on a worker thread.
   *
   * @param r runnable
   * @return a future for the runnable
   */
  public CompletableFuture<Void> startWorker(Runnable r) {
    return Uni.createFrom()
        .<Void>item(
            () -> {
              r.run();
              return null;
            })
        .runSubscriptionOn(Infrastructure.getDefaultWorkerPool())
        .subscribe()
        .asCompletionStage();
  }

  /**
   * Starts the given callable on a worker thread.
   *
   * @param c callable
   * @param <T> the type of the result of the callable
   * @return a future for the callable
   */
  public <T> CompletableFuture<T> startWorker(Callable<T> c) {
    return Uni.createFrom()
        .item(Unchecked.supplier(c::call))
        .ifNoItem()
        .after(Duration.ofMinutes(5))
        .fail()
        .runSubscriptionOn(Infrastructure.getDefaultWorkerPool())
        .onFailure()
        .transform(Throwable::getCause)
        .subscribe()
        .asCompletionStage();
  }

  /**
   * Creates a future that automatically fails.
   *
   * @param t the exception that should be thrown
   * @param <T> the type of the value
   * @return a future that is already completed with the given exception
   */
  public <T> CompletableFuture<T> failure(Throwable t) {
    return Uni.createFrom()
        .<T>failure(t)
        .runSubscriptionOn(Infrastructure.getDefaultWorkerPool())
        .subscribe()
        .asCompletionStage();
  }
}

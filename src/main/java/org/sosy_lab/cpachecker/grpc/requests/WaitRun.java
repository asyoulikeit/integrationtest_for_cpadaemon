// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.requests;

import java.time.Duration;

/**
 * Request to wait for a run to finish.
 *
 * @param name the name of the run to wait for
 * @param timeout the maximum time to wait for the run to finish
 */
public record WaitRun(String name, Duration timeout) {}

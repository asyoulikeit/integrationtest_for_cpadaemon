// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.requests;

/**
 * Request to get an output file from a run.
 *
 * @param runName name of the run
 * @param fileName name of the output file
 */
public record GetFile(String runName, String fileName) {}

// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc;

import com.google.protobuf.ByteString;
import com.google.protobuf.util.Durations;
import io.quarkus.grpc.GrpcService;
import io.smallrye.common.annotation.Blocking;
import io.smallrye.mutiny.Uni;
import jakarta.annotation.Nullable;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.List;
import java.util.Optional;
import org.sosy_lab.cpachecker.grpc.proto.Cpachecker;
import org.sosy_lab.cpachecker.grpc.proto.RunManager;
import org.sosy_lab.cpachecker.grpc.requests.CancelRun;
import org.sosy_lab.cpachecker.grpc.requests.CloseRun;
import org.sosy_lab.cpachecker.grpc.requests.GetFile;
import org.sosy_lab.cpachecker.grpc.requests.GetRun;
import org.sosy_lab.cpachecker.grpc.requests.ListRuns;
import org.sosy_lab.cpachecker.grpc.requests.WaitRun;
import org.sosy_lab.cpachecker.grpc.runs.RunState;

/** gRPC service for managing existing CPAchecker runs. */
@GrpcService
public class RunManagerService implements RunManager {

  private final org.sosy_lab.cpachecker.grpc.runs.RunManager runManager;
  private final RunResponseMapper resultMapper;

  /**
   * Creates a new instance of this class.
   *
   * @param runManager the manager for CPAchecker runs
   */
  RunManagerService(
      org.sosy_lab.cpachecker.grpc.runs.RunManager runManager, RunResponseMapper resultMapper) {
    this.runManager = runManager;
    this.resultMapper = resultMapper;
  }

  @Override
  public Uni<Cpachecker.RunResponse> getRun(Cpachecker.GetRunRequest request) {
    GetRun getRequest = new GetRun(request.getName());
    return Uni.createFrom().optional(runManager.getRun(getRequest)).map(this::toDetailedResponse);
  }

  @Override
  public Uni<Cpachecker.ListRunsResponse> listRuns(Cpachecker.ListRunsRequest request) {
    return Uni.createFrom()
        .item(runManager.listRuns(new ListRuns()))
        .map(resultMapper::toListRunsResponse);
  }

  @Override
  public Uni<Cpachecker.RunResponse> cancelRun(Cpachecker.CancelRunRequest request) {
    CancelRun cancelRequest = new CancelRun(request.getName());
    return Uni.createFrom()
        .optional(runManager.cancelRun(cancelRequest))
        .map(this::toDetailedResponse);
  }

  @Override
  public Uni<Cpachecker.Void> closeRun(Cpachecker.CloseRunRequest request) {
    CloseRun closeRequest = new CloseRun(request.getName());
    return Uni.createFrom().item(runManager.closeRun(closeRequest));
  }

  @Override
  @Blocking
  public Uni<Cpachecker.RunResponse> waitRun(Cpachecker.WaitRunRequest request) {
    // we keep millisecond precision. No need to have nano precision, as our internal process
    // implementation does not support that.
    Duration timeout = Duration.ofMillis(Durations.toMillis(request.getTimeout()));
    return Uni.createFrom()
        .optional(runManager.waitRun(new WaitRun(request.getName(), timeout)))
        .map(this::toDetailedResponse);
  }

  @Override
  @Blocking
  public Uni<Cpachecker.GetFileResponse> getFile(Cpachecker.GetFileRequest request) {
    List<GetFile> fileRequests =
        request.getFileNameList().stream()
            .map(fileName -> new GetFile(request.getRunName(), fileName))
            .toList();

    Cpachecker.GetFileResponse.Builder builder = Cpachecker.GetFileResponse.newBuilder();
    for (GetFile fileRequest : fileRequests) {
      builder.addResult(getFile(fileRequest));
    }

    return Uni.createFrom().item(builder.build());
  }

  private Cpachecker.MaybeFile getFile(GetFile fileRequest) {
    Optional<ByteBuffer> content = runManager.getOutputFile(fileRequest);

    Cpachecker.MaybeFile.Builder result = Cpachecker.MaybeFile.newBuilder();
    result.setFileName(fileRequest.fileName());
    result.setRunName(fileRequest.runName());
    if (content.isEmpty()) {
      result.setError("Run or file not found.");
    } else {
      result.setContent(ByteString.copyFrom(content.get()));
    }
    return result.build();
  }

  private Cpachecker.RunResponse toDetailedResponse(@Nullable RunState state) {
    Cpachecker.RunResponse.Builder builder = Cpachecker.RunResponse.newBuilder();
    if (state == null) {
      builder.setError("Run not found.");
    } else {
      Cpachecker.Run run = resultMapper.toRun(state);
      run = Cpachecker.Run.newBuilder(run).setDetails(resultMapper.mapDetails(state)).build();
      builder.setRun(run);
    }
    return builder.build();
  }
}

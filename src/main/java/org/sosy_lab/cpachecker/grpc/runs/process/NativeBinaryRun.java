// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.process;

import com.google.common.collect.ImmutableList;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import org.jboss.logging.Logger;
import org.sosy_lab.cpachecker.grpc.CpacheckerService;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.runs.RunConfig;
import org.sosy_lab.cpachecker.grpc.runs.RunConfigMapper;
import org.sosy_lab.cpachecker.grpc.runs.RunException;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerProcessBuilder;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

/**
 * Native run of CPAchecker. This run executes CPAchecker that was previously compiled to an
 * executable binary. This run should have a significantly lower start-up time than JVM-based
 * runners, but it probably uses a worse garbage collector and does not perform Just-in-time
 * compilation and the corresponding optimizations, so it <i>may</i> perform worse on complicated
 * analysis tasks.
 */
class NativeBinaryRun extends AbstractProcessRun {

  private static final Logger LOG = Logger.getLogger(CpacheckerService.class);
  private static final String ERROR_MESSAGE_INTERNAL =
      "Internal server error while executing CPAchecker";

  private final ProcessOptions processOptions;
  private final RunConfigMapper configMapper;
  private final CpacheckerSupport cpacheckerSupport;
  private final Path binary;

  /**
   * Sets up a new CPAchecker run. The run is not started yet.
   *
   * @see NativeBinaryRunFactory
   */
  NativeBinaryRun(
      String name,
      ProcessOptions processOptions,
      RunConfigMapper configMapper,
      CpacheckerSupport cpacheckerSupport,
      DirectoryManager directoryManager,
      CpacheckerProcessBuilder processBuilder,
      Path cpacheckerNativeBinary) {
    super(name, cpacheckerSupport, directoryManager, processBuilder);
    this.processOptions = processOptions;
    this.configMapper = configMapper;
    this.cpacheckerSupport = cpacheckerSupport;
    this.binary = cpacheckerNativeBinary;
  }

  @Override
  ImmutableList<String> createCommand(StartRun request, DirectoryManager.RunDirectory runDirectory)
      throws RunException {
    RunConfig config = createRunConfig(request, runDirectory);
    Path outputDir = runDirectory.getCurrentOutputDirectory();
    ImmutableList.Builder<String> command = ImmutableList.builder();
    command.addAll(getBaseCommand());
    command.addAll(cpacheckerSupport.createCommandLineOptions(config, outputDir));
    return command.build();
  }

  private RunConfig createRunConfig(StartRun request, DirectoryManager.RunDirectory runDirectory)
      throws RunException {
    try {
      return configMapper.createRunConfig(request, runDirectory);
    } catch (IOException e) {
      LOG.warn("IOException when creating RunConfig", e);
      throw new RunException(ERROR_MESSAGE_INTERNAL, e);
    }
  }

  private List<String> getBaseCommand() {
    // we use -noout because java.awt is not supported by our
    // cpachecker binary so far, but it is used by its SVG output writer.
    // Instead of trying to turn off all outputs that make the binary fail,
    // turn off everything and only enable on-demand
    return List.of(
        binary.toString(),
        "-noout",
        "-timelimit",
        String.valueOf(processOptions.getTimelimitInSeconds()),
        "-Djava.library.path=" + getNativeLibraryPaths(processOptions.getDistributionDirectory()));
  }

  private String getNativeLibraryPaths(Path distributionDirectory) {
    String distribution = distributionDirectory.toAbsolutePath().toString();
    return String.join(
        ":", distribution + "/lib/native/x86_64-linux", distribution + "/lib/java/runtime");
  }
}

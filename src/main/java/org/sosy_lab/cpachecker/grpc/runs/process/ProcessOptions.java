// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.process;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import java.nio.file.Path;
import org.eclipse.microprofile.config.inject.ConfigProperty;

/** Options for processes that run CPAchecker. */
@Singleton
public class ProcessOptions {
  private final Path distributionDirectory;
  private final int timelimitInSeconds;

  @Inject
  ProcessOptions(
      @ConfigProperty(name = "cpachecker.grpc.run.distribution") Path distributionDirectory,
      @ConfigProperty(name = "cpachecker.grpc.run.timelimitInS") int timelimitInSeconds) {
    this.distributionDirectory = distributionDirectory;
    this.timelimitInSeconds = timelimitInSeconds;
  }

  public Path getDistributionDirectory() {
    return distributionDirectory;
  }

  public int getTimelimitInSeconds() {
    return timelimitInSeconds;
  }
}

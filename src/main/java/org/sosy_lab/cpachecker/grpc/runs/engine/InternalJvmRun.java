// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.engine;

import static com.google.common.base.Preconditions.checkState;

import com.google.common.base.Preconditions;
import io.quarkus.runtime.util.ExceptionUtil;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Stream;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.jboss.logging.Logger;
import org.sosy_lab.common.ShutdownManager;
import org.sosy_lab.common.configuration.InvalidConfigurationException;
import org.sosy_lab.common.time.Timer;
import org.sosy_lab.cpachecker.grpc.CpacheckerService;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.runs.Run;
import org.sosy_lab.cpachecker.grpc.runs.RunConfig;
import org.sosy_lab.cpachecker.grpc.runs.RunConfigMapper;
import org.sosy_lab.cpachecker.grpc.runs.RunException;
import org.sosy_lab.cpachecker.grpc.runs.RunResult;
import org.sosy_lab.cpachecker.grpc.runs.RunState;
import org.sosy_lab.cpachecker.grpc.util.Async;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

/** A {@link Run} that executes CPAchecker in the same JVM as the current process. */
public class InternalJvmRun implements Run {
  private static final String ERROR_MESSAGE_INTERNAL =
      "Internal server error while executing CPAchecker";
  private static final Logger LOG = Logger.getLogger(CpacheckerService.class);
  private final RunConfigMapper configMapper;
  private final CpacheckerSupport cpacheckerSupport;
  private final DirectoryManager directoryManager;
  private final CpacheckerRunnerFactory runnerFactory;
  private final Async async;
  private final String name;
  private final ShutdownManager shutdownManager;
  private final long creationTime;
  private final ByteArrayOutputStream stdout = new ByteArrayOutputStream();
  @Nullable private Path logFile;
  private CompletableFuture<RunResult> execution;

  /** Run directory of the current execution. Nullable. */
  private DirectoryManager.RunDirectory runDirectory;

  /**
   * Sets up a new CPAchecker run. The run is not started yet.
   *
   * @see InternalJvmRunFactory
   */
  InternalJvmRun(
      String name,
      RunConfigMapper configMapper,
      CpacheckerSupport cpacheckerSupport,
      DirectoryManager directoryManager,
      CpacheckerRunnerFactory runnerFactory,
      Async async) {
    creationTime = System.currentTimeMillis();
    this.name = name;
    this.configMapper = configMapper;
    this.cpacheckerSupport = cpacheckerSupport;
    this.directoryManager = directoryManager;
    this.runnerFactory = runnerFactory;
    this.async = async;
    this.shutdownManager = ShutdownManager.create();
  }

  @Override
  public void start(StartRun runRequest) throws RunException {
    LOG.debugf("Starting '%s'", name);
    LOG.debugf("Activeness: %s", getActiveness());

    if (getActiveness() != RunState.Activeness.PENDING) {
      throw new IllegalStateException("Trying to run a run that is not pending. Name: " + name);
    }

    try {
      // do not use try-with-resources on the run directory
      // so that the directory is not deleted after this non-blocking call finishes.
      runDirectory = directoryManager.createNewRunDirectory();
      LOG.infof("Run-related files will be stored in %s", runDirectory);
      logFile = runDirectory.getNewLoggingFile();
    } catch (IOException e) {
      LOG.warn("IOException handling temporary directory", e);
      throw new RunException(ERROR_MESSAGE_INTERNAL);
    }

    Timer timer = new Timer();
    try {
      timer.start();
      runDirectory.makeCpacheckerConfigsAvailable();
    } catch (IOException e) {
      LOG.warn("IOException handling temporary directory", e);
      throw new RunException(ERROR_MESSAGE_INTERNAL);
    } catch (IllegalStateException e) {
      LOG.warn("IllegalStateException handling temporary directory", e);
    } catch (Exception e) {
      LOG.errorf(e, "Exception handling temporary directory");
    } finally {
      timer.stop();
      LOG.debugf("Copying CPAchecker configs took %s", timer);
    }

    assert runDirectory != null;
    assert logFile != null;

    RunConfig config = createRunConfig(runRequest, runDirectory);

    LOG.debugf("Creating a %s...", CpacheckerRunner.class.getSimpleName());
    try {
      CpacheckerRunner runner =
          runnerFactory.create(
              config, runDirectory.getCurrentOutputDirectory(), logFile, stdout, shutdownManager);
      execution = async.startWorker(runner);
    } catch (InvalidConfigurationException e) {
      LOG.warn("Invalid Configuration", e);
      execution = async.failure(e);
    } catch (Exception e) {
      LOG.errorf(e, "Unexpected exception while starting run %s", name);
      execution = async.failure(e);
    }
    LOG.debugf("Submitted execution for '%s'.", name);
  }

  @Override
  public synchronized void stop() {
    LOG.debugf("stop() called on '%s'", name);
    if (getActiveness() == RunState.Activeness.FINISHED) {
      LOG.infof("Run '%s' is already finished, no need to stop it.", name);
      return;
    }
    if (getActiveness() == RunState.Activeness.PENDING) {
      throw new IllegalStateException("Trying to stop a run that is still pending. Name: " + name);
    }

    checkState(execution != null, "Assertion Failed: execution is null but should not be.");
    LOG.debugf("Cancelling execution of run '%s'", name);
    shutdownManager.requestShutdown("Run " + name + " was cancelled.");

    // it could be useful to cancel the future here.
    // If we don't do it, the activeness to the run will accurately reflect the activeness
    // of the CPAchecker callable in the background. Once the termination request has been processed
    // by CPAchecker, the execution will be finished.
    // execution.cancel(true);
  }

  /** Returns the output files generated by this run. */
  private List<String> getOutputFiles() {
    Preconditions.checkNotNull(runDirectory);
    Path outputDirectory = runDirectory.getCurrentOutputDirectory();
    try (Stream<Path> files = Files.walk(outputDirectory)) {
      return files
          // ignore the output directory itself.
          .filter(p -> !p.equals(outputDirectory))
          // only keep paths that are actually files
          .filter(Files::isRegularFile)
          // relativize each path to only expose the relevant parts of the output file to the user,
          // and not expose our internal directory structure.
          // We want to talk about output files as 'names', irrelevant of their concrete location.
          .map(p -> outputDirectory.relativize(p).toString())
          .toList();
    } catch (IOException | SecurityException e) {
      LOG.warn("Exception handling output directory", e);
      return List.of();
    }
  }

  @Override
  public RunState waitUntilFinished(long timeout) throws InterruptedException {

    if (getActiveness() == RunState.Activeness.PENDING) {
      throw new IllegalStateException(
          "Trying to wait for a run that is not running. Name: " + name);
    }

    LOG.debugf("Waiting on run '%s' with timeout %dms", name, timeout);

    RunState.RunStateBuilder runStateBuilder =
        RunState.builder().name(name).timestamp(creationTime);

    try {
      RunResult result = execution.get(timeout, TimeUnit.MILLISECONDS);
      LOG.debugf("Got result for '%s' after waiting: %s", name, result);
      return runStateBuilder
          .stderr(readLog())
          .stdout(readStdout())
          .result(result)
          .activeness(getActiveness())
          .outputFiles(getOutputFiles())
          .build();

    } catch (TimeoutException e) {
      throw new InterruptedException(e.getMessage());
    } catch (CancellationException e) {
      LOG.debugf("Execution of run '%s' was cancelled", name);
      return runStateBuilder
          .stderr(readLog())
          .stdout(readStdout())
          .result(new RunResult(RunResult.Verdict.UNKNOWN))
          .activeness(getActiveness())
          .outputFiles(getOutputFiles())
          .build();
    } catch (ExecutionException e) {

      String errorMsg = e.getMessage() + "\n" + ExceptionUtil.generateStackTrace(e);

      LOG.errorf("An Exception occurred during the execution of '%s':\n%s", name, errorMsg);

      return runStateBuilder
          .stderr(readLog() + "\n" + errorMsg)
          .stdout(readStdout())
          .result(new RunResult(RunResult.Verdict.ERROR))
          .activeness(getActiveness())
          .outputFiles(getOutputFiles())
          .build();
    }
  }

  private String readStdout() {
    return stdout.toString(Charset.defaultCharset());
  }

  private String readLog() {
    if (logFile == null) {
      return "";
    }
    try {
      return Files.readString(logFile);
    } catch (IOException e) {
      LOG.warnf(e, "IOException when reading log file '%s'", logFile);
      return "Error reading log file";
    }
  }

  @Override
  public RunState getState() {
    RunState.RunStateBuilder runStateBuilder =
        RunState.builder().name(name).timestamp(creationTime).activeness(getActiveness());

    StringBuilder stderr = new StringBuilder(readLog());

    if (getActiveness() == RunState.Activeness.FINISHED) {
      try {
        return waitUntilFinished(100);
      } catch (InterruptedException e) {
        LOG.warn("Interrupted while receiving finished execution", e);
        stderr.append("\nInterrupted while receiving finished execution");

        return runStateBuilder
            .stderr(stderr.toString())
            .stdout(readStdout())
            .result(new RunResult(RunResult.Verdict.ERROR))
            .build();
      }
    }
    return runStateBuilder.stderr(stderr.toString()).stdout(readStdout()).build();
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public ByteBuffer getOutputFile(String fileName) {
    Path outputDirectory = runDirectory.getCurrentOutputDirectory();
    Path outputFile = outputDirectory.resolve(fileName);
    if (!Files.isRegularFile(outputFile)) {
      LOG.warnf("File '%s' is no regular file", outputFile);
      return null;
    }
    try {
      return ByteBuffer.wrap(Files.readAllBytes(outputFile));
    } catch (IOException | SecurityException e) {
      LOG.warn("Exception handling output file", e);
      return null;
    }
  }

  private RunConfig createRunConfig(StartRun request, DirectoryManager.RunDirectory runDirectory)
      throws RunException {
    try {
      return configMapper.createRunConfig(request, runDirectory);
    } catch (IOException e) {
      LOG.warn("IOException when creating RunConfig", e);
      throw new RunException(ERROR_MESSAGE_INTERNAL, e);
    }
  }

  private RunState.Activeness getActiveness() {
    if (execution == null) {
      return RunState.Activeness.PENDING;
    }
    if (execution.isDone() || execution.isCancelled()) {
      return RunState.Activeness.FINISHED;
    }
    // future exist but is not done
    return RunState.Activeness.RUNNING;
  }

  @Override
  public void close() throws Exception {
    if (runDirectory != null) {
      runDirectory.close();
    }
    if (getActiveness() == RunState.Activeness.RUNNING) {
      stop();
    }
  }
}

// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.process;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;
import org.jboss.logging.Logger;
import org.sosy_lab.cpachecker.grpc.CpacheckerService;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.runs.Run;
import org.sosy_lab.cpachecker.grpc.runs.RunException;
import org.sosy_lab.cpachecker.grpc.runs.RunResult;
import org.sosy_lab.cpachecker.grpc.runs.RunState;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerProcess;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerProcessBuilder;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

/**
 * A CPAchecker run in a separate process. This abstract class handles creation and all
 * communication with the CPAchecker process. Extending classes only have to provide the CPAchecker
 * command to run through {@link #createCommand(StartRun, DirectoryManager.RunDirectory)}.
 */
abstract class AbstractProcessRun implements Run {

  // do not make public, but keep error messages local
  // and easy to modify without influencing other classes
  private static final String ERROR_MESSAGE_INTERNAL =
      "Internal server error while executing CPAchecker";
  private static final Logger LOG = Logger.getLogger(CpacheckerService.class);

  private final CpacheckerSupport cpacheckerSupport;
  private final DirectoryManager directoryManager;
  private final CpacheckerProcessBuilder processBuilder;

  private final String name;
  private final long creationTime;

  private CpacheckerProcess process;
  private DirectoryManager.RunDirectory runDirectory;

  AbstractProcessRun(
      String name,
      CpacheckerSupport cpacheckerSupport,
      DirectoryManager directoryManager,
      CpacheckerProcessBuilder processBuilder) {
    creationTime = System.currentTimeMillis();
    this.name = name;
    this.cpacheckerSupport = cpacheckerSupport;
    this.directoryManager = directoryManager;
    this.processBuilder = processBuilder;
  }

  /**
   * Returns the command that must be run to start a new CPAchecker process. The returned list of
   * Strings has as first element the CPAchecker executable to run. The following parameters are the
   * individual parameters to that executable.
   *
   * @param request start-run request
   * @param runDirectory directory that was created for the run. The run will not necessarily use
   *     this directory as working directory, but it is available for the run to store and read
   *     data. This directory should not be deleted by this method, but it may be deleted sometime
   *     after the run finishes.
   * @return the command to run CPAchecker
   * @throws RunException if creation of the command fails
   */
  abstract ImmutableList<String> createCommand(
      StartRun request, DirectoryManager.RunDirectory runDirectory) throws RunException;

  @Override
  public final void start(StartRun runRequest) throws RunException {
    if (getActiveness() != RunState.Activeness.PENDING) {
      throw new IllegalStateException("Trying to run a run that is not pending. Name: " + name);
    }
    try {
      // do not use try-with-resources on the run directory
      // so that the directory is not deleted after this non-blocking call finishes.
      runDirectory = directoryManager.createNewRunDirectory();
      runDirectory.makeCpacheckerConfigsAvailable();
      LOG.infof("Run-related files will be stored in %s", runDirectory);
      process = processBuilder.create(createCommand(runRequest, runDirectory));
    } catch (IOException e) {
      LOG.warn("IOException handling temporary directory", e);
      throw new RunException(ERROR_MESSAGE_INTERNAL);
    }
  }

  /**
   * Stops the process cleanly. If no clean stop is supported by the implementation, the process is
   * stopped forcefully. This method is non-blocking: it does not wait on the process to terminate
   * but returns immediately.
   *
   * @throws IllegalStateException if the process is not running
   */
  // synchronized so that we never try to destroy the same process twice
  @Override
  public final synchronized void stop() {
    RunState.Activeness activeness = getActiveness();
    if (activeness == RunState.Activeness.PENDING) {
      throw new IllegalStateException("Trying to stop a run that is still pending. Name: " + name);
    }
    if (activeness == RunState.Activeness.FINISHED) {
      // We swallow a stop on a finished process to avoid race conditions
      LOG.debugf("Silently ignoring stop-request on done process");
      return;
    }
    LOG.debugf("Destroying process: %s", process.getId());
    process.destroy();
  }

  @Override
  public final RunState waitUntilFinished(long timeoutInMillis) throws InterruptedException {
    RunState.Activeness activeness = getActiveness();
    if (activeness == RunState.Activeness.PENDING) {
      throw new IllegalStateException(
          "Trying to wait on a run that is not started yet. Name: " + name);
    } else if (activeness == RunState.Activeness.RUNNING) {
      process.waitFor(timeoutInMillis);
    }

    return getState();
  }

  private RunState.Activeness getActiveness() {
    if (process == null) {
      return RunState.Activeness.PENDING;
    } else if (!process.isDone()) {
      return RunState.Activeness.RUNNING;
    } else {
      return RunState.Activeness.FINISHED;
    }
  }

  @Override
  public final RunState getState() {
    RunState.Activeness activeness = getActiveness();
    RunState.RunStateBuilder builder =
        RunState.builder().name(name).timestamp(creationTime).activeness(activeness);
    switch (activeness) {
      case RUNNING -> {
        String stdout = process.getCurrentStdout();
        String stderr = process.getCurrentStderr();
        builder.stdout(stdout).stderr(stderr);
      }
      case FINISHED -> {
        String stdout = process.getCurrentStdout();
        String stderr = process.getCurrentStderr();
        builder.stdout(stdout).stderr(stderr);
        RunResult result = cpacheckerSupport.parseExecutionResult(stdout, stderr);
        builder.result(result);
        builder.outputFiles(getOutputFiles());
      }
      case PENDING -> doNothing();
      default -> throw new AssertionError("Unhandled activeness: " + activeness);
    }
    return builder.build();
  }

  /** Returns the output files generated by this run. */
  private List<String> getOutputFiles() {
    Preconditions.checkNotNull(runDirectory);
    Path outputDirectory = runDirectory.getCurrentOutputDirectory();
    try (Stream<Path> files = Files.walk(outputDirectory)) {
      return files
          // ignore the output directory itself.
          .filter(p -> !p.equals(outputDirectory))
          // only keep paths that are actually files
          .filter(Files::isRegularFile)
          // relativize each path to only expose the relevant parts of the output file to the user,
          // and not expose our internal directory structure.
          // We want to talk about output files as 'names', irrelevant of their concrete location.
          .map(p -> outputDirectory.relativize(p).toString())
          .toList();
    } catch (IOException | SecurityException e) {
      LOG.warn("Exception handling output directory", e);
      return List.of();
    }
  }

  private void doNothing() {}

  @Override
  public ByteBuffer getOutputFile(String fileName) {
    Path outputDirectory = runDirectory.getCurrentOutputDirectory();
    Path outputFile = outputDirectory.resolve(fileName);
    if (!Files.isRegularFile(outputFile)) {
      LOG.warnf("File %s is no regular file", outputFile);
      return null;
    }
    try {
      return ByteBuffer.wrap(Files.readAllBytes(outputFile));
    } catch (IOException | SecurityException e) {
      LOG.warn("Exception handling output file", e);
      return null;
    }
  }

  @Override
  public final String getName() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AbstractProcessRun that = (AbstractProcessRun) o;
    return creationTime == that.creationTime
        && Objects.equals(name, that.name)
        && Objects.equals(process, that.process);
  }

  @Override
  public void close() throws IOException {
    if (runDirectory != null) {
      runDirectory.close();
    }
    if (getActiveness() == RunState.Activeness.RUNNING) {
      stop();
    }
  }

  @Override
  public int hashCode() {
    return Objects.hash(name, creationTime, process);
  }
}

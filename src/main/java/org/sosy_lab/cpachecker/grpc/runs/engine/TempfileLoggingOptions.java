// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.engine;

import io.smallrye.common.constraint.NotNull;
import java.nio.file.Path;
import java.util.List;
import java.util.logging.Level;
import org.sosy_lab.common.annotations.ReturnValuesAreNonnullByDefault;
import org.sosy_lab.common.configuration.Configuration;
import org.sosy_lab.common.configuration.InvalidConfigurationException;
import org.sosy_lab.common.log.LoggingOptions;

/**
 * A wrapper around {@link LoggingOptions} that allows to set the output file directly. This allows
 * for recording the output of CPAchecker in a temporary file.
 */
@ReturnValuesAreNonnullByDefault
class TempfileLoggingOptions extends LoggingOptions {
  private Path outputFile;
  private final LoggingOptions loggingOptions;

  public TempfileLoggingOptions(Configuration configuration) throws InvalidConfigurationException {
    loggingOptions = new LoggingOptions(configuration);
    outputFile = loggingOptions.getOutputFile();
  }

  @Override
  public Level getFileLevel() {
    return loggingOptions.getConsoleLevel();
  }

  @Override
  public Level getConsoleLevel() {
    return loggingOptions.getConsoleLevel();
  }

  @Override
  public List<Level> getFileExclude() {
    return loggingOptions.getConsoleExclude();
  }

  @Override
  public List<Level> getConsoleExclude() {
    return loggingOptions.getConsoleExclude();
  }

  public void setOutputFile(@NotNull Path file) {
    this.outputFile = file;
  }

  @Override
  public Path getOutputFile() {
    return outputFile;
  }

  @Override
  public int getTruncateSize() {
    return loggingOptions.getTruncateSize();
  }

  @Override
  public boolean useColors() {
    return loggingOptions.useColors();
  }
}

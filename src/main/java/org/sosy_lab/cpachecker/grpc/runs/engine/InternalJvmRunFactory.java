// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.engine;

import io.quarkus.arc.properties.IfBuildProperty;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import org.sosy_lab.cpachecker.grpc.runs.Run;
import org.sosy_lab.cpachecker.grpc.runs.RunConfigMapper;
import org.sosy_lab.cpachecker.grpc.runs.RunFactory;
import org.sosy_lab.cpachecker.grpc.util.Async;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

/**
 * A factory for {@link InternalJvmRun}s. Creates CPAchecker runs that run in the same JVM as the
 * current process.
 */
@Singleton
@IfBuildProperty(name = "cpachecker.grpc.run.engine.enabled", stringValue = "true")
public class InternalJvmRunFactory implements RunFactory {

  private final CpacheckerSupport cpacheckerSupport;
  private final RunConfigMapper configMapper;
  private final DirectoryManager directoryManager;
  private final CpacheckerRunnerFactory runnerFactory;
  private final Async async;

  /**
   * Creates a new instance of this class.
   *
   * @param cpacheckerSupport utility class for handling CPAchecker
   * @param configMapper config mapper
   * @param directoryManager manager for working directories of CPAchecker runs
   * @param async async utility
   */
  @Inject
  InternalJvmRunFactory(
      CpacheckerSupport cpacheckerSupport,
      RunConfigMapper configMapper,
      DirectoryManager directoryManager,
      CpacheckerRunnerFactory runnerFactory,
      Async async) {
    this.cpacheckerSupport = cpacheckerSupport;
    this.configMapper = configMapper;
    this.directoryManager = directoryManager;
    this.runnerFactory = runnerFactory;
    this.async = async;
  }

  @Override
  public Run create(String name) {
    return new InternalJvmRun(
        name, configMapper, cpacheckerSupport, directoryManager, runnerFactory, async);
  }
}

// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.process;

import com.google.common.collect.ImmutableList;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import org.jboss.logging.Logger;
import org.sosy_lab.cpachecker.grpc.CpacheckerService;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.runs.RunConfig;
import org.sosy_lab.cpachecker.grpc.runs.RunConfigMapper;
import org.sosy_lab.cpachecker.grpc.runs.RunException;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerProcessBuilder;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

/** A CPAchecker run in a separate process, with JVM. */
class ExternalJvmRun extends AbstractProcessRun {

  private static final Logger LOG = Logger.getLogger(CpacheckerService.class);
  private static final String ERROR_MESSAGE_INTERNAL =
      "Internal server error while executing CPAchecker";

  private static final String JVM_PROPERTY_FILE_ENCODING = "file.encoding";
  private static final String CPACHECKER_JAR = "cpachecker.jar";
  private static final String CPACHECKER_MAIN_CLASS = "org.sosy_lab.cpachecker.cmdline.CPAMain";

  private final RunConfigMapper configMapper;
  private final CpacheckerSupport cpacheckerSupport;
  private final JvmOptions jvmOptions;
  private final ProcessOptions processOptions;

  /**
   * Sets up a new CPAchecker run. The run is not started yet.
   *
   * @see ExternalJvmRunFactory
   */
  ExternalJvmRun(
      String name,
      JvmOptions jvmOptions,
      ProcessOptions processOptions,
      RunConfigMapper configMapper,
      CpacheckerSupport cpacheckerSupport,
      DirectoryManager directoryManager,
      CpacheckerProcessBuilder processBuilder) {
    super(name, cpacheckerSupport, directoryManager, processBuilder);
    this.jvmOptions = jvmOptions;
    this.processOptions = processOptions;
    this.configMapper = configMapper;
    this.cpacheckerSupport = cpacheckerSupport;
  }

  @Override
  ImmutableList<String> createCommand(StartRun request, DirectoryManager.RunDirectory runDirectory)
      throws RunException {
    RunConfig config = createRunConfig(request, runDirectory);
    Path outputDir = runDirectory.getCurrentOutputDirectory();
    ImmutableList.Builder<String> command = ImmutableList.builder();
    command.addAll(getBaseCommand());
    command.addAll(cpacheckerSupport.createCommandLineOptions(config, outputDir));
    return command.build();
  }

  private RunConfig createRunConfig(StartRun request, DirectoryManager.RunDirectory runDirectory)
      throws RunException {
    try {
      return configMapper.createRunConfig(request, runDirectory);
    } catch (IOException e) {
      LOG.warn("IOException when creating RunConfig", e);
      throw new RunException(ERROR_MESSAGE_INTERNAL, e);
    }
  }

  private List<String> getBaseCommand() {
    return List.of(
        getJavaBinary(),
        "-XX:+PerfDisableSharedMem",
        "-Djava.awt.headless=true",
        "-D" + JVM_PROPERTY_FILE_ENCODING + "=" + StandardCharsets.UTF_8,
        "-Xss" + jvmOptions.getStackSizeInKb() + "k",
        "-Xmx" + jvmOptions.getHeapSizeInMb() + "M",
        "-ea", // enable assertions
        "-cp",
        getCpacheckerClasspath(processOptions.getDistributionDirectory()),
        CPACHECKER_MAIN_CLASS,
        "-timelimit",
        String.valueOf(processOptions.getTimelimitInSeconds()));
  }

  private String getJavaBinary() {
    // If this runner is executed not natively but in a JVM,
    // do not rely on PATH to get the correct Java version,
    // but use java.home so that CPAchecker is executed with the same Java version
    // that this runner runs in
    String javaHome = System.getProperty("java.home");
    if (javaHome != null) {
      return javaHome + "/bin/java";
    } else {
      return "java";
    }
  }

  private String getCpacheckerClasspath(Path cpacheckerDistributionDirectory) {
    String distribution = toString(cpacheckerDistributionDirectory);
    return String.join(
        ":",
        distribution + "/bin",
        distribution + "/" + CPACHECKER_JAR,
        distribution + "/lib/*",
        distribution + "/lib/java/runtime/*");
  }

  private String toString(Path path) {
    return path.toAbsolutePath().toString();
  }
}

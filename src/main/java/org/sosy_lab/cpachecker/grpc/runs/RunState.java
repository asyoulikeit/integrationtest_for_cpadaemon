// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

import java.util.List;
import lombok.Builder;
import lombok.Value;

/** State of a {@link Run}. */
@Builder
@Value
public class RunState {

  /** Current activeness of a run. */
  public enum Activeness {
    /** The run was created, but not started yet. */
    PENDING,
    /** The run is currently running. */
    RUNNING,
    /** The run is finished. */
    FINISHED,
  }

  /** Name of this {@link Run}. */
  String name;

  /** The current activeness of this run. */
  Activeness activeness;

  /** The creation time of this {@link Run}. */
  long timestamp;

  /** The current output on standard output. */
  String stdout;

  /** The current output on error output. */
  String stderr;

  /**
   * The current output files of this run. Files included here are guaranteed to exist, but they may
   * still be written to.
   */
  List<String> outputFiles;

  /**
   * The result of this {@link Run}, if the run finished successfully. <code>null</code> otherwise.
   */
  RunResult result;
}

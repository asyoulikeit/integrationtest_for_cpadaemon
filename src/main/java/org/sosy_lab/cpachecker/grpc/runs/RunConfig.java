// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

import java.nio.file.Path;
import java.util.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Singular;
import lombok.Value;

/**
 * Run config for CPAchecker. This config contains input files and config parameters that match the
 * parameters expected by CPAchecker.
 */
@Value
@Builder
public class RunConfig {
  /** The program to run CPAchecker against. Must be set. */
  @NonNull Path programFile;

  /** The CPAchecker properties configuration file to start CPAchecker with. Must be set. */
  @NonNull Path cpacheckerConfig;

  /**
   * The specification to provide to CPAchecker. If not set, no specification is provided to
   * CPAchecker.
   */
  Path specification;

  /**
   * Additional configuration options to provide to CPAchecker. The command-line equivalent is
   * '-setprop'.
   */
  @Singular List<Option> additionalOptions;

  /** The machine model to assume during analysis. */
  @NonNull MachineModel machineModel;

  /** The entry function to use during analysis. */
  @NonNull String entryFunction;

  /** The witness to validate during analysis. If not set, no witness is given to CPAchecker. */
  Path witness;

  /** Supported machine models. */
  public enum MachineModel {
    LINUX32,
    LINUX64
  }

  /**
   * A single configuration option for CPAchecker. On the command-line interface of CPAchecker,
   * these options can be provided through '-setprop key=value'.
   *
   * @param key the option name
   * @param value the value to set the option to
   */
  public record Option(@NonNull String key, @NonNull String value) {}
}

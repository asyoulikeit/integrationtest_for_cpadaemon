// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

import jakarta.annotation.Nullable;
import java.util.Collection;
import java.util.Optional;

/** Store for {@link Run Runs}. It allows to store runs and retrieve them. */
public interface RunStore {

  /**
   * Stores a new run.
   *
   * @param run the run to store.
   */
  void put(Run run);

  /**
   * Retrieves a run by its name.
   *
   * @param name name of the run to retrieve
   * @return the corresponding run, if any matches the given name. Returns null otherwise.
   */
  @Nullable
  Run get(String name);

  /**
   * Retrieves all currently stored runs.
   *
   * @return all stored runs
   */
  Collection<Run> getAll();

  /** Clears the store. */
  void clear();

  /** Removes a run from the store. */
  Optional<Run> remove(String name);
}

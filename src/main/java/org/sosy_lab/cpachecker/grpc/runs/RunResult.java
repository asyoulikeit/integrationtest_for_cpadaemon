// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

/** The result of a CPAchecker run. */
public record RunResult(Verdict verdict) {

  /**
   * The verdict of a CPAchecker run.
   *
   * <p>If CPAchecker ran a verification, {@link Verdict#TRUE} means that the verified property
   * holds in the program under verification, and {@link Verdict#FALSE} means that the verified
   * property does not hold in the program under verification.
   *
   * <p>If CPAchecker ran a violation-witness validation, {@link Verdict#TRUE} means that the
   * witness is rejected, and {@link Verdict#FALSE} means that the witness is confirmed.
   *
   * <p>If CPAchecker ran a correctnes-witness validation, {@link Verdict#TRUE} means that the
   * witness is confirmed, and {@link Verdict#FALSE} means that the witness is rejected.
   *
   * <p>If CPAchecker did not run a verification and did not run a validation, the verdict is never
   * {@link Verdict#TRUE} and never {@link Verdict#FALSE}.
   *
   * <p>Verdict {@link Verdict#DONE} means that CPAchecker finished a non-verification and
   * non-validation run successfully, and {@link Verdict#UNKNOWN} is not expressive.
   */
  public enum Verdict {
    ERROR,
    UNKNOWN,
    TRUE,
    DONE,
    FALSE
  }
}

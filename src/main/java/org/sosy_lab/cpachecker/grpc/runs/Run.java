// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

import java.nio.ByteBuffer;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;

/** A single CPAchecker run. */
public interface Run extends AutoCloseable {

  /**
   * Starts this CPAchecker run. This call is non-blocking.
   *
   * @param runRequest the requested run parameters
   * @throws IllegalStateException if run was already started.
   */
  void start(StartRun runRequest) throws RunException;

  /**
   * Stops this CPAchecker run. This call is non-blocking.
   *
   * @throws IllegalStateException if run was not started yet
   */
  void stop();

  /**
   * Waits until this run is finished. This call is blocking if the run is not finished yet. If the
   * run is already finished, the method returns immediately. If the state does not finish within
   * the time limit, the last polled run state is returned after the time limit passed.
   *
   * @return the run state after the run is finished or the last polled run state if the timeout
   *     passed and the run was not finished.
   * @throws InterruptedException if the thread is interrupted while waiting
   * @throws IllegalStateException if run was not started yet
   */
  RunState waitUntilFinished(long timeout) throws InterruptedException;

  /** Returns the last polled run state. */
  RunState getState();

  /** Returns this runs unique name. */
  String getName();

  /** Returns the content of the output file with the given name. */
  ByteBuffer getOutputFile(String fileName);
}

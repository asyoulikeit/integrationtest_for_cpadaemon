// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.engine;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;
import java.util.stream.Collectors;
import lombok.NonNull;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.logging.Logger;
import org.sosy_lab.common.ShutdownManager;
import org.sosy_lab.common.configuration.Configuration;
import org.sosy_lab.common.configuration.ConfigurationBuilder;
import org.sosy_lab.common.configuration.FileOption;
import org.sosy_lab.common.configuration.InvalidConfigurationException;
import org.sosy_lab.common.configuration.converters.FileTypeConverter;
import org.sosy_lab.common.log.BasicLogManager;
import org.sosy_lab.common.log.LogManager;
import org.sosy_lab.cpachecker.cmdline.CPAMain;
import org.sosy_lab.cpachecker.grpc.CpacheckerService;
import org.sosy_lab.cpachecker.grpc.runs.RunConfig;
import org.sosy_lab.cpachecker.grpc.runs.RunException;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;

/**
 * * A factory creating a {@link CpacheckerRunner}. The factory sets up the configuration file and
 * creates the external objects that are injected into the main run method.
 */
@Singleton
public class CpacheckerRunnerFactory {

  private static final Logger LOG = Logger.getLogger(CpacheckerService.class);
  private static final String OUTPUT_PATH_OPTION = "output.path";
  private static final String SPEC_OPTION = "specification";
  private static final String WITNESS_FILE_OPTION = "witness.validation.file";
  private static final String MACHINE_MODEL_OPTION = "analysis.machineModel";
  private static final String ENTRY_FUNCTION_OPTION = "analysis.entryFunction";
  private static final String TIME_LIMIT_OPTION = "limits.time.cpu";

  private final int timelimitInSeconds;
  private final CpacheckerSupport cpacheckerSupport;

  @Inject
  CpacheckerRunnerFactory(
      @ConfigProperty(name = "cpachecker.grpc.run.timelimitInS") int timelimitInSeconds,
      CpacheckerSupport cpacheckerSupport) {
    this.timelimitInSeconds = timelimitInSeconds;
    this.cpacheckerSupport = cpacheckerSupport;
  }

  /**
   * Creates a new instance of a {@link CpacheckerRunner} from the given {@link RunConfig}.
   *
   * @param config the RunConfig used for this run
   * @param outputDirectory the directory CPAchecker should write its output files to
   * @param logFile the file to write the log to
   * @return a new instance of a {@link CpacheckerRunner}
   * @throws RunException if reading of the configuration file fails
   * @throws InvalidConfigurationException if the configuration is invalid
   */
  public CpacheckerRunner create(
      RunConfig config,
      Path outputDirectory,
      Path logFile,
      OutputStream stdout,
      ShutdownManager shutdownManager)
      throws RunException, InvalidConfigurationException {

    Configuration configuration;
    TempfileLoggingOptions logOptions;
    try {
      configuration = mapRunConfig(config, outputDirectory);
      logOptions = new TempfileLoggingOptions(configuration);
    } catch (IOException e) {
      LOG.error("IOException while mapping configuration", e);
      throw new RunException(e);
    } catch (InterruptedException e) {
      LOG.error("InterruptedException while mapping configuration", e);
      throw new RunException(e);
    }

    if (logFile != null) {
      logOptions.setOutputFile(logFile);
    }
    LOG.debugf("Writing log to file: %s", logOptions.getOutputFile());

    final LogManager logManager = BasicLogManager.create(logOptions);
    configuration.enableLogging(logManager);

    if (!System.getProperty("file.encoding", "UTF-8").equalsIgnoreCase("UTF-8")) {
      logManager.logf(
          Level.WARNING,
          "JVM property file.encoding is set to non-standard value '%s'. This is not recommended"
              + " and output files might be written in unexpected encodings.",
          System.getProperty("file.encoding"));
    }

    Path program = config.getProgramFile();

    return new CpacheckerRunner(
        configuration, stdout, logManager, shutdownManager, program, cpacheckerSupport);
  }

  /**
   * We do not allow multiple configs as of now; so we can skip the {@link
   * CPAMain#handlePropertyOptions}, thus also {@link CPAMain.BootstrapOptions} does not concern us.
   */
  private Configuration bootstrapConfiguration(Configuration config)
      throws InvalidConfigurationException {
    FileTypeConverter fileTypeConverter = FileTypeConverter.create(config);
    Configuration.getDefaultConverters().put(FileOption.class, fileTypeConverter);

    return Configuration.builder()
        .copyFrom(config)
        .addConverter(FileOption.class, fileTypeConverter)
        .build();
  }

  private void addIfNotNull(ConfigurationBuilder builder, String key, Object value) {
    if (value != null) {
      builder.setOption(key, value.toString());
    }
  }

  private void addIfNotNull(Map<String, String> map, String key, Object value) {
    if (value != null) {
      map.put(key, value.toString());
    }
  }

  /**
   * Maps a RunConfig to a CPAchecker Configuration. There are some caveats: When calling CPAchecker
   * through the external interfaces, the witness, timelimits, the entry function, and the machine
   * model are set via the commandline. When calling CPAchecker through the internal interfaces, we
   * can't just add these options to the configuration, as they would be lost in the case of witness
   * validation. We also consider the special options, e.g., `-spec` to have a higher priority than
   * the additional options, e.g., `-setprop specification=somefile.prp`.
   *
   * @param config the RunConfig to map
   * @param outputDirectory the directory CPAchecker should write its output files to
   * @return a CPAchecker Configuration
   * @throws IOException if an I/O error occurs during loading the configuration file
   * @throws InvalidConfigurationException if the configuration is invalid
   * @throws InterruptedException if an error occurs when handling witness options
   */
  private Configuration mapRunConfig(@NonNull RunConfig config, Path outputDirectory)
      throws IOException, InvalidConfigurationException, InterruptedException {

    ConfigurationBuilder configurationBuilder = Configuration.builder();
    LOG.debugf("Loading configuration from file %s", config.getCpacheckerConfig());
    configurationBuilder.loadFromFile(config.getCpacheckerConfig());

    // Handle potential witness options in RunConfig
    addIfNotNull(configurationBuilder, WITNESS_FILE_OPTION, config.getWitness());

    // handle entry function
    configurationBuilder.setOption(ENTRY_FUNCTION_OPTION, config.getEntryFunction());

    // Additional options are the ones set with -setprop key=value
    Map<String, String> optionMap =
        config.getAdditionalOptions().stream()
            .collect(Collectors.toMap(RunConfig.Option::key, RunConfig.Option::value, (a, b) -> b));

    // Only options from the optionMap survive a potential replacement of the original configuration
    // by a new witness configuration file.
    // We consider all options in the RunConfig to be passed as command line options to CPAchecker.
    // thus, these should override options regardless of the configuration file.
    optionMap.put(TIME_LIMIT_OPTION, String.valueOf(timelimitInSeconds));

    // Handle specification
    addIfNotNull(optionMap, SPEC_OPTION, config.getSpecification());

    // Handle machine model
    switch (config.getMachineModel()) {
      case LINUX32 -> optionMap.put(MACHINE_MODEL_OPTION, "Linux32");
      case LINUX64 -> optionMap.put(MACHINE_MODEL_OPTION, "Linux64");
      default -> throw new InvalidConfigurationException("Unsupported machine model");
    }

    configurationBuilder.setOptions(optionMap);

    // Users should not be able to set the output path, but we want to control this. So we set the
    // output path
    // to the expected output directory last to override any user-given values.
    configurationBuilder.setOption(OUTPUT_PATH_OPTION, outputDirectory.toString());

    Configuration configuration = configurationBuilder.build();
    configuration = bootstrapConfiguration(configuration);
    configuration = CPAMain.handleWitnessOptions(configuration, optionMap, Optional.empty());
    return configuration;
  }
}

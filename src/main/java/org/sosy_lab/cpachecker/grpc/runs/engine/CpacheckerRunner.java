// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.engine;

import static org.sosy_lab.common.io.DuplicateOutputStream.mergeStreams;

import com.google.common.io.MoreFiles;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.concurrent.Callable;
import org.jboss.logging.Logger;
import org.sosy_lab.common.ShutdownManager;
import org.sosy_lab.common.configuration.Configuration;
import org.sosy_lab.common.configuration.FileOption;
import org.sosy_lab.common.configuration.InvalidConfigurationException;
import org.sosy_lab.common.configuration.Option;
import org.sosy_lab.common.configuration.Options;
import org.sosy_lab.common.log.LogManager;
import org.sosy_lab.cpachecker.core.CPAchecker;
import org.sosy_lab.cpachecker.core.CPAcheckerResult;
import org.sosy_lab.cpachecker.grpc.CpacheckerService;
import org.sosy_lab.cpachecker.grpc.runs.RunResult;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;

/**
 * A {@link Callable} that runs CPAchecker on a single program. This class is used to run CPAchecker
 * in a new thread.
 */
public class CpacheckerRunner implements Callable<RunResult> {
  private static final Logger LOG = Logger.getLogger(CpacheckerService.class);
  private static final long WAIT_ON_CFA_EXPORT_IN_MS = 5000; // 5 seconds
  final Path programPath;
  final Configuration config;
  final LogManager logManager;
  final ShutdownManager shutdownManager;
  final CPAchecker cpachecker;

  private final PrintStream stdout;
  private final CpacheckerSupport cpacheckerSupport;

  /**
   * Creates a new instance of a {@link CpacheckerRunner} with the given parameters.
   *
   * @param config the configuration used for this run
   * @param logManager the log manager passed on to CPAchecker
   * @param shutdownManager the shutdown manager passed on to CPAchecker
   * @param programPath the path to the program that should be checked
   * @throws InvalidConfigurationException if the configuration is invalid
   */
  public CpacheckerRunner(
      Configuration config,
      OutputStream stdout,
      LogManager logManager,
      ShutdownManager shutdownManager,
      Path programPath,
      CpacheckerSupport cpacheckerSupport)
      throws InvalidConfigurationException {
    this.config = config;
    this.logManager = logManager;
    this.shutdownManager = shutdownManager;
    this.programPath = programPath;
    this.stdout = makePrintStream(stdout);
    this.cpacheckerSupport = cpacheckerSupport;
    cpachecker = new CPAchecker(config, logManager, shutdownManager);
  }

  @Override
  public RunResult call() {
    List<String> programNames = List.of(programPath.toString());
    LOG.debugf("Starting CPAchecker with program %s", programNames);
    CPAcheckerResult result;
    try {
      // Start capturing stdout
      System.setOut(new PrintStream(mergeStreams(stdout, System.out)));

      result = cpachecker.run(programNames);
    } finally {
      // Stop capturing
      System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out)));
    }
    LOG.debugf("CPAchecker finished");
    try {
      exportStatistics(result);
    } catch (IOException e) {
      LOG.error("Failed to export statistics", e);
    }
    logManager.flush();
    // We do not expose the CPAchecker-internal CPAcheckerResult because these objects can be
    // immense (hundreds of MB).
    // Instead, only expose that information that we use.
    return convertCpacheckerResult(result);
  }

  private void exportStatistics(CPAcheckerResult result) throws IOException {
    StatisticsOptions options = new StatisticsOptions();
    try {
      config.inject(options);
    } catch (InvalidConfigurationException e) {
      LOG.error("Failed to configure statistics export options", e);
      return;
    }
    if (options.exportStatistics && options.exportStatisticsFile != null) {
      LOG.infof("Exporting statistics to %s", options.exportStatisticsFile);
      MoreFiles.createParentDirectories(options.exportStatisticsFile);
      OutputStream console = options.printStatistics ? stdout : null;
      try (OutputStream file =
          mergeStreams(Files.newOutputStream(options.exportStatisticsFile), console)) {

        PrintStream stream = makePrintStream(file);
        result.printStatistics(stream);
        stream.println();
        if (!options.printStatistics) {
          stream = makePrintStream(mergeStreams(stdout, file));
        }
        result.printResult(stream);
      }
    } else {
      result.printStatistics(stdout);
      // Just print the result
      result.printResult(stdout);
    }
    result.writeOutputFiles();
    waitOnCfaExport();
  }

  private void waitOnCfaExport() {
    // find all threads that contain "export" in their names and wait until they are finished
    Thread.getAllStackTraces().keySet().stream()
        .filter(t -> t.getName().equals("CFA export thread"))
        .forEach(
            t -> {
              try {
                t.join(WAIT_ON_CFA_EXPORT_IN_MS);
              } catch (InterruptedException e) {
                // ignore and continue. It's only the CFA export, so we don't care too much.
              }
            });
  }

  private static PrintStream makePrintStream(OutputStream stream) {
    if (stream instanceof PrintStream printStream) {
      return printStream;
    } else {
      // Use default charset because we output to the terminal
      return new PrintStream(stream, false, Charset.defaultCharset());
    }
  }

  /**
   * Converts a CPAchecker result to a run result.
   *
   * @param result the CPAchecker result received from running CPAchecker
   * @return the converted run result
   */
  private RunResult convertCpacheckerResult(CPAcheckerResult result) {
    RunResult.Verdict verdict = convertVerdict(result);
    return new RunResult(verdict);
  }

  private RunResult.Verdict convertVerdict(CPAcheckerResult result) {
    CPAcheckerResult.Result rawVerdict = result.getResult();
    return switch (rawVerdict) {
      case UNKNOWN -> RunResult.Verdict.UNKNOWN;
      case TRUE -> RunResult.Verdict.TRUE;
      case FALSE -> RunResult.Verdict.FALSE;
      case DONE -> RunResult.Verdict.DONE;
      case NOT_YET_STARTED -> RunResult.Verdict.ERROR;
    };
  }

  @Options
  private static class StatisticsOptions {
    @Option(
        secure = true,
        name = "statistics.export",
        description = "write some statistics to disk")
    private boolean exportStatistics = true;

    @Option(secure = true, name = "statistics.file", description = "write some statistics to disk")
    @FileOption(FileOption.Type.OUTPUT_FILE)
    private Path exportStatisticsFile = Path.of("Statistics.txt");

    @Option(secure = true, name = "statistics.print", description = "print statistics to console")
    private boolean printStatistics = false;
  }
}

// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.process;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import org.eclipse.microprofile.config.inject.ConfigProperty;

/** Options for the JVM that runs CPAchecker. */
@Singleton
public class JvmOptions {
  private final int stackSizeInKb;
  private final int heapSizeInMb;

  @Inject
  JvmOptions(
      @ConfigProperty(name = "cpachecker.grpc.run.jvm.external.stackSizeInKb") int stackSizeInKb,
      @ConfigProperty(name = "cpachecker.grpc.run.jvm.external.heapSizeInMb") int heapSizeInMb) {
    this.stackSizeInKb = stackSizeInKb;
    this.heapSizeInMb = heapSizeInMb;
  }

  public int getStackSizeInKb() {
    return stackSizeInKb;
  }

  public int getHeapSizeInMb() {
    return heapSizeInMb;
  }
}

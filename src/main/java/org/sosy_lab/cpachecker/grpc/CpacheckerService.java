// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc;

import io.quarkus.grpc.GrpcService;
import io.smallrye.mutiny.Uni;
import org.sosy_lab.cpachecker.grpc.proto.Cpachecker;
import org.sosy_lab.cpachecker.grpc.proto.Runner;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.runs.RunException;
import org.sosy_lab.cpachecker.grpc.runs.RunManager;
import org.sosy_lab.cpachecker.grpc.runs.RunState;

/** gRPC service for running CPAchecker. */
@GrpcService
public class CpacheckerService implements Runner {

  private final RunManager runManager;
  private final RunRequestMapper requestMapper;
  private final RunResponseMapper resultMapper;

  /**
   * Creates a new instance of this class.
   *
   * @param runManager the manager for CPAchecker runs
   * @param requestMapper the mapper to use for converting RPC requests to the service's internal
   *     formats
   * @param resultMapper the mapper to use for converting CPAchecker results to responses
   */
  CpacheckerService(
      RunManager runManager, RunRequestMapper requestMapper, RunResponseMapper resultMapper) {
    this.runManager = runManager;
    this.requestMapper = requestMapper;
    this.resultMapper = resultMapper;
  }

  @Override
  public Uni<Cpachecker.RunResponse> startRun(Cpachecker.StartRunRequest request) {
    return Uni.createFrom()
        .item(request)
        .map(requestMapper::toStartRunRequest)
        .map(this::commissionRun);
  }

  private Cpachecker.RunResponse commissionRun(StartRun request) {
    Cpachecker.RunResponse.Builder responseBuilder = Cpachecker.RunResponse.newBuilder();
    try {
      RunState state = runManager.commissionRun(request);
      responseBuilder.setRun(resultMapper.toRun(state));
    } catch (RunException e) {
      responseBuilder.setError(e.getMessage());
    }
    return responseBuilder.build();
  }
}

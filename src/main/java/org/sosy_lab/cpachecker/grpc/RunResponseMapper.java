// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc;

import java.util.List;
import org.mapstruct.CollectionMappingStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.ValueMapping;
import org.mapstruct.ValueMappings;
import org.sosy_lab.cpachecker.grpc.proto.Cpachecker;
import org.sosy_lab.cpachecker.grpc.runs.RunResult;
import org.sosy_lab.cpachecker.grpc.runs.RunState;

@Mapper(
    // We ignore unmapped targets. We map to a protobuf type,
    // which contains multiple target properties as bells and whistles
    // that we do not have to set explicitly.
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    collectionMappingStrategy = CollectionMappingStrategy.ADDER_PREFERRED,
    componentModel = MappingConstants.ComponentModel.CDI)
interface RunResponseMapper {

  List<Cpachecker.Run> toRun(List<RunState> states);

  // We allow mapstruct to ignore unmapped targets.
  // Because of this we provide all expected mappings explicitly,
  // so that they are not missed by accident.
  @Mapping(target = "name", source = "name")
  @Mapping(target = "status", source = "activeness")
  @Mapping(target = "resultVerdict", source = "result.verdict")
  // We ignore run details on purpose. The caller has to explicitly add them
  // with mapDetails.
  Cpachecker.Run toRun(RunState runState);

  @Mapping(target = "stdout", source = "stdout")
  @Mapping(target = "stderr", source = "stderr")
  @Mapping(target = "outputFilesList", source = "outputFiles")
  Cpachecker.RunDetails mapDetails(RunState runState);

  @Mapping(target = "runs", source = "runStates")
  default Cpachecker.ListRunsResponse toListRunsResponse(List<RunState> runStates) {
    return Cpachecker.ListRunsResponse.newBuilder().addAllRuns(toRun(runStates)).build();
  }

  @ValueMappings({
    @ValueMapping(source = "PENDING", target = "PENDING"),
    @ValueMapping(source = "RUNNING", target = "RUNNING"),
    @ValueMapping(source = "FINISHED", target = "FINISHED")
  })
  Cpachecker.RunStatus map(RunState.Activeness activeness);

  default Cpachecker.Verdict map(RunResult.Verdict verdict) {
    if (verdict == null) {
      return Cpachecker.Verdict.UNKNOWN;
    }
    return switch (verdict) {
      case ERROR -> Cpachecker.Verdict.ERROR;
      case UNKNOWN -> Cpachecker.Verdict.UNKNOWN;
      case TRUE -> Cpachecker.Verdict.TRUE;
      case DONE -> Cpachecker.Verdict.DONE;
      case FALSE -> Cpachecker.Verdict.FALSE;
    };
  }
}

// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc;

import com.google.protobuf.ByteString;
import java.nio.ByteBuffer;
import java.util.List;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.ValueMapping;
import org.mapstruct.ValueMappings;
import org.sosy_lab.cpachecker.grpc.proto.Cpachecker;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;

@Mapper(componentModel = MappingConstants.ComponentModel.CDI)
interface RunRequestMapper {

  @Mapping(target = "textOptions", source = "additionalOptionsList")
  @Mapping(target = "fileOptions", source = "additionalOptionsList")
  StartRun toStartRunRequest(Cpachecker.StartRunRequest request);

  @ValueMappings({
    @ValueMapping(source = "UNRECOGNIZED", target = "UNRECOGNIZED"),
    @ValueMapping(source = "UNSPECIFIED", target = "UNSPECIFIED"),
    @ValueMapping(source = "LINUX32", target = "LINUX32"),
    @ValueMapping(source = "LINUX64", target = "LINUX64")
  })
  StartRun.MachineModel toMachineModel(Cpachecker.MachineModel machineModel);

  default List<String> map(Cpachecker.TextValues textValues) {
    return textValues.getValuesList().stream().toList();
  }

  default List<ByteBuffer> map(Cpachecker.FileValues fileValues) {
    return fileValues.getValuesList().stream().map(this::map).toList();
  }

  default ByteBuffer map(ByteString byteString) {
    return byteString.asReadOnlyByteBuffer();
  }

  default List<StartRun.TextOption> mapTextOptions(
      List<Cpachecker.CpacheckerOption> additionalOptions) {
    return additionalOptions.stream()
        .filter(Cpachecker.CpacheckerOption::hasTextValue)
        .map(option -> new StartRun.TextOption(option.getName(), map(option.getTextValue())))
        .toList();
  }

  default List<StartRun.FileOption> mapFileOptions(
      List<Cpachecker.CpacheckerOption> additionalOptions) {
    return additionalOptions.stream()
        .filter(Cpachecker.CpacheckerOption::hasFileValue)
        .map(option -> new StartRun.FileOption(option.getName(), map(option.getFileValue())))
        .toList();
  }
}

#!/usr/bin/env bash

# This file is part of CPA-Daemon,
# a gRPC frontend for CPAchecker:
# https://gitlab.com/sosy-lab/software/cpa-daemon/
#
# SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -euo pipefail

BASE_DIR=$(dirname "$0")
DIST_CPACHECKER="$BASE_DIR"/lib/cpachecker
BIN_CPACHECKER_NATIVE="$BASE_DIR"/lib/cpachecker-native/cpachecker
QUARKUS_JAR="$BASE_DIR"/lib/quarkus-app/quarkus-run.jar

java \
  -Dcpachecker.grpc.run.distribution="$DIST_CPACHECKER" \
  -Dcpachecker.grpc.run.native.binary="$BIN_CPACHECKER_NATIVE" \
  "$@" -jar "$QUARKUS_JAR"
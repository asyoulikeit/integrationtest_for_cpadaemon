extern void reach_error();

// it's important that we do not create a program that produces a long-running analysis,
// because we want our tests to finish fast.
// so we create a very short number of recursions.
// This makes valueAnalysis fail if it's configured to not skip recursions,
// but can still be unrolled fast if an analysis can handle recursion
int recursive_call(int i) {
  if (i > 0) {
    return recursive_call(i-1);
  }
  return i;
}

int main() {
  int i = recursive_call(5);
  if (i > 0) {
    reach_error();
  }
}

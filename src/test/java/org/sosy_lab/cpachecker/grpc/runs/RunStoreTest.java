// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

import java.util.Collection;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/** This interface tests the constracts of {@link RunStore}. */
public interface RunStoreTest {

  String NAME_1 = "Run/1";
  String NAME_2 = "foobar";

  Run RUN_1 = mock(Run.class);
  Run RUN_2 = mock(Run.class);

  /** Returns a new instance of {@link RunStore} for each test. */
  RunStore getRunStore();

  @BeforeEach
  default void setup() {
    lenient().when(RUN_1.getName()).thenReturn(NAME_1);
    lenient().when(RUN_2.getName()).thenReturn(NAME_2);
  }

  @Test
  default void testPutGet() {
    // given
    RunStore runStore = getRunStore();

    // when
    runStore.put(RUN_1);
    Run received = runStore.get(RUN_1.getName());

    // then
    assertThat(RUN_1, is(received));
  }

  @Test
  default void testPutGet_multipleRuns() {
    // given
    RunStore runStore = getRunStore();

    // when
    runStore.put(RUN_1);
    runStore.put(RUN_2);
    Run received1 = runStore.get(RUN_1.getName());
    Run received2 = runStore.get(RUN_2.getName());

    // then
    assertThat(RUN_1, is(received1));
    assertThat(RUN_2, is(received2));
  }

  @Test
  default void testPutGet_repeatedGet() {
    // given
    RunStore runStore = getRunStore();

    // when
    runStore.put(RUN_1);
    Run received1 = runStore.get(RUN_1.getName());
    Run received2 = runStore.get(RUN_1.getName());
    Run received3 = runStore.get(RUN_1.getName());

    // then
    assertThat(RUN_1, is(received1));
    assertThat(RUN_1, is(received2));
    assertThat(RUN_1, is(received3));
  }

  @Test
  default void testGet_missing() {
    // given
    RunStore runStore = getRunStore();

    // when
    Run received = runStore.get(RUN_1.getName());

    // then
    assertThat(received, is(nullValue()));
  }

  @Test
  default void testGetAll_multiple() {
    // given
    RunStore runStore = getRunStore();

    // when
    runStore.put(RUN_1);
    runStore.put(RUN_2);

    Collection<Run> received = runStore.getAll();

    // then
    assertThat(received, containsInAnyOrder(RUN_1, RUN_2));
  }

  @Test
  default void testGetAll_empty() {
    // given
    RunStore runStore = getRunStore();

    // when
    Collection<Run> received = runStore.getAll();

    // then
    assertThat(received, empty());
  }

  @Test
  default void testClear_multipleValues() {
    // given
    RunStore runStore = getRunStore();

    // when
    runStore.put(RUN_1);
    runStore.put(RUN_2);
    runStore.clear();

    // get requests
    Run received1 = runStore.get(RUN_1.getName());
    Run received2 = runStore.get(RUN_2.getName());

    // then received values are null
    assertThat(received1, is(nullValue()));
    assertThat(received2, is(nullValue()));
  }

  @Test
  default void testClear_noValues() {
    // given
    RunStore runStore = getRunStore();

    // when
    runStore.clear();

    // get requests
    Run received1 = runStore.get(RUN_1.getName());
    Run received2 = runStore.get(RUN_2.getName());

    // then received values are null
    assertThat(received1, is(nullValue()));
    assertThat(received2, is(nullValue()));
  }
}

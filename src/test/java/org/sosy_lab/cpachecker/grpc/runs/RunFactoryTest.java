// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

import org.junit.jupiter.api.Test;

/** This interface tests the contract of {@link RunFactory}. */
public interface RunFactoryTest {

  String SOME_NAME = "someName";

  RunFactory getFactory();

  @Test
  default void testCreate() {
    // given
    RunFactory factory = getFactory();

    // when
    Run createdRun = factory.create(SOME_NAME);

    // then
    assertThat(createdRun, not(nullValue()));
  }
}

// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.process;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.getBasicRequest;

import java.io.IOException;
import java.nio.file.Path;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.runs.RunConfigMapper;
import org.sosy_lab.cpachecker.grpc.runs.RunException;
import org.sosy_lab.cpachecker.grpc.runs.TestUtils;
import org.sosy_lab.cpachecker.grpc.util.Async;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerProcessBuilder;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

@ExtendWith(MockitoExtension.class)
class ExternalJvmRunTest implements AbstractProcessRunTest {

  private static final int STACK_SIZE_KB = 1024;
  private static final int HEAP_SIZE_MB = 1200;
  private static final int TIMELIMIT_SECONDS = 30; // half a minute per CPAchecker run
  private static final String DEFAULT_NAME = "someName";

  private final RunConfigMapper configMapper = new RunConfigMapper();
  private final CpacheckerSupport cpacheckerSupport = new CpacheckerSupport();
  private final Async async = new Async();
  private final CpacheckerProcessBuilder processBuilder = new CpacheckerProcessBuilder(async);
  private final Path cpacheckerDirectory = Path.of("build/cpachecker");

  @Mock private DirectoryManager directoryManager;

  private final JvmOptions jvmOptions = new JvmOptions(STACK_SIZE_KB, HEAP_SIZE_MB);

  private final ProcessOptions processOptions =
      new ProcessOptions(cpacheckerDirectory, TIMELIMIT_SECONDS);

  @BeforeEach
  void setUpRunDirectory() throws IOException {
    Path tempPath = mutableRunContext.getCurrentOutputDirectory();
    DirectoryManager.RunDirectory runDirectory = TestUtils.getRunDirectory(tempPath, tempPath);
    // these stubs may not be used (e.g., in case a test expects an early error).
    // But the expected return values are the same for all tests, so define it here instead of
    lenient().when(directoryManager.createNewRunDirectory()).thenReturn(runDirectory);
  }

  @Override
  public ExternalJvmRun getRun() {
    return new ExternalJvmRun(
        DEFAULT_NAME,
        jvmOptions,
        processOptions,
        configMapper,
        cpacheckerSupport,
        directoryManager,
        processBuilder);
  }

  @Test
  void testStart_configCreationFails() throws IOException {
    // given
    RunConfigMapper mapperMock = mock(RunConfigMapper.class);
    when(mapperMock.createRunConfig(any(), any())).thenThrow(IOException.class);
    ExternalJvmRun run =
        new ExternalJvmRun(
            DEFAULT_NAME,
            jvmOptions,
            processOptions,
            mapperMock,
            cpacheckerSupport,
            directoryManager,
            processBuilder);
    StartRun runRequest = getBasicRequest(SOME_PROGRAM, CONFIG_WITHOUT_VERIFICATION, null);

    // when
    Executable startRun = () -> run.start(runRequest);

    // then a RunException is thrown
    assertThrows(RunException.class, startRun);
  }

  @Test
  void testStart_directoryCreationFails() throws IOException {
    // given
    when(directoryManager.createNewRunDirectory()).thenThrow(IOException.class);
    ExternalJvmRun run = getRun();
    StartRun runRequest = getBasicRequest(SOME_PROGRAM, CONFIG_WITHOUT_VERIFICATION, null);

    // when
    Executable startRun = () -> run.start(runRequest);

    // then a RunException is thrown
    assertThrows(RunException.class, startRun);
  }
}

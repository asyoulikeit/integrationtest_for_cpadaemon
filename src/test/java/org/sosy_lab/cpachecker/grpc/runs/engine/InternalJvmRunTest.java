// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.engine;

import static org.mockito.Mockito.lenient;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.sosy_lab.cpachecker.grpc.runs.Run;
import org.sosy_lab.cpachecker.grpc.runs.RunConfigMapper;
import org.sosy_lab.cpachecker.grpc.runs.RunTest;
import org.sosy_lab.cpachecker.grpc.runs.TestUtils;
import org.sosy_lab.cpachecker.grpc.util.Async;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

/**
 * Set up the run directory for a {@link Run} and stub the {@link DirectoryManager} to return the
 * same directory for all tests, as well as a logging file within that directory.
 */
@ExtendWith(MockitoExtension.class)
public class InternalJvmRunTest implements RunTest {

  private static final String DEFAULT_NAME = "someName";
  private final RunConfigMapper configMapper = new RunConfigMapper();
  private final CpacheckerSupport cpacheckerSupport = new CpacheckerSupport();
  private final Async async = new Async();

  private final int timelimitInSeconds = 10;
  private final CpacheckerRunnerFactory runnerFactory =
      new CpacheckerRunnerFactory(timelimitInSeconds, cpacheckerSupport);

  @Mock private DirectoryManager directoryManager;

  @BeforeEach
  void setUpRunDirectory() throws IOException {
    Path tempPath = mutableRunContext.getCurrentOutputDirectory();
    DirectoryManager.RunDirectory runDirectory = TestUtils.getRunDirectory(tempPath, tempPath);
    Path logFile = Files.createTempFile(tempPath, "log", ".txt");
    TestUtils.addLoggingFile(runDirectory, logFile);
    // these stubs may not be used (e.g., in case a test expects an early error).
    // But the expected return values are the same for all tests, so define it here instead of
    lenient().when(directoryManager.createNewRunDirectory()).thenReturn(runDirectory);
  }

  @Override
  public Run getRun() {
    return new InternalJvmRun(
        DEFAULT_NAME, configMapper, cpacheckerSupport, directoryManager, runnerFactory, async);
  }
}

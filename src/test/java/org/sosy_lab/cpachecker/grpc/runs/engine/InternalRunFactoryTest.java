// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.engine;

import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.sosy_lab.cpachecker.grpc.runs.RunConfigMapper;
import org.sosy_lab.cpachecker.grpc.runs.RunFactory;
import org.sosy_lab.cpachecker.grpc.runs.RunFactoryTest;
import org.sosy_lab.cpachecker.grpc.util.Async;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

/** Runs the interface tests for {@link InternalJvmRunFactory}. */
@ExtendWith(MockitoExtension.class)
public class InternalRunFactoryTest implements RunFactoryTest {
  /*
  We just mock all components of the JVM run factory. We do not have to mock any concrete behavior
  because the factory should simply pass the components to the Run, and not use them.
   */
  @Mock private RunConfigMapper configMapper;
  @Mock private CpacheckerSupport cpacheckerSupport;
  @Mock private DirectoryManager directoryManager;
  @Mock private CpacheckerRunnerFactory runnerFactory;
  @Mock private Async async;

  @Override
  public RunFactory getFactory() {
    return new InternalJvmRunFactory(
        cpacheckerSupport, configMapper, directoryManager, runnerFactory, async);
  }
}

// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.process;

import java.nio.file.Path;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.sosy_lab.cpachecker.grpc.runs.RunConfigMapper;
import org.sosy_lab.cpachecker.grpc.runs.RunFactory;
import org.sosy_lab.cpachecker.grpc.runs.RunFactoryTest;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerProcessBuilder;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

@ExtendWith(MockitoExtension.class)
class NativeBinaryRunFactoryTest implements RunFactoryTest {
  /*
  We just mock all components of the JVM run factory. We do not have to mock any concrete behavior
  because the factory should simply pass the components to the Run, and not use them.
   */
  @Mock private ProcessOptions processOptions;
  @Mock private RunConfigMapper configMapper;
  @Mock private CpacheckerSupport cpacheckerSupport;
  @Mock private CpacheckerProcessBuilder processBuilder;
  @Mock private DirectoryManager directoryManager;
  @Mock private Path nativeBinaryPath;

  @Override
  public RunFactory getFactory() {
    return new NativeBinaryRunFactory(
        processOptions,
        cpacheckerSupport,
        configMapper,
        directoryManager,
        processBuilder,
        nativeBinaryPath);
  }
}

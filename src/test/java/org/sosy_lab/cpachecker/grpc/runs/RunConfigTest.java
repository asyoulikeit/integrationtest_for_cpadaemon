// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.ENTRY_FUNCTION_DEFAULT;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.MACHINE_MODEL_DEFAULT;

import java.nio.file.Path;
import org.junit.jupiter.api.Test;

class RunConfigTest {

  private static final String VALID_CONFIG = "valueAnalysis.properties";
  private static final String VALID_PROGRAM = "safe.c";
  private static final String VALID_SPEC = "unreach-call.spc";
  private static final String VALID_WITNESS = "violationWitness-correct.graphml";

  @Test
  void testConstructor_nullProgramThrows() {
    assertThrows(
        NullPointerException.class,
        () ->
            new RunConfig(
                null,
                getFile(VALID_CONFIG),
                null,
                null,
                MACHINE_MODEL_DEFAULT,
                ENTRY_FUNCTION_DEFAULT,
                null));
  }

  @Test
  void testConstructor_nullConfigThrows() {
    assertThrows(
        NullPointerException.class,
        () ->
            new RunConfig(
                getFile(VALID_PROGRAM),
                null,
                null,
                null,
                MACHINE_MODEL_DEFAULT,
                ENTRY_FUNCTION_DEFAULT,
                null));
  }

  @Test
  void testConstructor_nullMachineModelThrows() {
    assertThrows(
        NullPointerException.class,
        () ->
            new RunConfig(
                getFile(VALID_PROGRAM),
                getFile(VALID_CONFIG),
                null,
                null,
                null,
                ENTRY_FUNCTION_DEFAULT,
                null));
  }

  @Test
  void testConstructor_validValuesIsOk() {
    assertDoesNotThrow(
        () ->
            new RunConfig(
                getFile(VALID_PROGRAM),
                getFile(VALID_CONFIG),
                null,
                null,
                RunConfig.MachineModel.LINUX32,
                "main",
                null));
    assertDoesNotThrow(
        () ->
            new RunConfig(
                getFile(VALID_PROGRAM),
                getFile(VALID_CONFIG),
                getFile(VALID_SPEC),
                null,
                RunConfig.MachineModel.LINUX64,
                "foo",
                getFile(VALID_WITNESS)));
  }

  private static Path getFile(String name) {
    return TestUtils.getRunnerFile(name);
  }
}

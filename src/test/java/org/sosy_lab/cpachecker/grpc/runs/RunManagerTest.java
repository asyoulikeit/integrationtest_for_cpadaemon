// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.time.Duration;
import java.util.List;
import java.util.Optional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.sosy_lab.cpachecker.grpc.requests.CancelRun;
import org.sosy_lab.cpachecker.grpc.requests.GetFile;
import org.sosy_lab.cpachecker.grpc.requests.GetRun;
import org.sosy_lab.cpachecker.grpc.requests.ListRuns;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.requests.WaitRun;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

@ExtendWith(MockitoExtension.class)
class RunManagerTest {
  private static final Path PROGRAM_FILE = TestUtils.getRunnerFile("safe.c");
  private static final Path CONFIG_FILE = TestUtils.getRunnerFile("valueAnalysis.properties");
  private static final Path SPECIFICATION_FILE = TestUtils.getRunnerFile("unreach-call.spc");
  private static final String PROGRAM_CODE = TestUtils.getContentAsString(PROGRAM_FILE);

  private static final String CONFIG_VALUES = TestUtils.getContentAsString(CONFIG_FILE);
  private static final String SPECIFICATION = TestUtils.getContentAsString(SPECIFICATION_FILE);

  private static final StartRun REQUEST_VALID =
      new StartRun(
          PROGRAM_CODE,
          CONFIG_VALUES,
          SPECIFICATION,
          null,
          null,
          StartRun.MachineModel.LINUX32,
          "main",
          null);

  private static final String NAME_EXISTING_RUN = "someName";
  private static final String NAME_EXISTING_RUN_2 = "Run/42";
  private static final String NAME_MISSING_RUN = "otherName";
  private static final long CREATION_TIME = 9999;

  private static final RunState STATE_EXPECTED =
      RunState.builder()
          .name(NAME_EXISTING_RUN)
          .activeness(RunState.Activeness.PENDING)
          .timestamp(CREATION_TIME)
          .build();

  private static final RunState STATE_2 =
      RunState.builder()
          .name(NAME_EXISTING_RUN_2)
          .activeness(RunState.Activeness.PENDING)
          .timestamp(CREATION_TIME)
          .build();

  private static final Duration TIMEOUT_LONG_ENOUGH = Duration.ofSeconds(20);
  private static final String OUTPUT_FILE_NAME = "output.txt";

  @Mock private RunFactory runFactory;
  @Mock private Run run;
  @Mock private Run run2;
  @Mock private RunStore runStore;
  @Mock private CpacheckerSupport cpacheckerSupport;
  @Mock private DirectoryManager directoryManager;
  @Mock private DirectoryManager.RunDirectory runDirectory;

  private RunManager runManager;

  @BeforeEach
  void setUp(@TempDir Path outputDir) throws IOException {
    // these stubs may not be used,
    // but the expected return values are the same for all tests, so define it here instead of
    lenient().when(runFactory.create(any())).thenReturn(run).thenReturn(run2);
    lenient().when(run.getState()).thenReturn(STATE_EXPECTED);
    lenient().when(run.getName()).thenReturn(NAME_EXISTING_RUN);
    lenient().when(run2.getState()).thenReturn(STATE_2);
    lenient().when(run.getName()).thenReturn(NAME_EXISTING_RUN_2);
    lenient().when(directoryManager.createNewRunDirectory()).thenReturn(runDirectory);
    // we don't know whether input- and output-directories are actively accessed by the service
    // or just forwarded to subcomponents (which are mocked in this class). So these stubs may not
    // be used.
    // But we are lenient about it to avoid encoding implementation details in the mocks.
    lenient().when(runDirectory.getCurrentOutputDirectory()).thenReturn(outputDir);
    // use the same directory on purpose; it shouldn't hurt because we are not running anything
    lenient().when(runDirectory.getCurrentInputDirectory()).thenReturn(outputDir);

    runManager = new RunManager(runFactory, cpacheckerSupport, runStore);
  }

  @Test
  void testCommissionRun_validRequest() throws RunException {
    // when
    RunState initialRunState = runManager.commissionRun(REQUEST_VALID);

    // then
    assertThat(initialRunState, equalTo(STATE_EXPECTED));
  }

  @Test
  void testCommissionRun_startFails() throws RunException {
    String errorMessage = "invalid request";
    doThrow(new RunException(errorMessage)).when(run).start(any());
    assertThrows(RunException.class, () -> runManager.commissionRun(REQUEST_VALID));
  }

  @Test
  void testGetRun_runExists() {
    // given
    when(runStore.get(NAME_EXISTING_RUN)).thenReturn(run);
    GetRun getRequest = new GetRun(NAME_EXISTING_RUN);

    // when
    Optional<RunState> retrievedRun = runManager.getRun(getRequest);

    // then
    assertThat(retrievedRun.isPresent(), equalTo(true));
    assertThat(retrievedRun.get(), equalTo(run.getState()));
    verify(run, atLeast(1)).getState();
  }

  @Test
  void testGetRun_runMissing() {
    // given
    when(runStore.get(NAME_MISSING_RUN)).thenReturn(null);
    GetRun getRequest = new GetRun(NAME_MISSING_RUN);

    // when
    Optional<RunState> retrievedRun = runManager.getRun(getRequest);

    // then
    assertThat(retrievedRun.isPresent(), equalTo(false));
  }

  @Test
  void testCancelRun_runExists() {
    // given
    when(runStore.get(NAME_EXISTING_RUN)).thenReturn(run);
    CancelRun cancelRequest = new CancelRun(NAME_EXISTING_RUN);

    // when
    Optional<RunState> cancelledRun = runManager.cancelRun(cancelRequest);

    // then
    assertThat(cancelledRun.isPresent(), equalTo(true));
    assertThat(cancelledRun.get(), equalTo(run.getState()));
    verify(run).stop();
  }

  @Test
  void testCancelRun_runMissing() {
    // given
    when(runStore.get(NAME_MISSING_RUN)).thenReturn(null);
    CancelRun cancelRequest = new CancelRun(NAME_MISSING_RUN);

    // when
    Optional<RunState> retrievedRun = runManager.cancelRun(cancelRequest);

    // then
    assertThat(retrievedRun.isPresent(), equalTo(false));
  }

  @Test
  void testListRuns_noRuns() {
    // given
    when(runStore.getAll()).thenReturn(List.of());
    ListRuns listRequest = new ListRuns();

    // when
    List<RunState> retrievedRuns = runManager.listRuns(listRequest);

    // then
    assertThat(retrievedRuns, empty());
  }

  @Test
  void testListRuns_singleRun() {
    // given
    when(runStore.getAll()).thenReturn(List.of(run));
    ListRuns listRequest = new ListRuns();

    // when
    List<RunState> retrievedRuns = runManager.listRuns(listRequest);

    // then
    assertThat(retrievedRuns, contains(run.getState()));
  }

  @Test
  void testListRuns_multipleRuns() throws RunException {
    // given
    when(runStore.getAll()).thenReturn(List.of(run, run2));
    ListRuns listRequest = new ListRuns();

    // when
    runManager.commissionRun(REQUEST_VALID);
    runManager.commissionRun(REQUEST_VALID);
    List<RunState> retrievedRuns = runManager.listRuns(listRequest);

    // then
    assertThat(retrievedRuns, contains(run.getState(), run2.getState()));
  }

  @Test
  void testWaitRun_runExistsAndWaitFinishes() throws InterruptedException {
    // given
    when(runStore.get(NAME_EXISTING_RUN)).thenReturn(run);
    when(run.waitUntilFinished(anyLong())).thenReturn(STATE_EXPECTED);
    WaitRun waitRequest = new WaitRun(NAME_EXISTING_RUN, TIMEOUT_LONG_ENOUGH);

    // when
    Optional<RunState> retrievedRun = runManager.waitRun(waitRequest);

    // then
    assertThat(retrievedRun.isPresent(), equalTo(true));
    assertThat(retrievedRun.get(), equalTo(STATE_EXPECTED));
  }

  @Test
  void testWaitRun_runExistsButWaitInterrupted() throws InterruptedException {
    // given
    when(runStore.get(NAME_EXISTING_RUN)).thenReturn(run);
    when(run.waitUntilFinished(anyLong())).thenThrow(InterruptedException.class);
    when(run.getState()).thenReturn(STATE_EXPECTED);
    WaitRun waitRequest = new WaitRun(NAME_EXISTING_RUN, TIMEOUT_LONG_ENOUGH);

    // when
    Optional<RunState> retrievedRun = runManager.waitRun(waitRequest);

    // then
    assertThat(retrievedRun.isPresent(), equalTo(true));
    assertThat(retrievedRun.get(), equalTo(STATE_EXPECTED));
  }

  @Test
  void testWaitRun_runMissing() {
    // given
    when(runStore.get(NAME_MISSING_RUN)).thenReturn(null);
    WaitRun waitRequest = new WaitRun(NAME_MISSING_RUN, TIMEOUT_LONG_ENOUGH);

    // when
    Optional<RunState> retrievedRun = runManager.waitRun(waitRequest);

    // then
    assertThat(retrievedRun.isPresent(), equalTo(false));
  }

  @Test
  void testGetOutputFile_exists() {
    // given
    ByteBuffer outputFileContent = ByteBuffer.wrap("some content".getBytes());
    when(runStore.get(NAME_EXISTING_RUN)).thenReturn(run);
    when(run.getOutputFile(OUTPUT_FILE_NAME)).thenReturn(outputFileContent);
    GetFile getFileRequest = new GetFile(NAME_EXISTING_RUN, OUTPUT_FILE_NAME);

    // when
    Optional<ByteBuffer> retrievedOutputFile = runManager.getOutputFile(getFileRequest);

    // then
    assertThat(retrievedOutputFile.isPresent(), equalTo(true));
    assertThat(retrievedOutputFile.get(), equalTo(outputFileContent));
  }

  @Test
  void testGetOutputFile_missing() {
    // given
    when(runStore.get(NAME_MISSING_RUN)).thenReturn(null);
    GetFile getFileRequest = new GetFile(NAME_MISSING_RUN, OUTPUT_FILE_NAME);

    // when
    Optional<ByteBuffer> retrievedOutputFile = runManager.getOutputFile(getFileRequest);

    // then
    assertThat(retrievedOutputFile.isPresent(), equalTo(false));
  }
}

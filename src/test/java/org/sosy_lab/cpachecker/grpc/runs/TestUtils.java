// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;

import com.google.protobuf.ByteString;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import lombok.experimental.UtilityClass;
import org.sosy_lab.cpachecker.grpc.proto.Cpachecker;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

/** Test utils for handling CPAchecker runners in test settings. */
@UtilityClass
public class TestUtils {
  public static final RunConfig.MachineModel MACHINE_MODEL_DEFAULT = RunConfig.MachineModel.LINUX32;
  public static final String ENTRY_FUNCTION_DEFAULT = "main";
  private static final Charset CHARSET = StandardCharsets.UTF_8;

  /** Returns a basic configuration with the given program, configuration and specification. */
  public static StartRun getBasicRequest(
      String programName, String configName, String specificationName) {
    final String specification;
    if (specificationName != null) {
      specification = getContentAsString(getRunnerFile(specificationName));
    } else {
      specification = "";
    }
    return new StartRun(
        /* programCode= */ getContentAsString(getRunnerFile(programName)),
        /* config= */ getContentAsString(getRunnerFile(configName)),
        /* specification= */ specification,
        /* textOptions= */ List.of(),
        /* fileOptions= */ List.of(),
        StartRun.MachineModel.LINUX32,
        ENTRY_FUNCTION_DEFAULT,
        /* witness= */ "");
  }

  /** Returns the file with the given name. */
  public static Path getRunnerFile(String s) {
    return Path.of(TestUtils.class.getResource(s).getFile());
  }

  /** Returns the content of the given file. */
  public static String getContentAsString(Path file) {
    try {
      return Files.readString(file, CHARSET);
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

  public static ByteBuffer getContentAsBytes(Path file) throws IOException {
    return ByteBuffer.wrap(Files.readAllBytes(file));
  }

  public static Cpachecker.TextValues value(String value) {
    return Cpachecker.TextValues.newBuilder().addValues(value).build();
  }

  public static Cpachecker.FileValues value(ByteBuffer value) {
    ByteString byteString = ByteString.copyFrom(value);
    return Cpachecker.FileValues.newBuilder().addValues(byteString).build();
  }

  /**
   * Returns a {@link DirectoryManager.RunDirectory} with the given paths. The run directory is not
   * managed: the given paths will not be created and not be deleted at any point in time. The
   * caller must make sure that the paths exist and are deleted when not needed anymore.
   *
   * @param inputDir input directory that {@link
   *     DirectoryManager.RunDirectory#getCurrentInputDirectory()} should return
   * @param outputDir output directory that {@link
   *     DirectoryManager.RunDirectory#getCurrentOutputDirectory()} should return
   */
  public static DirectoryManager.RunDirectory getRunDirectory(Path inputDir, Path outputDir) {
    DirectoryManager.RunDirectory runDirectory = mock(DirectoryManager.RunDirectory.class);
    // lenient because the methods don't have to be called by the caller;
    // we don't know what they want to do.
    lenient().when(runDirectory.getCurrentInputDirectory()).thenReturn(inputDir);
    lenient().when(runDirectory.getCurrentOutputDirectory()).thenReturn(outputDir);
    lenient()
        .when(runDirectory.getConfigLocation())
        .thenReturn(inputDir.resolve("config.properties"));
    return runDirectory;
  }

  /**
   * Adds a logging file to a given {@link DirectoryManager.RunDirectory}. The logging file is not
   * managed: the given path will not be created and not be deleted at any point in time. The caller
   * must make sure that the path exists and is deleted when not needed anymore.
   *
   * @param runDirectory the run directory to add the logging file to
   * @param logFile the logging file that {@link DirectoryManager.RunDirectory#getNewLoggingFile()}
   *     should return
   * @throws IOException when {@link DirectoryManager.RunDirectory#getNewLoggingFile()} produces an
   *     I/O error
   */
  public static void addLoggingFile(DirectoryManager.RunDirectory runDirectory, Path logFile)
      throws IOException {
    lenient().when(runDirectory.getNewLoggingFile()).thenReturn(logFile);
  }
}

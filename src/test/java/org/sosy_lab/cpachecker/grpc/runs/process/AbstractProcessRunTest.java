// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.process;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.blankOrNullString;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.spy;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.getBasicRequest;

import com.google.common.collect.ImmutableList;
import java.nio.file.Path;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.runs.RunException;
import org.sosy_lab.cpachecker.grpc.runs.RunTest;
import org.sosy_lab.cpachecker.grpc.runs.TestUtils;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

interface AbstractProcessRunTest extends RunTest {

  @Override
  AbstractProcessRun getRun();

  @Test
  default void testStart_createCommandFails() throws RunException {
    // given createCommand fails
    AbstractProcessRun failingRun = spy(getRun());
    doThrow(RunException.class).when(failingRun).createCommand(any(), any());

    StartRun runRequest = getBasicRequest(SOME_PROGRAM, CONFIG_WITHOUT_VERIFICATION, null);

    // when
    Executable createCommand = () -> failingRun.start(runRequest);

    // then the RunException is propagated by the start method
    assertThrows(RunException.class, createCommand);
  }

  @Test
  default void testCreateCommand_succeeds() throws RunException {
    // given
    AbstractProcessRun run = getRun();

    StartRun runRequest = getBasicRequest(SOME_PROGRAM, CONFIG_WITHOUT_VERIFICATION, null);
    DirectoryManager.RunDirectory runDirectory = getDirectory();

    // when
    ImmutableList<String> command = run.createCommand(runRequest, runDirectory);

    // then there should be at least one CPAchecker executable and one argument
    // for the program to analyze. Everything else may be optional.
    assertThat(command, notNullValue());
    assertThat(command.size(), greaterThanOrEqualTo(2));
    for (String s : command) {
      assertThat(s, not(blankOrNullString()));
    }
  }

  private DirectoryManager.RunDirectory getDirectory() {
    Path outputDir = mutableRunContext.getCurrentOutputDirectory();
    // use the actual output directory for both input and output files.
    // Reason: it doesn't matter where the files are written.
    return TestUtils.getRunDirectory(outputDir, outputDir);
  }
}

// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.getContentAsString;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.getRunnerFile;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.util.DirectoryManager;

class RunConfigMapperTest {

  private static final String PROGRAM_CODE = "int main() { return 1; }";
  private static final String CONFIG_CODE =
      """
cpa = cpa.arg.ARGCPA
ARGCPA.cpa = cpa.composite.CompositeCPA
CompositeCPA.cpas = cpa.location.LocationCPA, \\
                    cpa.callstack.CallstackCPA, \\
                    cpa.functionpointer.FunctionPointerCPA, \\
                    cpa.value.ValueAnalysisCPA, $specification""";

  private static final String SPECIFICATION_CODE =
      """
CONTROL AUTOMATON SVCOMP

INITIAL STATE Init;

STATE USEFIRST Init :
  MATCH {__VERIFIER_error($?)} || MATCH {reach_error($?)} || MATCH FUNCTIONCALL "reach_error"
      -> ERROR("unreach-call: $rawstatement called in $location");
  MATCH {__assert_fail($?)} || MATCH {abort($?)} || MATCH {exit($?)} -> STOP;

END AUTOMATON""";

  private static final String WITNESS_CODE =
      """
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
</graphml>""";
  private static final String DEFAULT_ENTRY_FUNCTION = "main";
  private static final StartRun.MachineModel MACHINE_MODEL_REQUEST = StartRun.MachineModel.LINUX32;
  private static final RunConfig.MachineModel MACHINE_MODEL_RESPONSE =
      RunConfig.MachineModel.LINUX32;

  private static final String OPTION_NAME_TEXT_VALUE = "analysis.alwaysStoreCounterexamples";
  private static final String OPTION_NAME_FILE_VALUE = "witness.validation.violation.config";
  private static final String OPTION_VALUE_TEXT_VALUE_1 = "true";
  private static final String OPTION_VALUE_TEXT_VALUE_2 = "foo";
  private static final String WITNESS_FILE = "witnessValidation.properties";

  private final RunConfigMapper mapper = new RunConfigMapper();

  @Test
  void testCreateRunConfig_requestWithMandatoryValues(
      @TempDir Path outputDirectory, @TempDir Path inputDirectory)
      throws IOException, RunException {
    // given
    DirectoryManager.RunDirectory directory = createMockDirectory(outputDirectory, inputDirectory);
    StartRun request =
        new StartRun(
            PROGRAM_CODE, CONFIG_CODE, "", List.of(), List.of(), MACHINE_MODEL_REQUEST, "", "");

    // when
    RunConfig result = mapper.createRunConfig(request, directory);

    // then
    checkFileHasContent(result.getProgramFile(), PROGRAM_CODE);
    checkFileHasContent(result.getCpacheckerConfig(), CONFIG_CODE);
    assertThat(result.getMachineModel(), is(MACHINE_MODEL_RESPONSE));
    assertThat(result.getSpecification(), nullValue());
    assertThat(result.getWitness(), nullValue());
    assertThat(result.getAdditionalOptions(), empty());
    assertThat(result.getEntryFunction(), is(DEFAULT_ENTRY_FUNCTION));
  }

  @Test
  void testCreateRunConfig_requestWithSpecification(
      @TempDir Path outputDirectory, @TempDir Path inputDirectory)
      throws IOException, RunException {
    // given
    DirectoryManager.RunDirectory directory = createMockDirectory(outputDirectory, inputDirectory);
    StartRun request =
        new StartRun(
            PROGRAM_CODE,
            CONFIG_CODE,
            SPECIFICATION_CODE,
            List.of(),
            List.of(),
            MACHINE_MODEL_REQUEST,
            "",
            "");

    // when
    RunConfig result = mapper.createRunConfig(request, directory);

    // then
    checkFileHasContent(result.getProgramFile(), PROGRAM_CODE);
    checkFileHasContent(result.getCpacheckerConfig(), CONFIG_CODE);
    assertThat(result.getMachineModel(), is(MACHINE_MODEL_RESPONSE));
    checkFileHasContent(result.getSpecification(), SPECIFICATION_CODE);
    assertThat(result.getWitness(), nullValue());
    assertThat(result.getAdditionalOptions(), empty());
    assertThat(result.getEntryFunction(), is(DEFAULT_ENTRY_FUNCTION));
  }

  @Test
  void testCreateRunConfig_requestWithEntryFunction(
      @TempDir Path outputDirectory, @TempDir Path inputDirectory)
      throws IOException, RunException {
    // given
    DirectoryManager.RunDirectory directory = createMockDirectory(outputDirectory, inputDirectory);
    String entryFunction = "foobar";
    StartRun request =
        new StartRun(
            PROGRAM_CODE,
            CONFIG_CODE,
            "",
            List.of(),
            List.of(),
            MACHINE_MODEL_REQUEST,
            entryFunction,
            "");

    // when
    RunConfig result = mapper.createRunConfig(request, directory);

    // then
    checkFileHasContent(result.getProgramFile(), PROGRAM_CODE);
    checkFileHasContent(result.getCpacheckerConfig(), CONFIG_CODE);
    assertThat(result.getMachineModel(), is(MACHINE_MODEL_RESPONSE));
    assertThat(result.getSpecification(), nullValue());
    assertThat(result.getWitness(), nullValue());
    assertThat(result.getAdditionalOptions(), empty());
    assertThat(result.getEntryFunction(), is(entryFunction));
  }

  @Test
  void testCreateRunConfig_requestWithWitness(
      @TempDir Path outputDirectory, @TempDir Path inputDirectory) throws IOException {
    // given
    DirectoryManager.RunDirectory directory = createMockDirectory(outputDirectory, inputDirectory);
    StartRun request =
        new StartRun(
            PROGRAM_CODE,
            CONFIG_CODE,
            "",
            List.of(),
            List.of(),
            MACHINE_MODEL_REQUEST,
            "",
            WITNESS_CODE);

    // when
    RunConfig result = mapper.createRunConfig(request, directory);

    // then
    checkFileHasContent(result.getProgramFile(), PROGRAM_CODE);
    checkFileHasContent(result.getCpacheckerConfig(), CONFIG_CODE);
    assertThat(result.getMachineModel(), is(MACHINE_MODEL_RESPONSE));
    assertThat(result.getSpecification(), nullValue());
    checkFileHasContent(result.getWitness(), WITNESS_CODE);
    assertThat(result.getAdditionalOptions(), empty());
    assertThat(result.getEntryFunction(), is(DEFAULT_ENTRY_FUNCTION));
  }

  @Test
  void testCreateRunConfig_requestWithAdditionalOptions(
      @TempDir Path outputDirectory, @TempDir Path inputDirectory)
      throws IOException, RunException {
    // given
    DirectoryManager.RunDirectory directory = createMockDirectory(outputDirectory, inputDirectory);
    Path fileForFilevalueOption = getRunnerFile(WITNESS_FILE);
    ByteBuffer fileValueContent = ByteBuffer.wrap(Files.readAllBytes(fileForFilevalueOption));
    StartRun.TextOption textOption =
        new StartRun.TextOption(
            OPTION_NAME_TEXT_VALUE, List.of(OPTION_VALUE_TEXT_VALUE_1, OPTION_VALUE_TEXT_VALUE_2));
    StartRun.FileOption fileOption =
        new StartRun.FileOption(OPTION_NAME_FILE_VALUE, List.of(fileValueContent));
    StartRun request =
        new StartRun(
            PROGRAM_CODE,
            CONFIG_CODE,
            "",
            List.of(textOption),
            List.of(fileOption),
            MACHINE_MODEL_REQUEST,
            "",
            "");

    // when
    RunConfig result = mapper.createRunConfig(request, directory);

    // then
    checkFileHasContent(result.getProgramFile(), PROGRAM_CODE);
    checkFileHasContent(result.getCpacheckerConfig(), CONFIG_CODE);
    assertThat(result.getMachineModel(), is(MACHINE_MODEL_RESPONSE));
    assertThat(result.getSpecification(), nullValue());
    assertThat(result.getWitness(), nullValue());
    assertThat(result.getEntryFunction(), is(DEFAULT_ENTRY_FUNCTION));

    // check that both additional options are set
    RunConfig.Option textvalueOption = null;
    RunConfig.Option filevalueOption = null;
    for (var opt : result.getAdditionalOptions()) {
      if (opt.key().equals(OPTION_NAME_TEXT_VALUE)) {
        textvalueOption = opt;
      } else if (opt.key().equals(OPTION_NAME_FILE_VALUE)) {
        filevalueOption = opt;
      } else {
        throw new AssertionError("Unexpected option: " + opt);
      }
    }
    assertThat(textvalueOption, notNullValue());
    assertThat(filevalueOption, notNullValue());

    // and check that the option values are as given
    assertThat(
        textvalueOption.value(),
        is(String.join(",", OPTION_VALUE_TEXT_VALUE_1, OPTION_VALUE_TEXT_VALUE_2)));
    checkFileHasContent(
        Path.of(filevalueOption.value()), getContentAsString(fileForFilevalueOption));
  }

  private void checkFileHasContent(Path path, String expectedContent) {
    // before we compare the actual file content,
    // first do some checks that hint to basic issues
    assertThat(Files.exists(path), is(true));
    assertThat(Files.isRegularFile(path), is(true));
    assertThat(path.toFile().length(), greaterThan(0L)); // file has content

    String fileContent = TestUtils.getContentAsString(path);
    assertThat(fileContent.strip(), is(expectedContent.strip()));
  }

  private DirectoryManager.RunDirectory createMockDirectory(
      Path outputDirectory, Path inputDirectory) {
    DirectoryManager.RunDirectory mock = mock(DirectoryManager.RunDirectory.class);
    when(mock.getCurrentInputDirectory()).thenReturn(inputDirectory);
    when(mock.getCurrentOutputDirectory()).thenReturn(outputDirectory);
    when(mock.getConfigLocation()).thenReturn(inputDirectory.resolve("config.properties"));
    return mock;
  }
}

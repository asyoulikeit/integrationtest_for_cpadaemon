// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.runs.engine;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.sosy_lab.common.ShutdownManager;
import org.sosy_lab.common.configuration.InvalidConfigurationException;
import org.sosy_lab.common.configuration.Option;
import org.sosy_lab.common.configuration.Options;
import org.sosy_lab.cpachecker.grpc.runs.RunConfig;
import org.sosy_lab.cpachecker.grpc.runs.TestUtils;
import org.sosy_lab.cpachecker.grpc.testing.RunContext;
import org.sosy_lab.cpachecker.grpc.util.CpacheckerSupport;

/** Tests for {@link CpacheckerRunnerFactory}. */
@ExtendWith(MockitoExtension.class)
public class CpacheckerRunnerFactoryTest {
  private static final String VALID_CONFIG_FILE = "valueAnalysis.properties";
  private static final String INVALID_CONFIG_FILE = "invalid.properties";
  private static final String EMPTY_PROGRAM_FILE = "empty.c";
  private static final int TIMELIMIT_IN_SECONDS = 10;

  @Mock private OutputStream stdout;
  @Mock private CpacheckerSupport cpacheckerSupport;
  private Path logFile;
  private ShutdownManager shutdownManager;

  private RunContext runContext;

  @BeforeEach
  void setUp() throws IOException {
    runContext = new RunContext();
    runContext.setCurrentOutputDirectory(Files.createTempDirectory("cpa-daemon-test-output-"));
    // use a new independent shutdown-manager for each test, to avoid side effects.
    // (final class, so we can't mock it efficiently.)
    logFile = runContext.getCurrentOutputDirectory().resolve("any-name");
    shutdownManager = ShutdownManager.create();
  }

  @AfterEach
  void tearDown() throws IOException {
    runContext.tearDown();
  }

  @Test
  public void test_createWithValidConfigNoAdditionalOptions() {
    // given a RunConfig with a valid config file
    RunConfig config =
        RunConfig.builder()
            .cpacheckerConfig(getFile(VALID_CONFIG_FILE))
            .machineModel(RunConfig.MachineModel.LINUX32)
            .entryFunction("main")
            .programFile(getFile(EMPTY_PROGRAM_FILE))
            .build();
    CpacheckerRunnerFactory factory =
        new CpacheckerRunnerFactory(TIMELIMIT_IN_SECONDS, cpacheckerSupport);

    // when creating a CpacheckerRunner
    CpacheckerRunner runner =
        assertDoesNotThrow(
            () ->
                factory.create(
                    config,
                    runContext.getCurrentOutputDirectory(),
                    logFile,
                    stdout,
                    shutdownManager));

    // then
    assertThat(runner.config, notNullValue());
    assertThat("The logManager is not null", runner.logManager, notNullValue());
    assertThat("The shutdownManager is not null", runner.shutdownManager, notNullValue());
    assertThat(
        "The program path is unaltered", runner.programPath, is(getFile(EMPTY_PROGRAM_FILE)));

    OptionTester resolvedConfig = new OptionTester();
    assertDoesNotThrow(() -> runner.config.inject(resolvedConfig));
    assertThat(
        "The config was actually read",
        resolvedConfig.cpas,
        containsString("cpa.value.ValueAnalysisCPA"));
  }

  @Test
  public void test_createWithInvalidConfigNoAdditionalOptions() {
    // given a RunConfig with an invalid config file
    RunConfig config =
        RunConfig.builder()
            .cpacheckerConfig(getFile(INVALID_CONFIG_FILE))
            .machineModel(RunConfig.MachineModel.LINUX32)
            .entryFunction("main")
            .programFile(getFile(EMPTY_PROGRAM_FILE))
            .build();
    CpacheckerRunnerFactory factory =
        new CpacheckerRunnerFactory(TIMELIMIT_IN_SECONDS, cpacheckerSupport);

    // when creating a CpacheckerRunner
    Executable create =
        () ->
            factory.create(
                config, runContext.getCurrentOutputDirectory(), logFile, stdout, shutdownManager);

    // then
    assertThrows(InvalidConfigurationException.class, create);
  }

  @Test
  public void test_createWithValidConfigAndAdditionalOptions() {
    // given a RunConfig with a valid config file and additional options
    RunConfig config =
        RunConfig.builder()
            .cpacheckerConfig(getFile(VALID_CONFIG_FILE))
            .machineModel(RunConfig.MachineModel.LINUX32)
            .entryFunction("main")
            .programFile(getFile(EMPTY_PROGRAM_FILE))
            .additionalOption(new RunConfig.Option("analysis.traversal.order", "dfs"))
            .build();
    CpacheckerRunnerFactory factory =
        new CpacheckerRunnerFactory(TIMELIMIT_IN_SECONDS, cpacheckerSupport);

    // when creating a CpacheckerRunner
    CpacheckerRunner runner =
        assertDoesNotThrow(
            () ->
                factory.create(
                    config,
                    runContext.getCurrentOutputDirectory(),
                    logFile,
                    stdout,
                    shutdownManager));

    // then
    assertThat(runner.config, notNullValue());
    assertThat(runner.logManager, notNullValue());
    assertThat(runner.shutdownManager, notNullValue());
    assertThat(runner.programPath, is(getFile(EMPTY_PROGRAM_FILE)));

    OptionTester resolvedConfig = new OptionTester();
    assertDoesNotThrow(() -> runner.config.inject(resolvedConfig));
    assertThat("The additional option was applied", resolvedConfig.traversalOrder, is("dfs"));
  }

  private Path getFile(String file) {
    return TestUtils.getRunnerFile(file);
  }

  @Options
  private static class OptionTester {
    @Option(secure = true, name = "analysis.traversal.order", description = "test option")
    private String traversalOrder = "rand";

    @Option(secure = true, name = "CompositeCPA.cpas", description = "test option")
    private String cpas = "cpa1, cpa2";
  }
}

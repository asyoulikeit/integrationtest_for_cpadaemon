// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.when;

import java.nio.file.Path;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.sosy_lab.cpachecker.grpc.proto.Cpachecker;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.runs.RunException;
import org.sosy_lab.cpachecker.grpc.runs.RunManager;
import org.sosy_lab.cpachecker.grpc.runs.RunState;
import org.sosy_lab.cpachecker.grpc.runs.TestUtils;

@ExtendWith(MockitoExtension.class)
class CpacheckerRunnerServiceTest {

  private static final Path PROGRAM_FILE = TestUtils.getRunnerFile("safe.c");
  private static final Path CONFIG_FILE = TestUtils.getRunnerFile("valueAnalysis.properties");
  private static final Path SPECIFICATION_FILE = TestUtils.getRunnerFile("unreach-call.spc");
  private static final String PROGRAM_CODE = TestUtils.getContentAsString(PROGRAM_FILE);

  private static final String CONFIG_VALUES = TestUtils.getContentAsString(CONFIG_FILE);
  private static final String SPECIFICATION = TestUtils.getContentAsString(SPECIFICATION_FILE);
  private static final Cpachecker.StartRunRequest REQUEST_VALID =
      Cpachecker.StartRunRequest.newBuilder()
          .setProgramCode(PROGRAM_CODE)
          .setConfig(CONFIG_VALUES)
          .setSpecification(SPECIFICATION)
          .setMachineModel(Cpachecker.MachineModel.LINUX32)
          .build();
  private static final StartRun RUN_REQUEST =
      new StartRun(
          PROGRAM_CODE,
          CONFIG_VALUES,
          SPECIFICATION,
          null,
          null,
          StartRun.MachineModel.LINUX32,
          "main",
          null);

  private static final String RUN_NAME = "Run/42";
  private static final long TIMESTAMP = 2132123;
  private static final String ERROR_MESSAGE = "some error occurred";
  private static final RunState RUN_STARTED =
      RunState.builder()
          .name(RUN_NAME)
          .timestamp(TIMESTAMP)
          .activeness(RunState.Activeness.RUNNING)
          .build();

  private static final Cpachecker.Run RUN_RESPONSE_STARTED =
      Cpachecker.Run.newBuilder().setName(RUN_NAME).setStatus(Cpachecker.RunStatus.RUNNING).build();

  @Mock private RunResponseMapper resultMapper;
  @Mock private RunManager runManager;
  @Mock private RunRequestMapper requestMapper;

  private CpacheckerService service;

  @BeforeEach
  void setUp() {
    // this stub may not be used (e.g., in case a test expects an early error).
    // But the expected return value is the same for all tests, so define it here instead of
    // duplicating code
    lenient().when(requestMapper.toStartRunRequest(any())).thenReturn(RUN_REQUEST);

    service = new CpacheckerService(runManager, requestMapper, resultMapper);
  }

  @Test
  void testRun_startRunSuccessful() throws RunException {
    // given
    when(runManager.commissionRun(any())).thenReturn(RUN_STARTED);
    when(resultMapper.toRun(RUN_STARTED)).thenReturn(RUN_RESPONSE_STARTED);

    // when
    Cpachecker.RunResponse response = service.startRun(REQUEST_VALID).await().indefinitely();

    // then
    assertThat(response.hasRun(), is(true));
    assertThat(response.getRun(), is(RUN_RESPONSE_STARTED));
  }

  @Test
  void testRun_startRunFails() throws RunException {
    // given
    when(runManager.commissionRun(any())).thenThrow(new RunException(ERROR_MESSAGE));

    // when
    Cpachecker.RunResponse response = service.startRun(REQUEST_VALID).await().indefinitely();

    // then
    assertThat(response.hasError(), is(true));
    assertThat(response.getError(), containsString(ERROR_MESSAGE));
  }
}

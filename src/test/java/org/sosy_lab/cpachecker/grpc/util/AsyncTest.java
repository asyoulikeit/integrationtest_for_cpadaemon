// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import org.junit.jupiter.api.Test;

class AsyncTest {

  private final Async async = new Async();

  @Test
  void testStartWorker_runWorks()
      throws InterruptedException, ExecutionException, TimeoutException {
    // given
    AtomicInteger i = new AtomicInteger(0);
    int expectedValue = 5;

    // when
    CompletableFuture<Void> future = async.startWorker(() -> i.set(expectedValue));
    // give worker some time to finish
    future.get(100, TimeUnit.MILLISECONDS);

    // then
    assertThat(i.get(), equalTo(expectedValue));
  }
}

// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.util;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.not;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.ENTRY_FUNCTION_DEFAULT;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.getBasicRequest;
import static org.sosy_lab.cpachecker.grpc.runs.TestUtils.getContentAsString;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.sosy_lab.cpachecker.grpc.requests.StartRun;
import org.sosy_lab.cpachecker.grpc.runs.TestUtils;

class CpacheckerSupportTest {

  private static final String VALID_CONFIG = "valueAnalysis.properties";
  private static final String VALID_PROGRAM = "safe.c";
  private static final String VALID_SPEC = "unreach-call.spc";
  private static final String VALID_WITNESS = "violationWitness-correct.graphml";

  private CpacheckerSupport support;

  @BeforeEach
  void setUp() {
    support = new CpacheckerSupport();
  }

  @ParameterizedTest(name = "{0}")
  @MethodSource("provideInvalidInputs")
  void testgetIssuesWithRequest_invalidRequest(String name, StartRun request) {
    List<String> issues = support.getIssuesWithRequest(request);

    assertThat(issues, not(empty()));
  }

  @ParameterizedTest(name = "{0}")
  @MethodSource("provideValidInputs")
  void testCheckInputs_valid(String name, StartRun request) {
    List<String> issues = support.getIssuesWithRequest(request);

    assertThat(issues, empty());
  }

  static Stream<Arguments> provideInvalidInputs() {
    return Stream.of(
        Arguments.of(
            "no program code",
            new StartRun(
                /* programCode= */ "",
                /* config= */ getContentAsString(getFile(VALID_CONFIG)),
                /* specification= */ getContentAsString(getFile(VALID_SPEC)),
                /* textOptions= */ List.of(),
                /* fileOptions= */ List.of(),
                StartRun.MachineModel.LINUX32,
                ENTRY_FUNCTION_DEFAULT,
                /* witness= */ "")),
        Arguments.of(
            "unrecognized machine model",
            new StartRun(
                /* programCode= */ getContentAsString(getFile(VALID_PROGRAM)),
                /* config= */ getContentAsString(getFile(VALID_CONFIG)),
                /* specification= */ getContentAsString(getFile(VALID_SPEC)),
                /* textOptions= */ List.of(),
                /* fileOptions= */ List.of(),
                StartRun.MachineModel.UNRECOGNIZED,
                ENTRY_FUNCTION_DEFAULT,
                /* witness= */ "")),
        Arguments.of(
            "unspecified machine model",
            new StartRun(
                /* programCode= */ getContentAsString(getFile(VALID_PROGRAM)),
                /* config= */ getContentAsString(getFile(VALID_CONFIG)),
                /* specification= */ getContentAsString(getFile(VALID_SPEC)),
                /* textOptions= */ List.of(),
                /* fileOptions= */ List.of(),
                StartRun.MachineModel.UNSPECIFIED,
                ENTRY_FUNCTION_DEFAULT,
                /* witness= */ "")),
        Arguments.of(
            "no configuration options",
            new StartRun(
                /* programCode= */ getContentAsString(getFile(VALID_PROGRAM)),
                /* config= */ "",
                /* specification= */ getContentAsString(getFile(VALID_SPEC)),
                /* textOptions= */ List.of(),
                /* fileOptions= */ List.of(),
                StartRun.MachineModel.LINUX32,
                ENTRY_FUNCTION_DEFAULT,
                /* witness= */ "")));
  }

  static Stream<Arguments> provideValidInputs() {
    return Stream.of(
        Arguments.of("with spec", getBasicRequest(VALID_PROGRAM, VALID_CONFIG, VALID_SPEC)),
        Arguments.of("without spec", getBasicRequest(VALID_PROGRAM, VALID_CONFIG, null)),
        Arguments.of(
            "with witness",
            new StartRun(
                /* programCode= */ getContentAsString(getFile(VALID_PROGRAM)),
                /* config= */ getContentAsString(getFile(VALID_CONFIG)),
                /* specification= */ getContentAsString(getFile(VALID_SPEC)),
                /* textOptions= */ List.of(),
                /* fileOptions= */ List.of(),
                StartRun.MachineModel.LINUX32,
                ENTRY_FUNCTION_DEFAULT,
                /* witness= */ getContentAsString(getFile(VALID_WITNESS)))),
        Arguments.of(
            "non-default machine model and entry function",
            new StartRun(
                /* programCode= */ getContentAsString(getFile(VALID_PROGRAM)),
                /* config= */ getContentAsString(getFile(VALID_CONFIG)),
                /* specification= */ getContentAsString(getFile(VALID_SPEC)),
                /* textOptions= */ List.of(),
                /* fileOptions= */ List.of(),
                StartRun.MachineModel.LINUX64,
                "f",
                /* witness= */ "")));
  }

  private static Path getFile(String name) {
    return TestUtils.getRunnerFile(name);
  }
}

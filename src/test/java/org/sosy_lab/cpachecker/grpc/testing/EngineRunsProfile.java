// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.testing;

import com.google.common.collect.ImmutableMap;
import io.quarkus.test.junit.QuarkusTestProfile;

/** Test profile to make sure that CPAchecker is executed as engine. */
public class EngineRunsProfile implements QuarkusTestProfile {

  @Override
  public ImmutableMap<String, String> getConfigOverrides() {
    return ImmutableMap.<String, String>builder()
        .put("cpachecker.grpc.run.engine.enabled", "true")
        .build();
  }
}

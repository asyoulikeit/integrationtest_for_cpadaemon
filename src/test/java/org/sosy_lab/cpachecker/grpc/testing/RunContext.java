// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.cpachecker.grpc.testing;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;

/** Context of an individual run. */
public class RunContext {

  private Path currentOutputDirectory;

  /** Sets the current output directory. */
  public void setCurrentOutputDirectory(Path outputDirectory) {
    this.currentOutputDirectory = outputDirectory;
  }

  /** Returns the current output directory. */
  public Path getCurrentOutputDirectory() {
    return currentOutputDirectory;
  }

  /**
   * Tears down this run context. The run context will be usable afterwards, but the current context
   * may be completely deleted.
   *
   * @throws IOException if an I/O error occurs during tear down. This hints to a file in the
   *     context that could not be deleted.
   */
  public void tearDown() throws IOException {
    Files.walk(currentOutputDirectory)
        .sorted(Comparator.reverseOrder()) // to delete deep files first
        .map(Path::toFile)
        .forEach(java.io.File::delete);
    currentOutputDirectory = null;
  }
}

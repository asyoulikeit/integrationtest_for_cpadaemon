// This file is part of CPA-Daemon,
// a gRPC frontend for CPAchecker:
// https://gitlab.com/sosy-lab/software/cpa-daemon/
//
// SPDX-FileCopyrightText: 2023 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: CC0-1.0

= How To Build a CPA-Daemon Release
:sectnums:
:toc:
:imagesdir: z_figures

CPA-Daemon's continuous integration automatically builds releases for each pushed git tag.
Process:

. Select the correct release version. For example, `1.1.1-SNAPSHOT` may become `1.2.0`, `1.1.2`, or `2.0.0`.
. Create a branch to prepare the release.
. Check that link:../../../CHANGELOG.md[CHANGELOG.md] is up-to-date and introduce the new release there.
. Update the version for the release in link:../../../build.gradle[build.gradle]
  and link:../../../.ci/gitlab-ci.yml[gitlab-ci.yml].
. Commit the changes (we call this *commit R*).
. Update the version for the next development cycle in `build.gradle` and `gitlab-ci.yml`.
  We always target the next bugfix release so that we directly see
  the next, smallest possible release.  For example, `1.2.2` becomes `1.2.3-SNAPSHOT`.
. Commit the changes.
. Merge your changes into main
. Tag *commit R* on the main branch with the release version and push that.

The CI will package that version and create a release for it.

Example commit history:

image::Release-diagram.svg[]

